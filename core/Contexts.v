
(************************************************************************)
(* Copyright (c) 2020, Gergei Bana, Qianli Zhang                        *)
(*                                                                      *)
(* This work is licensed under the MIT license. The license is          *)
(* described in the file "LICENSE" available at the root of the source  *)
(* or at https://opensource.org/licenses/MIT                            *)
(************************************************************************)



Require Export Coq.Lists.List.
Require Export Coq.Init.Logic.
Require Export Coq.Init.Peano.
Require Export Coq.Arith.EqNat.
Require Export Setoid.
Import ListNotations.
Require Export FunctionalExtensionality.
Require Import Coq.Bool.Bool.
Require Export Syntax.





(*******************************************************************************)
(************************* Property of PPT functions ***************************)
(*******************************************************************************)

Reserved Notation "t0 << t1" (at level 80, no associativity).

Inductive hag_lt_help : ComputationType -> ComputationType -> Prop :=
| Det_Hon : Deterministic << Honest
| Det_Adv : Deterministic << Adversarial
| Hon_Gen : Honest << General
| Adv_Gen : Adversarial << General
where "t0 << t1" := (hag_lt_help t0 t1).


Reserved Notation "t0 <<< t1" (at level 80, no associativity).

(*lt means less than*)
Inductive hag_lt : ComputationType -> ComputationType -> Prop :=
| hag_nil : forall {t}, t <<< t
| hag_cons : forall {t0 t1 t2}, t0 << t1 -> t1 <<< t2 -> t0 <<< t2
where "t0 <<< t1" := (hag_lt t0 t1).



Definition hag_ltb (t1 t2: ComputationType) : bool :=
  match t1 with
  | Deterministic => true
  | Honest => match t2 with
             | Deterministic => false
             | Adversarial => false
             | _ => true
             end
  | Adversarial => match t2 with
                  | Deterministic => false
                  | Honest => false
                  | _ => true
                  end
  | General => match t2 with
              | General => true
              | _ => false
              end
  end.

Lemma hag_ltb_lt : forall t1 t2, hag_ltb t1 t2 = true <-> t1 <<< t2.
  split; intros.
  destruct t1; destruct t2; try inversion H; try repeat constructor;
    match goal with
    | |-  Deterministic <<< General =>
      apply (@hag_cons Deterministic Honest General); try repeat constructor
    | |- ?t1 <<< ?t2 =>
      apply (@hag_cons t1 t2 t2); try repeat constructor
    end.
  apply (@hag_cons Honest General General); repeat constructor.

  induction H.
  destruct t; auto.
  destruct H; destruct t2; auto.
Defined.


Lemma hag_reflect : forall t1 t2, reflect (t1 <<< t2) (hag_ltb t1 t2).
  intros t1 t2.
  apply iff_reflect.
  apply (iff_sym (hag_ltb_lt t1 t2)).
Defined.

Ltac ProveHag :=
  match goal with
  | |- ?t1 <<< ?t2 => assert (H100 := hag_reflect t1 t2); inversion H100; auto
  end.

Goal Deterministic <<< General.
  ProveHag.
Defined.

Goal General <<< General.
  ProveHag.
Defined.

Lemma ppt_up1
  : forall {t0 t1 f},
    t0 << t1 -> PPT t0 f -> PPT t1 f.
  intros.
  destruct H.
  apply DeterministicHonest; auto.
  apply DeterministicAdversarial; auto.
  apply HonestGeneral; auto.
  apply AdversarialGeneral; auto.
Defined.

Theorem ppt_up : forall {t0 t1 f},
    t0 <<< t1 -> PPT t0 f -> PPT t1 f.
  intros.
  induction H.
  auto.
  apply (IHhag_lt (ppt_up1 H H0)).
Defined.


(* Deterministic algorithms are in particular Honest, Adversarial and General *)
Proposition DeterministicHAG :
      forall {lf : list ppt -> ppt} ,
        (PPT Deterministic) lf
         -> forall (hag : ComputationType) , (PPT hag) lf.
Proof.
  intros.
  apply (@ppt_up Deterministic hag lf).
  destruct hag; ProveHag.
  auto. Defined.


Proposition GeneralHAG :
      forall {f} {P : Prop} , (PPT General f -> P) ->
forall hag , (PPT hag f -> P).
Proof.
  intros. apply H.
  destruct hag.
  apply (@ppt_up Honest General f). ProveHag. auto.
  apply (@ppt_up Deterministic General f). ProveHag. auto.
  apply (@ppt_up Adversarial General f). ProveHag. auto.
  auto.
Qed.

Proposition HAGGeneral :
      forall (hag : ComputationType) , forall {lf : list ppt -> ppt} ,
          (PPT hag) lf
            -> (PPT General) lf .
Proof.
  intros.
  destruct hag.
  apply (@ppt_up Honest General lf). ProveHag. auto.
  apply (@ppt_up Deterministic General lf). ProveHag. auto.
  apply (@ppt_up Adversarial General lf). ProveHag. auto.
  auto.
Qed.





(**************************************************************************************)
(**************************************************************************************)
(************************************** CONTEXTS **************************************)
(**************************************************************************************)
(**************************************************************************************)








(****************************** some auxiliary functions *****************************)


(* Labels to distinguish ppt and list ppt *)

Inductive TermOrList : Type :=
| Term
| List.

Definition ToL (tol : TermOrList) : Type :=
match tol with
| Term => ppt
| List => list ppt
end.



(* Id represents the identity function for ppt -> ppt and
  for list ppt -> list ppt. For ppt -> list ppt it assigns [x] to x, and
for list ppt -> ppt it assigns HD lx to lx*)

Definition Id (tol : TermOrList ) (tol' : TermOrList ) : (ToL tol) -> (ToL tol') :=
match tol with
| Term => match tol' with Term => (fun x : ppt => x) | List => (fun x : ppt => [x]) end
| List => match tol' with Term => (fun lx : list ppt => HD lx) | List => (fun lx : list ppt => lx) end
end.


Proposition IdHD :
forall tol, (fun x  => Id tol Term x ) = (fun x  => HD (Id tol List x )).
Proof. intros. destruct tol.
  - unfold Id at 2. unfold HD. simpl. auto.
  - unfold Id. auto.
Qed.



(**************************************************************************************)
(********************************* Context Definition *********************************)
(**************************************************************************************)



Section Context.
(* Recursive definition of a Context outputting a list ppt and a ContextTerm outputting
a ppt. The input for both is (ToL tol), which is either ppt or list ppt.
for each hag Deterministic, Honest, Adversarial, General, contexts are defined.
 *)
Parameter ListContext : ComputationType -> (list ppt -> list ppt) -> Prop.
Parameter TermContext : ComputationType -> (ppt -> list ppt) -> Prop.
Variable hag : ComputationType.
(* Below ct is for ContextTerm variables, c is for Context variables *)
Inductive Context (tol : TermOrList) : (ToL tol -> list ppt) -> Prop :=  (* metapredicate *)
| cID  :
      Context tol (Id tol List)
| cnil   :
      Context tol (fun x : ToL tol => nil)
| cTL   :
      forall {c} ,
        Context tol c
        -> Context  tol (fun x => (TL (c x)))
| cfirstn   :
      forall {c n} ,
        Context tol c
        -> Context  tol (fun x => (firstn n (c x)))
| clength   :
      forall {c n} ,
        Context tol c
        -> Context  tol (fun x => (skipn ((length (c x)) - n) (c x)))
| cskipn   :
      forall {c n} ,
        Context tol c
        -> Context  tol (fun x => (skipn n (c x)))
| cConc  :
      forall {ct c},
        ContextTerm tol ct
        -> Context tol c
        -> Context tol (fun x => (ct x)::(c x))
| clConc  :
      forall {c1 c2},
        Context tol c1
        -> Context tol c2
        -> Context tol  (fun x => (c1 x)++(c2 x))
| cCont   :
      forall {c1 c2 },
        Context  List  c1
        -> Context tol c2
        -> Context tol (fun x   => (c1 (c2 x)) )
| tcCont   :
      forall {tc1 c2},
        Context Term tc1
        -> ContextTerm tol c2
        -> Context tol (fun x => (tc1  (c2 x)))

with ContextTerm (tol : TermOrList)  : (ToL tol -> ppt) -> Prop :=
| ctID   :
      ContextTerm tol (Id tol Term)
| ctHD  :
      forall {c} ,
        Context tol c
        -> ContextTerm  tol (fun x => (HD (c x)))
| ctCT:
      forall {f: list ppt -> ppt} {c : ToL tol -> list ppt} ,
        (PPT hag) f
        -> Context tol c
        -> ContextTerm tol  (fun x => (f (c x))).
End Context.


Check Context_ind.




Scheme Context_mut := Minimality for Context Sort Prop
  with ContextTerm_mut := Minimality for ContextTerm Sort Prop.


Lemma Ctx_mul_ind:
  forall(P : forall tol : TermOrList,
              ComputationType -> (ToL tol -> list ppt) -> Prop)
         (P0 : forall tol : TermOrList,
               ComputationType -> (ToL tol -> ppt) -> Prop),
       (forall (tol : TermOrList) (hag : ComputationType),
        P tol hag (Id tol List)) ->
       (forall (tol : TermOrList) (hag : ComputationType),
        P tol hag (fun _ : ToL tol => [])) ->
       (forall (tol : TermOrList) (hag : ComputationType)
          (c : ToL tol -> list ppt),
        @Context hag tol c -> P tol hag c -> P tol hag (fun x : ToL tol => TL (c x))) ->
       (forall (tol : TermOrList) (hag : ComputationType)
          (c : ToL tol -> list ppt) (n :nat),
        @Context hag tol c -> P tol hag c -> P tol hag (fun x : ToL tol => firstn n (c x))) ->
       (forall (tol : TermOrList) (hag : ComputationType)
          (c : ToL tol -> list ppt) (n :nat),
        @Context hag tol c -> P tol hag c -> P tol hag (fun x : ToL tol => skipn n (c x))) ->
       (forall (tol : TermOrList) (hag : ComputationType)
          (c : ToL tol -> list ppt) (n :nat),
        @Context hag tol c -> P tol hag c -> P tol hag (fun x : ToL tol => skipn (length (c x) - n) (c x))) ->
       (forall (tol : TermOrList) (hag : ComputationType)
          (ct : ToL tol -> ppt) (c : ToL tol -> list ppt),
        @ContextTerm hag tol  ct ->
        P0 tol hag ct ->
        @Context hag tol  c -> P tol hag c -> P tol hag (fun x : ToL tol => ct x :: c x)) ->
       (forall (tol : TermOrList) (hag : ComputationType)
          (c1 c2 : ToL tol -> list ppt),
        @Context hag tol  c1 ->
        P tol hag c1 ->
        @Context hag tol  c2 ->
        P tol hag c2 -> P tol hag (fun x : ToL tol => c1 x ++ c2 x)) ->
       (forall (tol : TermOrList) (hag : ComputationType)
          (c1 : ToL List -> list ppt) (c2 : ToL tol -> list ppt),
        @Context  hag List c1 ->
        P List hag c1 ->
        @Context  hag  tol c2 -> P tol hag c2 -> P tol hag (fun x : ToL tol => c1 (c2 x))) ->
       (forall (tol : TermOrList) (hag : ComputationType)
          (tc1 : ToL Term -> list ppt) (c2 : ToL tol -> ppt),
        @Context  hag Term tc1 ->
        P Term hag tc1 ->
        @ContextTerm  hag tol c2 ->
        P0 tol hag c2 -> P tol hag (fun x : ToL tol => tc1 (c2 x))) ->
       (forall (tol : TermOrList) (hag : ComputationType),
        P0 tol hag (Id tol Term)) ->
       (forall (tol : TermOrList) (hag : ComputationType)
          (c : ToL tol -> list ppt),
        @Context  hag tol c -> P tol hag c -> P0 tol hag (fun x : ToL tol => HD (c x))) ->
       (forall (tol : TermOrList) (hag : ComputationType)
          (f8 : list ppt -> ppt) (c : ToL tol -> list ppt),
        PPT hag f8 ->
        @Context  hag tol c -> P tol hag c -> P0 tol hag (fun x : ToL tol => f8 (c x))) ->
  (forall (tol : TermOrList) (hag : ComputationType)
     (l : ToL tol -> list ppt), @Context hag tol l -> P tol hag l)
/\ (forall (tol : TermOrList) (hag : ComputationType) (l : ToL tol -> ppt),
       @ContextTerm hag tol l -> P0 tol hag l).
  split; intros.
  apply (Context_mut  hag  (fun x => P x hag) (fun x => P0 x hag)); auto.
  apply (ContextTerm_mut hag (fun x => P x hag) (fun x => P0 x hag)); auto.
Defined.



(*Generally speaking, any {c0 c1} that if c0 <<< c1 holds, then PPT c0 f -> PPT c1 f
  similarily, Context tol c0 f -> Context tol c1 f*)



Theorem ctx_up:
            (forall tol c0 f,
                (Context c0 tol f -> forall c1, (c0 <<< c1) -> Context c1 tol f))
          /\ (forall tol c0 f,
                (ContextTerm  c0 tol f -> forall c1, (c0 <<< c1) -> ContextTerm  c1 tol f)).
Proof.
  apply (Ctx_mul_ind) with
      (P := fun   tol c f => forall c0, (c <<< c0) -> Context  c0 tol f)
      (P0 := fun tol c f => forall c0, (c <<< c0) -> ContextTerm  c0 tol f); intros; try (constructor; auto).
  destruct H2.
(* "+", "-", and "*" means bullets*)
  +
    auto.
  +
    apply (ppt_up (hag_cons H2 H3) H).
Qed.

Goal (forall tol f, Context  Deterministic tol f -> Context Honest tol f).
Proof.
  pose proof ctx_up as ctx_up.
  destruct ctx_up; intros.
  +
    apply (H tol Deterministic f H1 Honest).
    ProveHag.
Qed.

(**************************************************************************************)
(******************************** Context Propositions ********************************)
(**************************************************************************************)





(* [ ] makes a Context from ContextTerm*)
Proposition cTtoL :
      forall {hag tol ct},
        ContextTerm hag tol ct
        -> Context hag tol (fun x => [ (ct x)]).
Proof. intros. apply cConc. apply H. apply cnil. Qed.


(* Various ways of combining two Contexts *)
Proposition ctlCont :
      forall {hag tol c1 c2},
        Context hag Term c1
        -> ContextTerm hag tol c2
        -> Context hag tol (fun x => (c1  (c2 x))).
Proof. intros. apply tcCont.   assumption. assumption. Qed.

Proposition cllCont :
      forall {hag tol c1 c2},
        Context hag List c1
        -> Context hag tol c2
        -> Context hag tol (fun x => (c1  (c2 x))).
Proof. intros. apply cCont.  assumption. assumption. Qed.

Proposition cttCont :
      forall {hag tol tc1 tc2},
        ContextTerm hag Term tc1
        -> ContextTerm hag tol tc2
        -> ContextTerm hag tol (fun x => (tc1  (tc2 x))).
Proof. intros. assert ((fun x : ToL tol => tc1 (tc2 x))= (fun x : ToL tol => HD [tc1 (tc2 x)])). simpl. reflexivity.
rewrite H1. apply ctHD.  apply (@ctlCont hag tol (fun x => [tc1 x]) tc2). apply cTtoL. assumption. assumption. Qed.

Proposition cltCont :
      forall {hag tol tc1 tc2},
        ContextTerm hag List tc1
        -> Context hag tol tc2
        -> ContextTerm hag tol (fun x => (tc1  (tc2 x))).
Proof. intros. assert ((fun x : ToL tol => tc1 (tc2 x))= (fun x : ToL tol => HD [tc1 (tc2 x)])). simpl. reflexivity.
rewrite H1. apply ctHD.  apply (@cllCont hag tol (fun x => [tc1 x]) tc2). apply cTtoL. assumption. assumption. Qed.



(* Nth is a Context *)
Proposition cNth :
      forall {hag tol n c},
        Context hag tol c
          -> ContextTerm hag tol (fun x => (Nth n (c x))).
Proof. intros hag tol n.  induction n. intros c.
rewrite ZerothisHD. apply ctHD. intros. rewrite SnthisnthTL.
apply (IHn (fun x => TL (c x))). apply cTL. apply H. Qed.


Proposition NthContext:
forall {hag n},
 ContextTerm hag List (Nth n).
Proof. intros. apply cNth. apply cID. Qed.





Proposition HDContext:
forall {hag},
 ContextTerm hag List HD.
Proof. intros. apply ctHD. apply cID. Qed.



Proposition TLContext:
forall {hag},
 Context hag List TL.
Proof. intros. apply cTL. apply cID. Qed.

Proposition firstnContext:
forall {hag n},
 Context hag List (firstn n).
Proof. intros. apply cfirstn. apply cID. Qed.

Proposition skipnContext:
forall {hag n},
 Context hag List (skipn n).
Proof. intros. apply cskipn. apply cID. Qed.

Proposition lengthContext:
forall {hag n},
 Context hag List (fun x => (skipn ((length x) - n) x)).
Proof. intros. apply clength. apply cID. Qed.


(* Constant map of the right hag class is ContextTerm *)
Proposition ctPPT0 :
      forall {hag tol x0} ,
        (PPT hag) (fun lx => x0)
        -> ContextTerm hag tol (fun x => x0).
Proof. intros.   apply (@ctCT hag tol (fun lx : list ppt => x0) (Id tol List) H).
apply cID. Qed.


Proposition nonceContext :
forall n  tol , ContextTerm Honest tol (fun x => nonce n) /\ ContextTerm General tol (fun x => nonce n) .
Proof. intros.  apply conj. apply ctPPT0. apply nonceHonest. apply ctPPT0. apply HonestGeneral.
apply nonceHonest.  Qed.

Proposition advContext :
forall n  , ContextTerm Adversarial List (adv  n) /\ ContextTerm General List (adv n) .
Proof. intros.  apply conj. apply ctCT. apply advAdversarial.
apply cID.
 apply ctCT. apply AdversarialGeneral.
apply advAdversarial. apply cID.  Qed.

Proposition DetFSContext :
forall n  hag , ContextTerm hag List (FunctionSymbol Deterministic n).
Proof. intros.   apply ctCT. apply DeterministicHAG. apply FunHAG. apply cID. Qed.


Proposition HonFSContext :
forall n , ContextTerm Honest List (FunctionSymbol Honest n) /\ ContextTerm General List (FunctionSymbol Honest n).
Proof. intros.  apply conj. apply ctCT. apply FunHAG.
apply cID.
 apply ctCT. apply HonestGeneral.
apply FunHAG. apply cID.  Qed.

Proposition AdvFSContext :
forall n , ContextTerm Adversarial List (FunctionSymbol Adversarial n) /\ ContextTerm General List (FunctionSymbol Adversarial n).
Proof. intros.  apply conj. apply ctCT. apply FunHAG.
apply cID.
 apply ctCT. apply AdversarialGeneral.
apply FunHAG. apply cID.  Qed.


Proposition GenFSContext :
forall n , ContextTerm General List (FunctionSymbol General n).
Proof. intros.  apply ctCT. apply FunHAG.
apply cID.  Qed.


Proposition DetCSContext :
forall n  hag , ContextTerm hag List (fun lx => ConstantSymbol Deterministic n).
Proof. intros.   apply ctPPT0. apply DeterministicHAG. apply ConHAG. Qed.

Proposition HonCSContext :
forall n , ContextTerm Honest List (fun lx => ConstantSymbol Honest n) /\ ContextTerm General List (fun lx => ConstantSymbol Honest n).
Proof. intros.  apply conj. apply ctPPT0. apply ConHAG.
apply ctPPT0. apply HonestGeneral.
apply ConHAG. Qed.

Proposition AdvCSContext :
forall n , ContextTerm Adversarial List (fun lx => ConstantSymbol Adversarial n) /\ ContextTerm General List (fun lx => ConstantSymbol Adversarial n).
Proof. intros.  apply conj. apply ctPPT0. apply ConHAG.
 apply ctPPT0. apply AdversarialGeneral.
apply ConHAG.  Qed.

Proposition GenCSContext :
forall n , ContextTerm General List (fun lx => ConstantSymbol General n).
Proof. intros.  apply ctPPT0. apply ConHAG.
 Qed.




(* TRue, FAlse, default, EQ, If Then Else are contexts *)

Proposition defaultContext :
      forall {hag tol},
        ContextTerm hag tol (fun x => default).
Proof. intros.
apply ctPPT0. apply DeterministicHAG. apply defaultDeterministic. Qed.
Proposition TRueContext :
      forall {hag tol},
        ContextTerm hag tol (fun x => TRue).
Proof. intros.
apply ctPPT0. apply DeterministicHAG. apply trueDeterministic. Qed.
(*Proposition cTRue :
forall {hag tol},
    Context hag tol (fun x => [TRue]).
Proof. intros. apply cTtoL. apply TRueContext. Qed.*)
Proposition FAlseContext :
      forall {hag tol},
        ContextTerm hag tol (fun x => FAlse).
Proof. intros.
apply ctPPT0. apply DeterministicHAG. apply falseDeterministic. Qed.
(*Proposition cFAlse :
      forall {hag tol},
        Context hag tol (fun x => [FAlse]).
Proof. intros. apply cTtoL. apply FAlseContext.
Qed.*)
Proposition ctEQ :
      forall {hag tol tc1 tc2},
        ContextTerm hag tol tc1
        -> ContextTerm hag tol tc2
        -> ContextTerm hag tol (fun x => (EQ [(tc1 x) ; (tc2 x)])).
Proof. intros. apply ctCT. apply DeterministicHAG. apply EQDeterministic. apply cConc. apply H. apply cTtoL. apply H0. Qed.
Proposition cEQ :
      forall {hag tol tc1 tc2},
        ContextTerm hag tol tc1 ->ContextTerm hag tol tc2 -> Context hag tol (fun x =>  [EQ [(tc1 x) ; (tc2 x)]]).
Proof. intros. apply cTtoL. apply ctEQ. apply H. apply H0. Qed.


Proposition EQContext :
      forall {hag},
        ContextTerm hag List EQ.
Proof. intros. apply ctCT. apply DeterministicHAG. apply EQDeterministic. apply cID. Qed.


Proposition ctITE :
      forall {hag tol tc1 tc2 tc3},
        ContextTerm hag tol tc1 -> ContextTerm hag tol tc2 -> ContextTerm hag tol tc3 -> ContextTerm hag tol (fun x => (If_Then_Else_ [(tc1 x); (tc2 x); (tc3 x)])).
Proof. intros. apply ctCT. apply DeterministicHAG. apply ITEDeterministic. apply cConc. apply H. apply cConc. apply H0. apply cTtoL. apply H1. Qed.
Proposition cITE :
      forall {hag tol tc1 tc2 tc3},
        ContextTerm hag tol tc1
        -> ContextTerm hag tol tc2
        -> ContextTerm hag tol tc3
        -> Context hag tol (fun x => [If_Then_Else_ [(tc1 x); (tc2 x); (tc3 x)]]).
Proof. intros. apply cTtoL. apply ctITE. apply H.  apply H0.  apply H1. Qed.

Proposition ITEContext :
      forall {hag},
        ContextTerm hag List If_Then_Else_.
Proof. intros. apply ctCT. apply DeterministicHAG. apply ITEDeterministic. apply cID. Qed.


Proposition ctConstant :
      forall {x0 : ppt} {tol},
        ContextTerm General tol (fun x => x0).
Proof. intros. apply ctPPT0. apply ConstantGeneral. Qed.

Proposition cConstant : forall {x : list ppt}  {tol} ,  Context General tol
  (fun _ : ToL tol => x).
Proof. intros. induction x. apply cnil. apply cConc.  apply ctConstant.  trivial.
Qed.

(**************************************************************************************)
(***************************** Automated Check of Context *****************************)
(**************************************************************************************)


(* Automated proof that something is a context. *)
Ltac ProveContext :=
  repeat ( intros;
match goal with
      |H : ?prop |- ?prop => apply H
  (*    |H : ComputationType |- _ => destruct H *)
  (*    |H : TermOrList  |- _ => destruct H *)
      |   |- Context _ List (fun lx  => lx) => apply @cID
      |   |- Context _ List id => apply @cID
      |   |- Context _ _ (fun x   => ?a :: ?m) => apply @cConc
      |   |- Context _ _ (fun x   => ?a ++ ?m) => apply @clConc
      |   |- Context _ List (cons ?a) => apply @cConc
      |   |- Context _ _ (fun x => [ ]) => apply @cnil
      |   |- Context _ _ TL => apply @TLContext
      |   |- Context _ _ (fun x   => TL ?t) => apply @cllCont
      |   |- Context _ _ (firstn ?n) => apply @firstnContext
      |   |- Context _ _ (fun x   => firstn ?n ?t) => apply @cllCont
      |   |- Context _ _ (skipn ((length ?t) - ?n)) => apply @lengthContext
      |   |- Context _ _ (fun x   => skipn ((length ?t) - ?n) ?t) => apply @lengthContext
      |   |- Context _ _ (skipn ?n) => apply @skipnContext
      |   |- Context _ _ (fun x   => skipn ?n ?t) => apply @cllCont
      |   |- ContextTerm _ Term (fun x  => x) => apply @ctID
      |   |- ContextTerm _ Term id => apply @ctID
      |   |- ContextTerm _ _ (fun x => TRue) => apply @TRueContext
      |   |- ContextTerm _ _ (fun x => FAlse) => apply @FAlseContext
      |   |- ContextTerm _ _ (fun x => default) => apply @defaultContext
      |   |- ContextTerm _ _ (fun x => ConstantSymbol Deterministic ?n) => apply @DetCSContext
      |   |- ContextTerm _ _ (fun x => ConstantSymbol Honest ?n) => apply @HonCSContext
      |   |- ContextTerm _ _ (fun x => ConstantSymbol Adversarial ?n) => apply @AdvCSContext
      |   |- ContextTerm _ _ (fun x => ConstantSymbol General ?n) => apply @GenCSContext
      |   |- ContextTerm _ _ (fun x => nonce ?n) => apply @nonceContext
      |   |- ContextTerm _ _ (Nth ?n) => apply @NthContext
      |   |- ContextTerm _ _ HD => apply @HDContext
      |   |- ContextTerm _ _ (FunctionSymbol Deterministic ?n) => apply @DetFSContext
      |   |- ContextTerm _ _ (FunctionSymbol Honest ?n) => apply @HonFSContext
      |   |- ContextTerm _ _ (FunctionSymbol Adversarial ?n) => apply @AdvFSContext
      |   |- ContextTerm _ _ (FunctionSymbol General ?n) => apply @GenFSContext
      |   |- ContextTerm _ _ (adv _) => apply @advContext
      |   |- ContextTerm _ _ If_Then_Else_ => apply @ITEContext
      |   |- ContextTerm _ _ EQ => apply @EQContext
      |   |- ContextTerm _ _ (fun x   => HD ?t) => apply @cltCont
      |   |- ContextTerm _ _ (fun x  => Nth ?n ?t) => apply @cltCont
      |   |- ContextTerm _ _ (fun x   => adv ?n ?t) => apply @cltCont
      |   |- ContextTerm _ _ (fun x   => EQ ?t) => apply @cltCont
      |   |- ContextTerm _ _ (fun x   => If_Then_Else_ ?t) => apply @cltCont
      |   |- ContextTerm _ _ (fun x  => FunctionSymbol ?hag' ?n ?t) => apply @cltCont
      | H : Context _ List ?c  |- Context _ _ (fun lx => (?c ?t)) => apply (@cllCont _ _)
      | H : Context _ List ?c  |- Context _ List (fun lx : list ppt => (?c ?t)) => apply (@cllCont _ List)
      | H : Context _ List ?c  |- Context _ Term (fun x : ppt => (?c ?t)) => apply (@cllCont _ Term)
      | H : Context _ Term ?c  |- Context _ _ (fun x : ToL _ => (?c ?t)) => apply (@ctlCont _ _)
      | H : Context _ Term ?c  |- Context _ Term (fun x : ppt => (?c ?t)) => apply (@ctlCont _ Term)
      | H : Context _ Term ?c  |- Context _ List (fun lx : list ppt => (?c ?t)) => apply (@ctlCont _ List)
      | H : ContextTerm _ List ?c  |- ContextTerm _ _ (fun lx : ToL _ => (?c ?t)) => apply (@cltCont _ _)
      | H : ContextTerm _ List ?c  |- ContextTerm _ List (fun lx : list ppt => (?c ?t)) => apply (@cltCont _ List)
      | H : ContextTerm _ List ?c  |- ContextTerm _ Term (fun x : ppt => (?c ?t)) => apply (@cltCont _ Term)
      | H : ContextTerm _ Term ?c  |- ContextTerm _ _ (fun x : ToL _ => (?c ?t)) => apply (@cttCont _ _)
      | H : ContextTerm _ Term ?c  |- ContextTerm _ Term (fun x : ppt => (?c ?t)) => apply (@cttCont _ Term)
      | H : ContextTerm _ Term ?c  |- ContextTerm _ List (fun lx : list ppt => (?c ?t)) => apply (@cttCont _ List)
      |   |- ContextTerm _ _ (fun x => (?f ?t)) => apply @ctCT
      |   |- ContextTerm General _ (fun x => ?t) => apply @ctConstant
      |   |- Context General _ (fun x => ?t) => apply @cConstant
      |   |- ContextTerm _ _ (fun x  => ?t) => apply @ctPPT0
  end). (* Works quite well but needs to be extended. PPT _ _ goals should be skipped down. *)






(******************************** Testing the Automation ******************************)


Proposition FAlseContext2 :
      forall {hag tol},
        ContextTerm hag tol (fun x => FAlse).
Proof. ProveContext. Qed.



Proposition bla11 : Context Adversarial List (fun x0 : ToL List => x0).
Proof.  ProveContext. Qed.


Proposition  bla10: forall tc1 tc2 , ContextTerm Honest Term tc1 -> ContextTerm Honest Term tc2 -> Context Honest Term
  (fun x : ToL Term => [tc1 x; tc2 x]).
Proof.
 ProveContext. Qed.

Proposition bla : forall hag , ContextTerm hag Term (fun x : ToL Term => x).
Proof. ProveContext.  Qed.

Proposition bla0 : forall hag , ContextTerm hag List (fun x : ToL List => HD x ).
Proof.   ProveContext.  Qed.

Proposition bla2 :  forall hag  t1 t2 ,
(PPT hag (fun x => t1)) -> (PPT hag (fun x => t2)) ->
ContextTerm hag List (fun x : ToL List => (EQ [t1 ; t2])).
Proof. ProveContext.  Qed.

Proposition bla3 : forall t : ppt , ContextTerm General Term (fun x : ToL Term => (EQ [x ; t])).
Proof. ProveContext. Qed.

Proposition bla4 : forall t1 t2 : ppt , ContextTerm General Term (fun x : ToL Term => (If x Then t1 Else t2)).
Proof. ProveContext. Qed.

Proposition bla5 : forall b (x1 : ppt) (y1:ppt) t1  , ContextTerm General Term
  (fun t2' : ToL Term =>
   If b
      Then If b
              Then t1 x1
              Else t1 y1
      Else t2').
Proof. ProveContext. Qed.


Proposition bla6 : Context General Term (fun x : ToL Term => [x;x]).
Proof. ProveContext.    Qed.


Proposition ite : ContextTerm General Term (fun x => If x Then x Else x).
Proof.   ProveContext.  Qed.




(********************************* EXAMPLE: Projection ********************************)

Proposition proj02 :
      forall {hag} ,
        Context hag List (fun x => [HD x ; HD (TL (TL x))]).
Proof. ProveContext. Qed.



(****************** EXAMPLE: Permutation - using the nth, it's easier *****************)

Proposition perm021 :
      forall {hag } c , Context hag List c ->
        Context hag List (fun x => [Nth 0 (c x) ; Nth 2 x ; Nth 1 x ]).

Proof. ProveContext.  Qed.





(**************************************************************************************)
(************ Introduction of further symbols for pairing, primitives etc *************)
(**************************************************************************************)


(* Further functions can be introduced using the built-in countable function symbols.*)


(*(********************************** EXAMPLE: PAIRING **********************************)




Parameter pairs : Symbols.

Notation "'pair'" := (FunctionSymbol Deterministic pairs).



Axiom (*metaaxiom*) pair2arg :
      forall {x1 x2 : ppt} {lx : list ppt} ,
        (pair nil) = default
        /\ pair [x1] = default
        /\ (pair ([x1; x2]++lx)) = (pair [x1; x2]).


Proposition ctpair :
       forall {hag tol tc1 tc2},
         ContextTerm hag tol tc1
         -> ContextTerm hag tol tc2
         -> ContextTerm hag tol (fun x => (pair ([ (tc1 x) ; (tc2 x)]) )).
 Proof. intros. ProveContext. Qed.


Parameter proj1s : Symbols.

Notation "'proj1'" := (FunctionSymbol Deterministic proj1s).



Axiom (*metaaxiom*) proj11arg :
      forall {x : ppt} {lx : list ppt} ,
        (proj1 nil) = default
        /\ (proj1 ([x]++lx)) = (proj1 [x]).



Axiom (*metaaxiom*) proj1pair :
      forall {x1 x2 : ppt} ,
        (proj1 [pair [x1 ; x2]]) = x1.


Parameter proj2s : Symbols.

Notation "'proj2'" := (FunctionSymbol Deterministic proj2s).




Axiom (*metaaxiom*) proj21arg :
      forall {x : ppt} {lx : list ppt} ,
        (proj2 nil) = default
        /\ (proj2 ([x]++lx)) = (proj2 [x]).


Axiom (*metaaxiom*) proj2pair :
      forall {x1 x2 : ppt} ,
        (proj2 [pair [x1 ; x2]]) = x2.


Lemma bla15 : ContextTerm General Term (fun x => proj2 [(adv 5) [(pair [EQ [(nonce 2) ; x] ; x])]]).
Proof. ProveContext. Qed.  *)
