
(************************************************************************)
(* Copyright (c) 2020, Gergei Bana                                      *)
(*                                                                      *)
(* This work is licensed under the MIT license. The license is          *)
(* described in the file "LICENSE" available at the root of the source  *)
(* or at https://opensource.org/licenses/MIT                            *)
(************************************************************************)

Require Export Coq.Lists.List.
Require Export Coq.Init.Logic.
Require Export Coq.Init.Peano.
Require Export Coq.Arith.EqNat.
Require Export Ensembles.
Require Export Setoid.
Require Import Coq.micromega.Lia.
Import ListNotations.

Require Export AxiomImplications.




(*Key generation*)
Parameter pkeys : Symbols.

Notation "'pkey'" := (FunctionSymbol Deterministic pkeys).

Definition pk (n : nat) : ppt := pkey [nonce n].


Parameter skeys : Symbols.
Notation "'skey'" := (FunctionSymbol Deterministic skeys).

Definition sk (n : nat) : ppt := skey [nonce n].



Axiom (*metaaxiom*) pkey1arg :
      forall {x : ppt} {lx : list ppt} ,
        (pkey nil) = default
        /\ (pkey ([x]++lx)) = (pkey [x]).


Axiom (*metaaxiom*) skey1arg :
      forall {x : ppt} {lx : list ppt} ,
        (skey nil) = default
        /\ (skey ([x]++lx)) = (skey [x]).


(*Random input*)
Parameter rands : Symbols.
Notation "'rand'" := (FunctionSymbol Deterministic rands).

Definition r (n : nat) : ppt := rand [nonce n].

Axiom (*metaaxiom*) rand1arg :
      forall {x : ppt} {lx : list ppt} ,
        (rand nil) = default
        /\ (rand ([x]++lx)) = (rand [x]).


(*Encryption*)
Parameter encs : Symbols.
Notation "'enc'" := (FunctionSymbol Deterministic encs).


Axiom (*metaaxiom*) enc3arg :
      forall {x1 x2 x3: ppt} {lx : list ppt} ,
        (enc nil) = default
        /\ enc [x1] = default
        /\ enc [x1;x2] = default
        /\ (enc ([x1; x2;x3]++lx)) = (enc [x1; x2; x3]).


(*Decryption*)
Parameter decs : Symbols.
Notation "'dec'" := (FunctionSymbol Deterministic decs).



Axiom (*metaaxiom*) dec2arg :
      forall {x1 x2 : ppt} {lx : list ppt} ,
        (dec nil) = default
        /\ dec [x1] = default
        /\ (dec ([x1; x2]++lx)) = (dec [x1; x2]).





(*Decrypting the encryption gives plaintext*)
Axiom (*metaaxiom*) decenc :
      forall {m : ppt} {n n': nat},
        (dec [enc [m; pk n ; r n']; sk n]) # m.






(*Test*)
Goal forall n n' n'' n''',
      n <> n'
      -> n <> n''
       -> n <> n'''
      -> FreshTerm (nonce n)  (dec [enc [nonce n'; pk n'' ; r n''']; sk n']).
  intros. ProveFresh. Qed.







Inductive FreshExcept : ppt  -> list ppt -> ppt -> Prop := (* metapredicate *)
| frenil : forall n t, FreshExcept (nonce n) nil t
| freConc : forall (n : nat) (t t': ppt) (tc : list ppt) ,
    (FreshExceptTerm (nonce n) t t')
    -> (FreshExcept (nonce n) tc t')
    -> (FreshExcept (nonce n) (t :: tc) t')
with FreshExceptTerm : ppt -> ppt -> ppt -> Prop :=
| fretNN : forall (n m: nat) t , n <> m
                        -> (FreshExceptTerm (nonce n) (nonce m) t )
| fretNt : forall (n : nat) t ,
                        (FreshExceptTerm (nonce n) t t)
| fretFAdv: forall n (t: list ppt -> ppt) (tc : list ppt) t',
    (ContextTerm Adversarial List t)
    -> FreshExcept (nonce n) tc t'
    -> FreshExceptTerm (nonce n) (t tc) t'
| fretCAdv: forall n (t: ppt) t',
    (ContextTerm Adversarial List (fun x =>  t))
    -> FreshExceptTerm (nonce n) t t'.




Ltac ProveFreshExcept :=
  repeat ( intros; match goal with
           |[ |- FreshExceptTerm (nonce _) ?t ?t] => constructor; auto
           |[ |- FreshExceptTerm (nonce _) (nonce _) _] => constructor; auto
           |[ |- FreshExcept _ [] _] => apply frenil
           |[ |- FreshExcept _ (?h :: ?l) _] => apply freConc; auto
           | |- FreshExceptTerm _ (?c _ ) _ => apply fretFAdv; ProveContext; auto
           | |- FreshExceptTerm _ ?t _ => apply fretCAdv; ProveContext; auto
                   end).


(* tests *)
Goal  forall (n : nat) (p : ppt -> ppt), FreshExcept (nonce n) [p (nonce n)] (p (nonce n)).
  ProveFreshExcept. Qed.

Goal  forall (n : nat) (p s: ppt -> ppt), FreshExcept (nonce n) [p (nonce n)] (p (nonce n)).
  ProveFreshExcept. Qed.


(*IND-CPA property length omitted for now*)
Definition CPA (e d p s r: list ppt -> ppt) : Prop :=
  forall (n n1 n2: nat) (t: list ppt -> list ppt) (u u' : ppt), n1<>n ->
    FreshExcept (nonce n) ((t [e [u  ; p [nonce n]; r [nonce n1]]])
                        ++ (t [e [u' ; p [nonce n]; r [nonce n1]]]))
                (p [nonce n]) ->
      Fresh (nonce n1) ((t [e [u  ; p [nonce n]; r [nonce n2]]])
                     ++ (t [e [u' ; p [nonce n]; r [nonce n2]]]))
                 ->
    t [e [u  ; p [nonce n]; r [nonce n1]]]
   ~
    t [e [u' ; p [nonce n]; r [nonce n1]]].
