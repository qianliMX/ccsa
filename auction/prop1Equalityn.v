(************************************************************************)
(* Copyright (c) 2021, Gergei Bana, Qianli Zhang                        *)
(*                                                                      *)
(* This work is licensed under the MIT license. The license is          *)
(* described in the file "LICENSE" available at the root of the source  *)
(* or at https://opensource.org/licenses/MIT                            *)
(************************************************************************)

(*   Unicode used in this file:
 *   1.'｛' :     U+FF5B
 *   2.'｝' :     U+FF5D
 *   3.'ˆ'  :     U+02C6
 *   4.'Ｈ' :     U+03C0
 *   5.'⓪' :     U+24EA
     6. 'τ' :     U+03C4*)

Require Import Coq.Lists.List.
Require Import Coq.micromega.Lia.
Import ListNotations.
Require Export AuxiliaryTheoremEquality.


(**************************
 **   Nonce management   **
 **************************)

(* There should be separate key generation for encryption and
signature like in Ali's thesis *)

Notation pk0  := (Pkey [nonce 0]).
Notation sk0  := (Skey [nonce 0]).
Notation vk0  := (Pkey [nonce 1]).
Notation ssk0 := (Skey [nonce 1]).
Notation r0   := (Rand [nonce 2]).
Notation rs0  := (Rand [nonce 3]).

Notation pk1  := (Pkey [nonce 4]).
Notation sk1  := (Skey [nonce 4]).
Notation vk1  := (Pkey [nonce 5]).
Notation ssk1 := (Skey [nonce 5]).
Notation r1   := (Rand [nonce 6]).
Notation rs1  := (Rand [nonce 7]).


Notation pkRA  := (Pkey [nonce 8]).
Notation skRA  := (Skey [nonce 8]).
Notation vkRA  := (Pkey [nonce 9]).
Notation sskRA := (Skey [nonce 9]).

Notation rsRA  := (Rand [nonce 11]).


Notation pkS  := (Pkey [nonce 12]).
Notation skS  := (Skey [nonce 12]).
Notation vkS  := (Pkey [nonce 13]).
Notation sskS := (Skey [nonce 13]).
Notation rS   := (Rand [nonce 14]).
Notation rsS  := (Rand [nonce 15]).




(**********************************
 ** Fundamental Function Symbols **
 **********************************)

Parameter Signs : Symbols Deterministic (narg 3).
Notation "'Sign'" := (FuncInt Deterministic (narg 3) Signs).
Notation "'｛' m '｝_' sk 'ˆ' r " := (Sign [m; sk; r]) (at level 101, left associativity).

Parameter Vers : Symbols Deterministic (narg 3).
Notation "'Ver'" := (FuncInt Deterministic (narg 3) Vers).

Parameter Hashs : Symbols Deterministic (narg 1).
Notation "'Hash'" := (FuncInt Deterministic (narg 1) Hashs).
Notation "'Ｈ' m " := (Hash [m]) (at level 101, left associativity).

Parameter Zeros : Symbols Deterministic (narg 1).
Notation "'Zero'" := (FuncInt Deterministic (narg 1) Zeros).
Notation "'⓪' m " := (Hash [m]) (at level 101, left associativity).

Parameter Triples : Symbols Deterministic (narg 3).
Notation "'Triple'" := (FuncInt Deterministic (narg 3) Triples).

Parameter Taus1 : Symbols Deterministic (narg 1).
Notation "'Tau1'" := (FuncInt Deterministic (narg 1) Taus1).
Notation τ1 x := (Tau1 [x]).
Parameter Taus2 : Symbols Deterministic (narg 1).
Notation "'Tau2'" := (FuncInt Deterministic (narg 1) Taus2).
Notation τ2 x := (Tau2 [x]).
Parameter Taus3 : Symbols Deterministic (narg 1).
Notation "'Tau3'" := (FuncInt Deterministic (narg 1) Taus3).
Notation τ3 x := (Tau3 [x]).

Parameter Bidding0s : Symbols Deterministic (narg 0).
Notation bd0 := (ConstInt Deterministic Bidding0s).

Parameter Bidding1s : Symbols Deterministic (narg 0).
Notation bd1 := (ConstInt Deterministic Bidding1s).


(*******************
 **   Notations   **
 *******************)
(*1. how the priority works?
  2. how to use overload
  3. left or right associativity?
  4. difference of '' and no ''?*)

Notation id0 := (＜pk0 , vk0＞).
Notation id1 := (＜pk1,  vk1＞).

Inductive iIndex : Set :=
| I0
| I1.

Definition pk (i : iIndex) : ppt :=
  match i with
  | I0 => pk0
  | I1 => pk1
  end.

Definition sk (i : iIndex) : ppt :=
  match i with
  | I0 => sk0
  | I1 => sk1
  end.

Definition vk (i : iIndex) : ppt :=
  match i with
  | I0 => vk0
  | I1 => vk1
  end.

Definition ssk (i : iIndex) : ppt :=
  match i with
  | I0 => ssk0
  | I1 => ssk1
  end.

Definition r (i : iIndex) : ppt :=
  match i with
  | I0 => r0
  | I1 => r1
  end.

Definition rs (i : iIndex) : ppt :=
  match i with
  | I0 => rs0
  | I1 => rs1
  end.

Inductive kIndex : Set :=
| K1
| K2
| K3.


Definition tau (k : kIndex) : list ppt -> ppt :=
  match k with
  | K1 => Tau1
  | K2 => Tau2
  | K3 => Tau3
  end.

Inductive xIndex : Set :=
| X1
| X2
| X3
| X4.



Notation "'＜' c1 ',' c2 '＞'" := (Pair [c1; c2]) (at level 100, right associativity). (* U+FF1C and U+FF1E)*)
Notation "'＜' c1 ',' c2 ',' c3 '＞'" := (Triple [c1; c2; c3]) (at level 100, right associativity). (*overload ＜,＞ notation*)

Definition aid i := (＜pk i , vk i＞).


Definition bd (i : iIndex) : ppt :=
  match i with
  | I0 => bd0
  | I1 => bd1
  end.

Notation hbd0 := (Ｈ bd0).
Notation hbd1 := (Ｈ bd1).

Definition  hbd i := (Ｈ (bd i)).


Definition X (x : xIndex) : ppt :=
  match x with
  | X1 => (hbd I0)
  | X2 => (hbd I1)
  | X3 => (Zero [hbd I0])
  | X4 => (Zero [hbd I1])
  end.

(* Don't have to use CCA2M *)
Definition cca_2 := (CCA2M Len Enc Dec Pkey Skey Rand Error 1 1).
Definition cca_2nolength := (CCA2Mnolength  Enc Dec Pkey Skey Rand Error 1 1).

Definition  Plain1 i x := ＜ aid i,  X x, ｛X x｝_(ssk i) ˆ (rs i) ＞.

Definition  B1 i x := ❴＜ aid i,  X x, ｛X x｝_(ssk i) ˆ (rs i) ＞❵_pkRA ＾ (r i).



(**********************
 ** Auxiliary Axioms **
 **********************)

(* Fix nonces! *)

Axiom correctness :  forall {m n r}, Ver [m; ｛m｝_ (Skey [nonce n]) ˆ (Rand [nonce r]);  Pkey [nonce n]] = TRue.


(***********************
 **   Proposition 1   **
 ***********************)


Theorem prop1: cca_2 ->
  [X X1;  X X2;  B1 I0 X1;  B1 I1 X2]
 ~
  [X X1;  X X2;  B1 I0 X2;  B1 I1 X1].
Proof.
  intros cca2.
  
(* Claim1: t1[x1 ,h(b1)] is (npkRA, r0, h(b0), h(b1)) −- CCA2 compliant *)
  pose (cca2 [nonce 8] [nonce 2] (fun x => [X X1; X X2; x; B1 I1 X2])
             (＜aid I0, X X1,｛X X1｝_(ssk I0)ˆ(rs I0)＞) (＜aid I0, X X2,｛X X2｝_(ssk I0)ˆ(rs I0)＞)) as claim1.
    repeat rewrite EqLen in claim1.
    repeat rewrite If_true in claim1. (*Using cca_2nolength could avoid this two step*)
    
  rewrite claim1.
  clear claim1.

(* Claim2: t1[h(b1), x2] is (npkRA, r1, h(b1), h(b0)) −- CCA2 compliant *)
  pose (cca2 [nonce 8] [nonce 6] (fun x => [X X1; X X2; B1 I0 X2; x])
             (＜aid I1, X X2,｛X X2｝_(ssk I1)ˆ(rs I1)＞) (＜aid I1, X X1,｛X X1｝_(ssk I1)ˆ(rs I1)＞)) as claim2.
    repeat rewrite EqLen in claim2.
    repeat rewrite If_true in claim2.

  rewrite claim2.
  clear claim2.

  reflexivity.

  (* cbv delta in *. *)
  
  all : try ProveCCA2M. (*Need to refine ProvePPT and ProveListFresh*)
  Admitted.
