(************************************************************************)
(* Copyright (c) 2021, Gergei Bana, Qianli Zhang                        *)   
(*                                                                      *)
(* This work is licensed under the MIT license. The license is          *)
(* described in the file "LICENSE" available at the root of the source  *)
(* or at https://opensource.org/licenses/MIT                            *)
(************************************************************************)
  
Require Export Coq.Lists.List. 
Import ListNotations.
Require Import Coq.micromega.Lia.
Require Export prop2.


(* Φ : U+03A6 *)

Axiom Ceq_ref: forall m,  ((m ≟ m) = TRue). 

Notation Dec_i i c := (Dec [c; (sk i)]).
Notation adv5 t2_vec:= (adv (5) t2_vec).

Notation Dec_RAphi i k x1 x2 := match i with I0 => Dec_RAif (B1 I0 x1) (tau k [adv2 (B1 I0 x1) (B1 I1 x2)])
                                           | I1 => Dec_RAif (B1 I1 x2) (tau k [adv2 (B1 I0 x1) (B1 I1 x2)]) end.

Notation np13  := (nonce 163).
Notation np14  := (nonce 164).
Notation np15  := (nonce 165).
Notation np16  := (nonce 166).
Notation rRA13   := (Rand [nonce 102]).
Notation rRA14   := (Rand [nonce 103]).
Notation rRA15   := (Rand [nonce 104]).
Notation rRA16   := (Rand [nonce 105]).
Definition np'  k  := match k with K1 => np11 | K2 => np13 | K3 => np15 end.
Definition np'' k  := match k with K1 => np12 | K2 => np14 | K3 => np16 end.
Definition rRA'  k  := match k with K1 => rRA11 | K2 => rRA13 | K3 => rRA15 end.
Definition rRA'' k  := match k with K1 => rRA12 | K2 => rRA14 | K3 => rRA16 end.

(* Attention:  R1 k x1 x2 ≠ Phi2 i k x1 x2.  *)
Definition Phi2 i k x1 x2 :=
        (If C1 k x1 x2 Then ＜❴＜Pseudonym [pk0; vk0  ; (np' k)], x1 ＞❵_pk0 ＾ (rRA' k) ,
                            ｛❴＜Pseudonym [pk0; vk0  ; (np' k)], x1 ＞❵_pk0 ＾ (rRA' k) ｝_sskRA ˆ rsRA ＞ (*use np11 and rRA11 here*)
   Else (If C2 k x1 x2 Then ＜❴＜Pseudonym [pk1; vk1  ; np'' k], x2 ＞❵_pk1 ＾ (rRA'' k) ,
                            ｛❴＜Pseudonym [pk1; vk1  ; np'' k], x2 ＞❵_pk1 ＾ (rRA'' k) ｝_sskRA ˆ rsRA ＞(*use np12 and rRA12 here*)
   Else (If Ver [τ2 (Dec_RAphi i k x1 x2); τ3 (Dec_RAphi i k x1 x2); π2 τ1 (Dec_RAphi i k x1 x2)]
   Then ＜ (❴ ＜ Pseudonym [π1 τ1 (Dec_RAphi i k x1 x2); π2 τ1 (Dec_RAphi i k x1 x2); np1], τ2 (Dec_RAphi i k x1 x2) ＞ ❵_ π1 τ1 (Dec_RAphi i k x1 x2) ＾ rRA1), 
         (｛❴ ＜ Pseudonym [π1 τ1 (Dec_RAphi i k x1 x2); π2 τ1 (Dec_RAphi i k x1 x2); np1], τ2 (Dec_RAphi i k x1 x2) ＞ ❵_ π1 τ1 (Dec_RAphi i k x1 x2) ＾ rRA1 ｝_ sskRA ˆ rsRA) ＞ Else Error))).


Definition t2_vec i x1 x2 := [ bd I0 ; bd I1 ; B1 I0 (Ｈ x1) ; B1 I1 (Ｈ x2) ; Phi2 i K1 (Ｈ x1) (Ｈ x2)].

Definition t1 k x1 := (❴＜Pseudonym [pk0; vk0; np'  k], (Ｈ x1) ＞❵_pk0 ＾ (rRA'  k)).
Definition t2 k x2 := (❴＜Pseudonym [pk1; vk1; np'' k], (Ｈ x2) ＞❵_pk1 ＾ (rRA'' k)).

Definition C21 i k x1 x2 := ((π1 (adv5 (t2_vec i x1 x2))) ≟ (t1 k x1)).
Definition C22 i k x1 x2 := ((π1 (adv5 (t2_vec i x1 x2))) ≟ (t2 k x2)).


Definition B2 i k x1 x2 := (If Ver [ π1 (adv5 (t2_vec i x1 x2)); ｛(t1 k x1)｝_sskRA ˆ rsRA; vkRA] (*how to use i to index x?*)
                             Then If (Ｈ x1) ≟ π2 (Dec_i i (π1 (adv5 (t2_vec i x1 x2)))) (* (H x1) or (H (bd i))?*)
                                     Then ＜ (  ❴ x1 ❵_pkS ＾ (r3 i)),
                                             (｛❴ x1 ❵_pkS ＾ (r3 i)｝_(ssk i) ˆ (rs3 i)),
                                             ( π1 (Dec_i i (π1 (adv5 (t2_vec i x1 x2))))) ＞
                                     Else Error                                                  
                             Else Error).

Notation Dec_iif i x d := (If (d ≟ x) Then Error Else Dec_i i d).

(* This is essentially Ali's lemma3. *)
Lemma prop3_lemma3: forall k x1 x2,
    B2 I0 k x1 x2 =  If C21 I0 k x1 x2 Then ＜ ❴ x1 ❵_ pkS ＾ (r3 I0), ｛❴ x1 ❵_ pkS ＾ (r3 I0)｝_ ssk0 ˆ (rs3 I0), Pseudonym [pk0; vk0; np' k] ＞
                   Else (If Ver [π1 (adv5 (t2_vec I0 x1 x2)); (｛ t1 k x1 ｝_sskRA ˆ rsRA); vkRA] (*No decryption here*)
                             Then If ((Ｈ x1) ≟ (π2 (Dec_iif I0 (t1 k x1) (π1 (adv5 (t2_vec I0 x1 x2))))))
                                     Then ＜ (  ❴ x1 ❵_pkS ＾ (r3 I0)), 
                                             (｛❴ x1 ❵_pkS ＾ (r3 I0)｝_(ssk I0) ˆ (rs3 I0)),
                                             ( π1 (Dec_iif I0 (t1 k x1) (π1 (adv5 (t2_vec I0 x1 x2))))) ＞
                                     Else Error                                                  
                             Else Error).
Proof.
  intros. unfold C21. simpl.
(*right: (π1 adv5 (t2_vec I0 x1 x2)) ≟ (π1 t1 k x1)
  wrong: (π1 adv5 (t2_vec I0 x1 x2) ≟ (π1 t1 k x1)) *)
  rewrite (@If_eval (fun _ => _)
                    (fun y =>  If Ver _  Then If Ｈ x1 ≟ (π2 If y Then Error Else _ )
                               Then ＜ _ , _ ,  π1 If y Then Error Else _ ＞ Else Error Else Error)
                    ((π1 adv5 (t2_vec I0 x1 x2)) ≟ (t1 k x1))).
  rewrite If_false.
  rewrite <- (@If_same (C21 I0 k x1 x2) (B2 I0 k x1 x2)) at 1.
  rewrite (Eq_branch (π1 (adv5 (t2_vec I0 x1 x2))) (t1 k x1)
                   (fun x:ppt => If Ver [ x; ｛(t1 k x1)｝_sskRA ˆ rsRA; vkRA] (*how to use i to index x?*)
                             Then If (Ｈ x1) ≟ π2 (Dec_i I0 x) (* (H x1) or (H (bd i))?*)
                                     Then ＜ (  ❴ x1 ❵_pkS ＾ (r3 I0)),
                                             (｛❴ x1 ❵_pkS ＾ (r3 I0)｝_(ssk I0) ˆ (rs3 I0)),
                                             ( π1 (Dec_i I0 (x))) ＞
                                     Else Error                                                  
                             Else Error)); simpl.
  unfold t1 at 2 3 4 5. 
    rewrite correctness.
    rewrite If_true.
  destruct k;
    simpl;
    rewrite decenc; 
    rewrite proj2pair;
    rewrite proj1pair;
    rewrite Ceq_ref;
    rewrite If_true;
    reflexivity.
 all:  ProveContext. Qed.




(*npk0*)
Definition prop2_claim1 x1 := ([bd I0; bd I1; B1 I0 hbd0; B1 I1 hbd1;
  If C1 K1 hbd0 hbd1 Then ＜ x1, ｛ x1 ｝_ sskRA ˆ rsRA ＞
  Else If C2 K1 hbd0 hbd1 Then ＜ ❴ ＜ Pseudonym [pk1; vk1; np12], hbd1 ＞ ❵_ pk1 ＾ rRA12, ｛ ❴ ＜ Pseudonym [pk1; vk1; np12], hbd1 ＞ ❵_ pk1 ＾ rRA12 ｝_ sskRA ˆ rsRA ＞
       Else If Ver
                 [τ2 (Dec_RAif (B1 I0 hbd0) (tau K1 [adv2 (B1 I0 hbd0) (B1 I1 hbd1)])); τ3 (Dec_RAif (B1 I0 hbd0) (tau K1 [adv2 (B1 I0 hbd0) (B1 I1 hbd1)]));
                 π2 τ1 (Dec_RAif (B1 I0 hbd0) (tau K1 [adv2 (B1 I0 hbd0) (B1 I1 hbd1)]))]
            Then ＜ ❴ ＜ Pseudonym [π1 τ1 (Dec_RAif (B1 I0 hbd0) (tau K1 [adv2 (B1 I0 hbd0) (B1 I1 hbd1)])); π2 τ1 (Dec_RAif (B1 I0 hbd0) (tau K1 [adv2 (B1 I0 hbd0) (B1 I1 hbd1)])); np1],
                     τ2 (Dec_RAif (B1 I0 hbd0) (tau K1 [adv2 (B1 I0 hbd0) (B1 I1 hbd1)])) ＞ ❵_ π1 τ1 (Dec_RAif (B1 I0 hbd0) (tau K1 [adv2 (B1 I0 hbd0) (B1 I1 hbd1)])) ＾ rRA1,
                 ｛ ❴ ＜ Pseudonym [π1 τ1 (Dec_RAif (B1 I0 hbd0) (tau K1 [adv2 (B1 I0 hbd0) (B1 I1 hbd1)])); π2 τ1 (Dec_RAif (B1 I0 hbd0) (tau K1 [adv2 (B1 I0 hbd0) (B1 I1 hbd1)])); np1],
                     τ2 (Dec_RAif (B1 I0 hbd0) (tau K1 [adv2 (B1 I0 hbd0) (B1 I1 hbd1)])) ＞ ❵_ π1 τ1 (Dec_RAif (B1 I0 hbd0) (tau K1 [adv2 (B1 I0 hbd0) (B1 I1 hbd1)])) ＾ rRA1 ｝_ sskRA ˆ rsRA ＞ Else Error]).

(*npkRA*)
Definition prop2_claim1' x1 := ([bd I0; bd I1; x1; B1 I1 hbd1;
  If tau K1 [adv2 (x1) (B1 I1 hbd1)] ≟ x1
  Then ＜ ❴ ＜ Pseudonym [pk0; vk0; np11], hbd1 ＞ ❵_ pk0 ＾ rRA11, ｛ ❴ ＜ Pseudonym [pk0; vk0; np11], hbd1 ＞ ❵_ pk0 ＾ rRA11 ｝_ sskRA ˆ rsRA ＞
  Else If tau K1 [adv2 x1 (B1 I1 hbd1)] ≟ B1 I1 hbd1
       Then ＜ ❴ ＜ Pseudonym [pk1; vk1; np12], hbd1 ＞ ❵_ pk1 ＾ rRA12, ｛ ❴ ＜ Pseudonym [pk1; vk1; np12], hbd1 ＞ ❵_ pk1 ＾ rRA12 ｝_ sskRA ˆ rsRA ＞
       Else If Ver
                 [τ2 (Dec_RAif (x1) (tau K1 [adv2 (x1) (B1 I1 hbd1)])); τ3 (Dec_RAif (x1) (tau K1 [adv2 (x1) (B1 I1 hbd1)]));
                 π2 τ1 (Dec_RAif (x1) (tau K1 [adv2 (x1) (B1 I1 hbd1)]))]
            Then ＜ ❴ ＜ Pseudonym
                         [π1 τ1 (Dec_RAif (x1) (tau K1 [adv2 (x1) (B1 I1 hbd1)]));
                         π2 τ1 (Dec_RAif (x1) (tau K1 [adv2 (x1) (B1 I1 hbd1)])); np1],
                     τ2 (Dec_RAif (x1) (tau K1 [adv2 (x1) (B1 I1 hbd1)])) ＞
                   ❵_ π1 τ1 (Dec_RAif (x1) (tau K1 [adv2 (x1) (B1 I1 hbd1)])) ＾ rRA1,
                 ｛ ❴ ＜ Pseudonym
                         [π1 τ1 (Dec_RAif (x1) (tau K1 [adv2 (x1) (B1 I1 hbd1)]));
                         π2 τ1 (Dec_RAif (x1) (tau K1 [adv2 (x1) (B1 I1 hbd1)])); np1],
                     τ2 (Dec_RAif (x1) (tau K1 [adv2 (x1) (B1 I1 hbd1)])) ＞
                   ❵_ π1 τ1 (Dec_RAif (x1) (tau K1 [adv2 (x1) (B1 I1 hbd1)])) ＾ rRA1 ｝_ sskRA ˆ rsRA ＞ Else Error]) .

(*npk1*)
Definition prop2_claim2 x2 := ( [bd I0; bd I1; B1 I0 hbd1; B1 I1 hbd0;
  If C1 K1 hbd1 hbd0 Then ＜ ❴ ＜ Pseudonym [pk0; vk0; np11], hbd1 ＞ ❵_ pk0 ＾ rRA11, ｛ ❴ ＜ Pseudonym [pk0; vk0; np11], hbd1 ＞ ❵_ pk0 ＾ rRA11 ｝_ sskRA ˆ rsRA ＞
  Else If C2 K1 hbd1 hbd0 Then ＜ x2, ｛ x2 ｝_ sskRA ˆ rsRA ＞
       Else If Ver
                 [τ2 (Dec_RAif (B1 I1 hbd0) (tau K1 [adv2 (B1 I0 hbd1) (B1 I1 hbd0)])); τ3 (Dec_RAif (B1 I1 hbd0) (tau K1 [adv2 (B1 I0 hbd1) (B1 I1 hbd0)]));
                 π2 τ1 (Dec_RAif (B1 I1 hbd0) (tau K1 [adv2 (B1 I0 hbd1) (B1 I1 hbd0)]))]
            Then ＜ ❴ ＜ Pseudonym
                         [π1 τ1 (Dec_RAif (B1 I1 hbd0) (tau K1 [adv2 (B1 I0 hbd1) (B1 I1 hbd0)]));
                         π2 τ1 (Dec_RAif (B1 I1 hbd0) (tau K1 [adv2 (B1 I0 hbd1) (B1 I1 hbd0)])); np1],
                     τ2 (Dec_RAif (B1 I1 hbd0) (tau K1 [adv2 (B1 I0 hbd1) (B1 I1 hbd0)])) ＞
                   ❵_ π1 τ1 (Dec_RAif (B1 I1 hbd0) (tau K1 [adv2 (B1 I0 hbd1) (B1 I1 hbd0)])) ＾ rRA1,
                 ｛ ❴ ＜ Pseudonym
                         [π1 τ1 (Dec_RAif (B1 I1 hbd0) (tau K1 [adv2 (B1 I0 hbd1) (B1 I1 hbd0)]));
                         π2 τ1 (Dec_RAif (B1 I1 hbd0) (tau K1 [adv2 (B1 I0 hbd1) (B1 I1 hbd0)])); np1],
                     τ2 (Dec_RAif (B1 I1 hbd0) (tau K1 [adv2 (B1 I0 hbd1) (B1 I1 hbd0)])) ＞
                   ❵_ π1 τ1 (Dec_RAif (B1 I1 hbd0) (tau K1 [adv2 (B1 I0 hbd1) (B1 I1 hbd0)])) ＾ rRA1 ｝_ sskRA ˆ rsRA ＞ Else Error]).

(*npkRA*)
Definition prop2_claim2' x2 := ([bd I0; bd I1; B1 I0 hbd1; x2;
  If tau K1 [adv2 (B1 I0 hbd1) x2] ≟ B1 I0 hbd1
  Then ＜ ❴ ＜ Pseudonym [pk0; vk0; np11], hbd1 ＞ ❵_ pk0 ＾ rRA11, ｛ ❴ ＜ Pseudonym [pk0; vk0; np11], hbd1 ＞ ❵_ pk0 ＾ rRA11 ｝_ sskRA ˆ rsRA ＞
  Else If tau K1 [adv2 (B1 I0 hbd1) (x2)] ≟ x2
       Then ＜ ❴ ＜ Pseudonym [pk1; vk1; np12], hbd1 ＞ ❵_ pk1 ＾ rRA12, ｛ ❴ ＜ Pseudonym [pk1; vk1; np12], hbd1 ＞ ❵_ pk1 ＾ rRA12 ｝_ sskRA ˆ rsRA ＞
       Else If Ver
                 [τ2 (Dec_RAif (x2) (tau K1 [adv2 (B1 I0 hbd1) (x2)])); τ3 (Dec_RAif (x2) (tau K1 [adv2 (B1 I0 hbd1) (x2)]));
                 π2 τ1 (Dec_RAif (x2) (tau K1 [adv2 (B1 I0 hbd1) (x2)]))]
            Then ＜ ❴ ＜ Pseudonym
                         [π1 τ1 (Dec_RAif (x2) (tau K1 [adv2 (B1 I0 hbd1) (x2)]));
                         π2 τ1 (Dec_RAif (x2) (tau K1 [adv2 (B1 I0 hbd1) (x2)])); np1],
                     τ2 (Dec_RAif (x2) (tau K1 [adv2 (B1 I0 hbd1) (x2)])) ＞
                   ❵_ π1 τ1 (Dec_RAif (x2) (tau K1 [adv2 (B1 I0 hbd1) (x2)])) ＾ rRA1,
                 ｛ ❴ ＜ Pseudonym
                         [π1 τ1 (Dec_RAif (x2) (tau K1 [adv2 (B1 I0 hbd1) (x2)]));
                         π2 τ1 (Dec_RAif (x2) (tau K1 [adv2 (B1 I0 hbd1) (x2)])); np1],
                     τ2 (Dec_RAif (x2) (tau K1 [adv2 (B1 I0 hbd1) (x2)])) ＞
                   ❵_ π1 τ1 (Dec_RAif (x2) (tau K1 [adv2 (B1 I0 hbd1) (x2)])) ＾ rRA1 ｝_ sskRA ˆ rsRA ＞ Else Error]).
  
(* This is a brother of prop2_small *)
Proposition prop2_weak: cca_2 ->
  [ bd I0 ; bd I1 ; B1 I0 hbd0 ; B1 I1 hbd1 ; Phi2 I0 K1 hbd0 hbd1]
 ~
  [ bd I0 ; bd I1 ; B1 I0 hbd1 ; B1 I1 hbd0 ; Phi2 I1 K1 hbd1 hbd0].
Proof.
  intros cca2. unfold cca_2 in cca2.
  apply CCA2toCCA2L in cca2.

(* Claim1: t2[x1 ,h(b1)] is (npk0, rRA11, h(b0), h(b1)) −- CCA2 compliant. npk0 := nonce 0, rRA11 := nonce 101 *)
  pose (cca2 [nonce 0] [nonce 101]
             (fun x1 => prop2_claim1 x1)
       (＜ Pseudonym [pk0; vk0; np11], hbd0 ＞) (＜ Pseudonym [pk0; vk0; np11], hbd1 ＞)) as claim1.
  rewrite claim1.
  clear claim1.
  
(* Claim1': t2[x1 ,h(b1)] is (npkRA, r0, h(b0), h(b1)) −- CCA2 compliant. npkRA := nonce 8, r0 := nonce 2 *)
  pose (cca2 [nonce 8] [nonce 2]
             (fun x1 => prop2_claim1' x1) 
       (＜ aid I0, hbd0, ｛ hbd0 ｝_ ssk0 ˆ rs0 ＞) (＜ aid I0, hbd1, ｛ hbd1 ｝_ ssk0 ˆ rs0 ＞)) as claim1'.
  rewrite claim1'.
  clear claim1'.

  
(* Claim2: t2[h(b1), x2] is (npk1, rRA12, h(b0), h(b1)) −- CCA2 compliant. npk1 := nonce 4, rRA12 := nonce 191*)
  pose (cca2 [nonce 4] [nonce 191]
             (fun x2 => prop2_claim2 x2)
       (＜ Pseudonym [pk1; vk1; np12], hbd0 ＞) (＜ Pseudonym [pk1; vk1; np12], hbd1 ＞)) as claim2.
  rewrite claim2.
  clear claim2.

  
(* Claim2': t2[h(b1) ,x2] is (npkRA, r1, h(b0), h(b1)) −- CCA2 compliant.  npkRA := nonce 8, r0 := nonce 6*)
   pose (cca2 [nonce 8] [nonce 6]
              (prop2_claim2')
       (＜ aid I1, hbd0, ｛ hbd0 ｝_ ssk1 ˆ rs1 ＞) (＜ aid I1, hbd1, ｛ hbd1 ｝_ ssk1 ˆ rs1 ＞)) as claim2'.
  rewrite claim2'.
  clear claim2'.
  

  unfold prop2_claim1'.
  rewrite (@If_eval (fun _ => _ )
                    (fun x => If _ Then _
                              Else If Ver [τ2 (If x Then Error Else _ ); τ3 (If x Then Error Else _ ); π2 τ1 (If x Then Error Else _ )]
                              Then ＜ ❴ ＜ Pseudonym [π1 τ1 (If x Then Error Else _ ); π2 τ1 (If x Then Error Else _ ); np1],  τ2 (If x Then Error Else _ ) ＞
                                      ❵_ π1 τ1 (If x Then Error Else _ ) ＾ rRA1,
                           ｛❴ ＜ Pseudonym [π1 τ1 (If x Then Error Else _ ); π2 τ1 (If x Then Error Else _ ); np1],  τ2 (If x Then Error Else _ ) ＞
                             ❵_ π1 τ1 (If x Then Error Else _ ) ＾ rRA1 ｝_ sskRA ˆ rsRA ＞ Else Error)
                    (C1 K1 hbd1 hbd1)).
  rewrite If_false.

  unfold prop2_claim2'.
  rewrite (@If_eval (fun _ => _ )
                    (fun x => If Ver [τ2 (If x Then Error Else _ ); τ3 (If x Then Error Else _ ); π2 τ1 (If x Then Error Else _ )]
                              Then ＜ ❴ ＜ Pseudonym [π1 τ1 (If x Then Error Else _ ); π2 τ1 (If x Then Error Else _ ); np1],  τ2 (If x Then Error Else _ ) ＞
                                      ❵_ π1 τ1 (If x Then Error Else _ ) ＾ rRA1,
                           ｛❴ ＜ Pseudonym [π1 τ1 (If x Then Error Else _ ); π2 τ1 (If x Then Error Else _ ); np1],  τ2 (If x Then Error Else _ ) ＞
                             ❵_ π1 τ1 (If x Then Error Else _ ) ＾ rRA1 ｝_ sskRA ˆ rsRA ＞ Else Error)
                    (C2 K1 hbd1 hbd1)).
  rewrite If_false.
  
  reflexivity.

  all: try simpl; ProveContext.
  all : try ProveNonceList.
  all : try ProveListFresh.
  all : try lia.
  Focus 13. clear. unfold prop2_claim2.  time ProveCca2After.
  Focus 19. clear. unfold prop2_claim1'. time ProveCca2After.
  Focus 25. clear. unfold prop2_claim1.  time ProveCca2After.
  Focus 6. clear. unfold prop2_claim2'.  time ProveCca2After.
  all : try ProveCca2Before.
  all : try constructor.
Admitted.



  
Proposition prop3_small: cca_2 ->
  [ bd I0 ; bd I1 ; B1 I0 hbd0 ; B1 I1 hbd1 ; Phi2 I0 K1 hbd0 hbd1; B2 I0 K1 bd0 bd1]
 ~
  [ bd I0 ; bd I1 ; B1 I0 hbd1 ; B1 I1 hbd0 ; Phi2 I1 K1 hbd1 hbd0; B2 I0 K1 bd1 bd0].
Proof.
  intros cca2.
  unfold cca_2 in cca2.
  apply CCA2toCCA2L in cca2.

(* Claim1:   t2[x1 ,h(b1)] is (npkS, r30, h(b0), h(b1)) −- CCA2 compliant. npkS := nonce 12, r30 := nonce 200 *)
  rewrite prop3_lemma3 with (k := K1). 
  pose (cca2 [nonce 12] [nonce 200]
             (fun x1 => [ bd I0; bd I1; B1 I0 hbd0; B1 I1 hbd1; Phi2 I0 K1 hbd0 hbd1;
                     If C21 I0 K1 bd0 bd1 Then ＜ x1, ｛ x1 ｝_ ssk0 ˆ rs3 I0, Pseudonym [pk0; vk0; np' K1] ＞
                     Else If Ver [π1 adv5 (t2_vec I0 bd0 bd1); ｛ t1 K1 bd0 ｝_sskRA ˆ rsRA ; vkRA]
                          Then If hbd0 ≟ (π2 Dec_iif I0 (t1 K1 bd0) (π1 adv5 (t2_vec I0 bd0 bd1)))
                     Then ＜ x1, ｛ x1 ｝_ ssk I0 ˆ rs3 I0, π1 Dec_iif I0 (t1 K1 bd0) (π1 adv5 (t2_vec I0 bd0 bd1)) ＞
                     Else Error Else Error])
       (bd0) (bd1)) as claim1.
  rewrite claim1.
  clear claim1.
  (* Focus 9. clear. unfold C21. unfold t2_vec. simpl. time ProveCca2After.  Without unfold t2_vec, the time will be 8.767. Or will be 45.419*)

(* Claim1':  t2[x1 ,h(b1)] is (npk0, rRA11, h(b0), h(b1)) −- CCA2 compliant. npk0 := nonce 0, rRA11 := nonce 101 *)
  unfold C21. unfold t2_vec. simpl.
  pose (cca2 [nonce 0] [nonce 101]
             (fun x1 => (prop2_claim1 x1)
                       ++ [If (π1 adv5 (prop2_claim1 x1)) ≟ x1
                              Then ＜ ❴ bd1 ❵_ pkS ＾ r30, ｛ ❴ bd1 ❵_ pkS ＾ r30 ｝_ ssk0 ˆ rs30, Pseudonym [pk0; vk0; np11] ＞
                              Else If Ver [π1 adv5 (prop2_claim1 x1); ｛ x1 ｝_ sskRA ˆ rsRA; vkRA]
                              Then If hbd0  ≟ (π2 If (π1 adv5 (prop2_claim1 x1)) ≟ x1 Then Error Else Dec [π1 adv5 (prop2_claim1 x1); sk0])
                              Then ＜ ❴ bd1 ❵_ pkS ＾ r30, ｛ ❴ bd1 ❵_ pkS ＾ r30 ｝_ ssk0 ˆ rs30,
                           π1 If (π1 adv5 (prop2_claim1 x1)) ≟ x1 Then Error
                              Else Dec [π1 adv5 (prop2_claim1 x1); sk0] ＞ Else Error Else Error])
       (＜ Pseudonym [pk0; vk0; np11], hbd0 ＞) (＜ Pseudonym [pk0; vk0; np11], hbd1 ＞)) as claim1'.
  rewrite claim1'.
  clear claim1'.
  (* Focus 9. clear. unfold prop2_claim1. simpl. time ProveCca2After. 21.714 not so slow as I thought*)
  

  simpl.
  rewrite (@If_eval (fun _ => _ )
                    (fun x => If Ver [π1 adv5 (prop2_claim1 (t1 K1 bd1)); ｛ t1 K1 bd1 ｝_ sskRA ˆ rsRA; vkRA]
                              Then If hbd0  ≟ (π2 If x Then Error Else _ )
                              Then ＜ ❴ bd1 ❵_ pkS ＾ r30, ｛ ❴ bd1 ❵_ pkS ＾ r30 ｝_ ssk0 ˆ rs30, π1 If x Then Error Else _ ＞ Else Error Else Error)
                    ((π1 adv5 (prop2_claim1 (t1 K1 bd1))) ≟ (t1 K1 bd1))).
  rewrite If_false.


(* Claim1'': t2[x1 ,h(b1)] is (npkRA, r0, h(b0), h(b1)) −- CCA2 compliant. npkRA := nonce 8, r0 := nonce 2 *)
  pose (cca2 [nonce 8] [nonce 2]
             (fun x1 => (prop2_claim1' x1) ++
                                        [If (π1 adv5 (prop2_claim1' x1)) ≟  t1 K1 bd1
                                            Then ＜ ❴ bd1 ❵_ pkS ＾ r30, ｛ ❴ bd1 ❵_ pkS ＾ r30 ｝_ ssk0 ˆ rs30, Pseudonym [pk0; vk0; np11] ＞
                                            Else If Ver [π1 adv5 (prop2_claim1' x1); ｛ t1 K1 bd1 ｝_ sskRA ˆ rsRA; vkRA]
                                            Then If hbd0 ≟ (π2 (Dec [π1 adv5 (prop2_claim1' x1); sk0]))
                                            Then ＜ ❴ bd1 ❵_ pkS ＾ r30, ｛ ❴ bd1 ❵_ pkS ＾ r30 ｝_ ssk0 ˆ rs30, π1 (Dec [π1 adv5 (prop2_claim1' x1); sk0])
                  ＞ Else Error Else Error])
       (＜ aid I0, hbd0, ｛ hbd0 ｝_ ssk0 ˆ rs0 ＞) (＜ aid I0, hbd1, ｛ hbd1 ｝_ ssk0 ˆ rs0 ＞)) as claim1''.
  rewrite claim1''.
  clear claim1''.
  (* Focus 9. clear. unfold prop2_claim1'. time ProveCca2After. only 4.865. strange.*)

  
  rewrite prop3_lemma3 with (k := K1).
  (* Claim2:   t2[h(b1) ,x2] is (npkRA, r1, h(b0), h(b1)) −- CCA2 compliant.  npkRA := nonce 8, r0 := nonce 6*)

  (* Claim2':  t2[h(b1), x2] is (npk1, rRA12, h(b0), h(b1)) −- CCA2 compliant. npk1 := nonce 4, rRA12 := nonce 191*)

  (* Claim2'': t2[h(b1) ,x2] is (npkS, r11, h(b0), h(b1)) −- CCA2 compliant. npkS := nonce 12, r11 := nonce 600 *)

(* If_eval*)

Abort.

















(*Definition t1 k x1 := (＜(❴＜Pseudonym [pk0; vk0; np' k], (Ｈ x1) ＞❵_pk0 ＾ (rRA' k)) , (*using np11 and rRA11 are better than npk and rRAk*)
                       (｛❴＜Pseudonym [pk0; vk0; np' k], (Ｈ x1) ＞❵_pk0 ＾ (rRA' k)｝_sskRA ˆ rsRA)＞).
Definition t2 k x2 := (＜(❴＜Pseudonym [pk1; vk1; np'' k], (Ｈ x2) ＞❵_pk1 ＾ (rRA'' k)) ,
                       (｛❴＜Pseudonym [pk1; vk1; np'' k], (Ｈ x2) ＞❵_pk1 ＾ (rRA'' k)｝_sskRA ˆ rsRA)＞).

Definition C21 i k x1 x2 := ((π1 (adv5 (t2_vec i x1 x2))) ≟ π1 (t1 k x1)).
Definition C22 i k x1 x2 := ((π1 (adv5 (t2_vec i x1 x2))) ≟ π1 (t2 k x2)).


Definition B2 i k x1 x2 := (If Ver [ π1 (adv5 (t2_vec i x1 x2)); π2 (t1 k x1); vkRA] (*how to use i to index x?*)
                             Then If (Ｈ x1) ≟ π2 (Dec_i i (π1 (adv5 (t2_vec i x1 x2)))) (* (H x1) or (H (bd i))?*)
                                     Then ＜ (  ❴ x1 ❵_pkS ＾ (r3 i)),
                                             (｛❴ x1 ❵_pkS ＾ (r3 i)｝_(ssk i) ˆ (rs3 i)),
                                             ( π1 (Dec_i i (π1 (adv5 (t2_vec i x1 x2))))) ＞
                                     Else Error                                                  
                             Else Error).

Notation Dec_iif i x d := (If (d ≟ x) Then Error Else Dec_i i d).

(* This is essentially Ali's lemma3. *)
Lemma prop3_lemma3: forall k x1 x2,
    B2 I0 k x1 x2 =  If C21 I0 k x1 x2 Then ＜ ❴ x1 ❵_ pkS ＾ (r3 I0), ｛❴ x1 ❵_ pkS ＾ (r3 I0)｝_ ssk0 ˆ (rs3 I0), Pseudonym [pk0; vk0; np k] ＞
                   Else (If Ver [π1 (adv5 (t2_vec I0 x1 x2)); (π2 (t1 k x1)); vkRA] (*No decryptio here*)
                             Then If ((Ｈ x1) ≟ (π2 (Dec_iif I0 (π1 (t1 k x1)) (π1 (adv5 (t2_vec I0 x1 x2))))))
                                     Then ＜ (  ❴ x1 ❵_pkS ＾ (r3 I0)), 
                                             (｛❴ x1 ❵_pkS ＾ (r3 I0)｝_(ssk I0) ˆ (rs3 I0)),
                                             ( π1 (Dec_iif I0 (π1 (t1 k x1)) (π1 (adv5 (t2_vec I0 x1 x2))))) ＞
                                     Else Error                                                  
                             Else Error).
Proof.
  intros. unfold C21. simpl.
(*right: (π1 adv5 (t2_vec I0 x1 x2)) ≟ (π1 t1 k x1)
  wrong: (π1 adv5 (t2_vec I0 x1 x2) ≟ (π1 t1 k x1)) *)
  rewrite (@If_eval (fun _ => _)
                    (fun y =>  If Ver _  Then If Ｈ x1 ≟ (π2 If y Then Error Else _ )
                               Then ＜ _ , _ ,  π1 If y Then Error Else _ ＞ Else Error Else Error)
                    ((π1 adv5 (t2_vec I0 x1 x2)) ≟ (π1 t1 k x1))).
  rewrite If_false.
  rewrite <- (@If_same (C21 I0 k x1 x2) (B2 I0 k x1 x2)) at 1.
  rewrite (Eq_branch (π1 (adv5 (t2_vec I0 x1 x2))) (π1 (t1 k x1))
                   (fun x:ppt => If Ver [ x; π2 (t1 k x1); vkRA] (*how to use i to index x?*)
                             Then If (Ｈ x1) ≟ π2 (Dec_i I0 x) (* (H x1) or (H (bd i))?*)
                                     Then ＜ (  ❴ x1 ❵_pkS ＾ (r3 I0)),
                                             (｛❴ x1 ❵_pkS ＾ (r3 I0)｝_(ssk I0) ˆ (rs3 I0)),
                                             ( π1 (Dec_i I0 (x))) ＞
                                     Else Error                                                  
                             Else Error)); simpl.
  unfold t1 at 2 3 4 5. 
    rewrite proj1pair.
    rewrite proj2pair. 
    rewrite correctness.
    rewrite If_true.
  destruct k.
    simpl.
    rewrite decenc. 
    rewrite proj2pair.
    rewrite proj1pair.
    rewrite Ceq_ref.
    rewrite If_true.
    reflexivity.
 all:  ProveContext. Qed.




(*npk0*)
Definition prop2_claim1 x1 := ([bd I0; bd I1; B1 I0 hbd0; B1 I1 hbd1;
  If C1 K1 hbd0 hbd1 Then ＜ x1, ｛ x1 ｝_ sskRA ˆ rsRA ＞
  Else If C2 K1 hbd0 hbd1 Then ＜ ❴ ＜ Pseudonym [pk1; vk1; np12], hbd1 ＞ ❵_ pk1 ＾ rRA12, ｛ ❴ ＜ Pseudonym [pk1; vk1; np12], hbd1 ＞ ❵_ pk1 ＾ rRA12 ｝_ sskRA ˆ rsRA ＞
       Else If Ver
                 [τ2 (Dec_RAif (B1 I0 hbd0) (tau K1 [adv2 (B1 I0 hbd0) (B1 I1 hbd1)])); τ3 (Dec_RAif (B1 I0 hbd0) (tau K1 [adv2 (B1 I0 hbd0) (B1 I1 hbd1)]));
                 π2 τ1 (Dec_RAif (B1 I0 hbd0) (tau K1 [adv2 (B1 I0 hbd0) (B1 I1 hbd1)]))]
            Then ＜ ❴ ＜ Pseudonym [π1 τ1 (Dec_RAif (B1 I0 hbd0) (tau K1 [adv2 (B1 I0 hbd0) (B1 I1 hbd1)])); π2 τ1 (Dec_RAif (B1 I0 hbd0) (tau K1 [adv2 (B1 I0 hbd0) (B1 I1 hbd1)])); np1],
                     τ2 (Dec_RAif (B1 I0 hbd0) (tau K1 [adv2 (B1 I0 hbd0) (B1 I1 hbd1)])) ＞ ❵_ π1 τ1 (Dec_RAif (B1 I0 hbd0) (tau K1 [adv2 (B1 I0 hbd0) (B1 I1 hbd1)])) ＾ rRA1,
                 ｛ ❴ ＜ Pseudonym [π1 τ1 (Dec_RAif (B1 I0 hbd0) (tau K1 [adv2 (B1 I0 hbd0) (B1 I1 hbd1)])); π2 τ1 (Dec_RAif (B1 I0 hbd0) (tau K1 [adv2 (B1 I0 hbd0) (B1 I1 hbd1)])); np1],
                     τ2 (Dec_RAif (B1 I0 hbd0) (tau K1 [adv2 (B1 I0 hbd0) (B1 I1 hbd1)])) ＞ ❵_ π1 τ1 (Dec_RAif (B1 I0 hbd0) (tau K1 [adv2 (B1 I0 hbd0) (B1 I1 hbd1)])) ＾ rRA1 ｝_ sskRA ˆ rsRA ＞ Else Error]).

(*npkRA*)
Definition prop2_claim1' x1 := ([bd I0; bd I1; x1; B1 I1 hbd1;
  If tau K1 [adv2 (x1) (B1 I1 hbd1)] ≟ x1
  Then ＜ ❴ ＜ Pseudonym [pk0; vk0; np11], hbd1 ＞ ❵_ pk0 ＾ rRA11, ｛ ❴ ＜ Pseudonym [pk0; vk0; np11], hbd1 ＞ ❵_ pk0 ＾ rRA11 ｝_ sskRA ˆ rsRA ＞
  Else If tau K1 [adv2 x1 (B1 I1 hbd1)] ≟ B1 I1 hbd1
       Then ＜ ❴ ＜ Pseudonym [pk1; vk1; np12], hbd1 ＞ ❵_ pk1 ＾ rRA12, ｛ ❴ ＜ Pseudonym [pk1; vk1; np12], hbd1 ＞ ❵_ pk1 ＾ rRA12 ｝_ sskRA ˆ rsRA ＞
       Else If Ver
                 [τ2 (Dec_RAif (x1) (tau K1 [adv2 (x1) (B1 I1 hbd1)])); τ3 (Dec_RAif (x1) (tau K1 [adv2 (x1) (B1 I1 hbd1)]));
                 π2 τ1 (Dec_RAif (x1) (tau K1 [adv2 (x1) (B1 I1 hbd1)]))]
            Then ＜ ❴ ＜ Pseudonym
                         [π1 τ1 (Dec_RAif (x1) (tau K1 [adv2 (x1) (B1 I1 hbd1)]));
                         π2 τ1 (Dec_RAif (x1) (tau K1 [adv2 (x1) (B1 I1 hbd1)])); np1],
                     τ2 (Dec_RAif (x1) (tau K1 [adv2 (x1) (B1 I1 hbd1)])) ＞
                   ❵_ π1 τ1 (Dec_RAif (x1) (tau K1 [adv2 (x1) (B1 I1 hbd1)])) ＾ rRA1,
                 ｛ ❴ ＜ Pseudonym
                         [π1 τ1 (Dec_RAif (x1) (tau K1 [adv2 (x1) (B1 I1 hbd1)]));
                         π2 τ1 (Dec_RAif (x1) (tau K1 [adv2 (x1) (B1 I1 hbd1)])); np1],
                     τ2 (Dec_RAif (x1) (tau K1 [adv2 (x1) (B1 I1 hbd1)])) ＞
                   ❵_ π1 τ1 (Dec_RAif (x1) (tau K1 [adv2 (x1) (B1 I1 hbd1)])) ＾ rRA1 ｝_ sskRA ˆ rsRA ＞ Else Error]) .

(*npk1*)
Definition prop2_claim2 x2 := ( [bd I0; bd I1; B1 I0 hbd1; B1 I1 hbd0;
  If C1 K1 hbd1 hbd0 Then ＜ ❴ ＜ Pseudonym [pk0; vk0; np11], hbd1 ＞ ❵_ pk0 ＾ rRA11, ｛ ❴ ＜ Pseudonym [pk0; vk0; np11], hbd1 ＞ ❵_ pk0 ＾ rRA11 ｝_ sskRA ˆ rsRA ＞
  Else If C2 K1 hbd1 hbd0 Then ＜ x2, ｛ x2 ｝_ sskRA ˆ rsRA ＞
       Else If Ver
                 [τ2 (Dec_RAif (B1 I1 hbd0) (tau K1 [adv2 (B1 I0 hbd1) (B1 I1 hbd0)])); τ3 (Dec_RAif (B1 I1 hbd0) (tau K1 [adv2 (B1 I0 hbd1) (B1 I1 hbd0)]));
                 π2 τ1 (Dec_RAif (B1 I1 hbd0) (tau K1 [adv2 (B1 I0 hbd1) (B1 I1 hbd0)]))]
            Then ＜ ❴ ＜ Pseudonym
                         [π1 τ1 (Dec_RAif (B1 I1 hbd0) (tau K1 [adv2 (B1 I0 hbd1) (B1 I1 hbd0)]));
                         π2 τ1 (Dec_RAif (B1 I1 hbd0) (tau K1 [adv2 (B1 I0 hbd1) (B1 I1 hbd0)])); np1],
                     τ2 (Dec_RAif (B1 I1 hbd0) (tau K1 [adv2 (B1 I0 hbd1) (B1 I1 hbd0)])) ＞
                   ❵_ π1 τ1 (Dec_RAif (B1 I1 hbd0) (tau K1 [adv2 (B1 I0 hbd1) (B1 I1 hbd0)])) ＾ rRA1,
                 ｛ ❴ ＜ Pseudonym
                         [π1 τ1 (Dec_RAif (B1 I1 hbd0) (tau K1 [adv2 (B1 I0 hbd1) (B1 I1 hbd0)]));
                         π2 τ1 (Dec_RAif (B1 I1 hbd0) (tau K1 [adv2 (B1 I0 hbd1) (B1 I1 hbd0)])); np1],
                     τ2 (Dec_RAif (B1 I1 hbd0) (tau K1 [adv2 (B1 I0 hbd1) (B1 I1 hbd0)])) ＞
                   ❵_ π1 τ1 (Dec_RAif (B1 I1 hbd0) (tau K1 [adv2 (B1 I0 hbd1) (B1 I1 hbd0)])) ＾ rRA1 ｝_ sskRA ˆ rsRA ＞ Else Error]).

(*npkRA*)
Definition prop2_claim2' x2 := ([bd I0; bd I1; B1 I0 hbd1; x2;
  If tau K1 [adv2 (B1 I0 hbd1) x2] ≟ B1 I0 hbd1
  Then ＜ ❴ ＜ Pseudonym [pk0; vk0; np11], hbd1 ＞ ❵_ pk0 ＾ rRA11, ｛ ❴ ＜ Pseudonym [pk0; vk0; np11], hbd1 ＞ ❵_ pk0 ＾ rRA11 ｝_ sskRA ˆ rsRA ＞
  Else If tau K1 [adv2 (B1 I0 hbd1) (x2)] ≟ x2
       Then ＜ ❴ ＜ Pseudonym [pk1; vk1; np12], hbd1 ＞ ❵_ pk1 ＾ rRA12, ｛ ❴ ＜ Pseudonym [pk1; vk1; np12], hbd1 ＞ ❵_ pk1 ＾ rRA12 ｝_ sskRA ˆ rsRA ＞
       Else If Ver
                 [τ2 (Dec_RAif (x2) (tau K1 [adv2 (B1 I0 hbd1) (x2)])); τ3 (Dec_RAif (x2) (tau K1 [adv2 (B1 I0 hbd1) (x2)]));
                 π2 τ1 (Dec_RAif (x2) (tau K1 [adv2 (B1 I0 hbd1) (x2)]))]
            Then ＜ ❴ ＜ Pseudonym
                         [π1 τ1 (Dec_RAif (x2) (tau K1 [adv2 (B1 I0 hbd1) (x2)]));
                         π2 τ1 (Dec_RAif (x2) (tau K1 [adv2 (B1 I0 hbd1) (x2)])); np1],
                     τ2 (Dec_RAif (x2) (tau K1 [adv2 (B1 I0 hbd1) (x2)])) ＞
                   ❵_ π1 τ1 (Dec_RAif (x2) (tau K1 [adv2 (B1 I0 hbd1) (x2)])) ＾ rRA1,
                 ｛ ❴ ＜ Pseudonym
                         [π1 τ1 (Dec_RAif (x2) (tau K1 [adv2 (B1 I0 hbd1) (x2)]));
                         π2 τ1 (Dec_RAif (x2) (tau K1 [adv2 (B1 I0 hbd1) (x2)])); np1],
                     τ2 (Dec_RAif (x2) (tau K1 [adv2 (B1 I0 hbd1) (x2)])) ＞
                   ❵_ π1 τ1 (Dec_RAif (x2) (tau K1 [adv2 (B1 I0 hbd1) (x2)])) ＾ rRA1 ｝_ sskRA ˆ rsRA ＞ Else Error]).
  
(* This is a brother of prop2_small *)
Proposition prop2_weak: cca_2 ->
  [ bd I0 ; bd I1 ; B1 I0 hbd0 ; B1 I1 hbd1 ; Phi2 I0 K1 hbd0 hbd1]
 ~
  [ bd I0 ; bd I1 ; B1 I0 hbd1 ; B1 I1 hbd0 ; Phi2 I1 K1 hbd1 hbd0].
Proof.
  intros cca2. unfold cca_2 in cca2.
  apply CCA2toCCA2L in cca2.

(* Claim1: t2[x1 ,h(b1)] is (npk0, rRA11, h(b0), h(b1)) −- CCA2 compliant. npk0 := nonce 0, rRA11 := nonce 101 *)
  pose (cca2 [nonce 0] [nonce 101]
             (fun x1 => prop2_claim1 x1)
       (＜ Pseudonym [pk0; vk0; np11], hbd0 ＞) (＜ Pseudonym [pk0; vk0; np11], hbd1 ＞)) as claim1.
  rewrite claim1.
  clear claim1.
  
(* Claim1': t2[x1 ,h(b1)] is (npkRA, r0, h(b0), h(b1)) −- CCA2 compliant. npkRA := nonce 8, r0 := nonce 2 *)
  pose (cca2 [nonce 8] [nonce 2]
             (fun x1 => prop2_claim1' x1) 
       (＜ aid I0, hbd0, ｛ hbd0 ｝_ ssk0 ˆ rs0 ＞) (＜ aid I0, hbd1, ｛ hbd1 ｝_ ssk0 ˆ rs0 ＞)) as claim1'.
  rewrite claim1'.
  clear claim1'.

  
(* Claim2: t2[h(b1), x2] is (npk1, rRA12, h(b0), h(b1)) −- CCA2 compliant. npk1 := nonce 4, rRA12 := nonce 191*)
  pose (cca2 [nonce 4] [nonce 191]
             (fun x2 => prop2_claim2 x2)
       (＜ Pseudonym [pk1; vk1; np12], hbd0 ＞) (＜ Pseudonym [pk1; vk1; np12], hbd1 ＞)) as claim2.
  rewrite claim2.
  clear claim2.

  
(* Claim2': t2[h(b1) ,x2] is (npkRA, r1, h(b0), h(b1)) −- CCA2 compliant.  npkRA := nonce 8, r0 := nonce 6*)
   pose (cca2 [nonce 8] [nonce 6]
              (prop2_claim2')
       (＜ aid I1, hbd0, ｛ hbd0 ｝_ ssk1 ˆ rs1 ＞) (＜ aid I1, hbd1, ｛ hbd1 ｝_ ssk1 ˆ rs1 ＞)) as claim2'.
  rewrite claim2'.
  clear claim2'.
  

  unfold prop2_claim1'.
  rewrite (@If_eval (fun _ => _ )
                    (fun x => If _ Then _
                              Else If Ver [τ2 (If x Then Error Else _ ); τ3 (If x Then Error Else _ ); π2 τ1 (If x Then Error Else _ )]
                              Then ＜ ❴ ＜ Pseudonym [π1 τ1 (If x Then Error Else _ ); π2 τ1 (If x Then Error Else _ ); np1],  τ2 (If x Then Error Else _ ) ＞
                                      ❵_ π1 τ1 (If x Then Error Else _ ) ＾ rRA1,
                           ｛❴ ＜ Pseudonym [π1 τ1 (If x Then Error Else _ ); π2 τ1 (If x Then Error Else _ ); np1],  τ2 (If x Then Error Else _ ) ＞
                             ❵_ π1 τ1 (If x Then Error Else _ ) ＾ rRA1 ｝_ sskRA ˆ rsRA ＞ Else Error)
                    (C1 K1 hbd1 hbd1)).
  rewrite If_false.

  unfold prop2_claim2'.
  rewrite (@If_eval (fun _ => _ )
                    (fun x => If Ver [τ2 (If x Then Error Else _ ); τ3 (If x Then Error Else _ ); π2 τ1 (If x Then Error Else _ )]
                              Then ＜ ❴ ＜ Pseudonym [π1 τ1 (If x Then Error Else _ ); π2 τ1 (If x Then Error Else _ ); np1],  τ2 (If x Then Error Else _ ) ＞
                                      ❵_ π1 τ1 (If x Then Error Else _ ) ＾ rRA1,
                           ｛❴ ＜ Pseudonym [π1 τ1 (If x Then Error Else _ ); π2 τ1 (If x Then Error Else _ ); np1],  τ2 (If x Then Error Else _ ) ＞
                             ❵_ π1 τ1 (If x Then Error Else _ ) ＾ rRA1 ｝_ sskRA ˆ rsRA ＞ Else Error)
                    (C2 K1 hbd1 hbd1)).
  rewrite If_false.
  
  reflexivity.

  all: try simpl; ProveContext.
  all : try ProveNonceList.
  all : try ProveListFresh.
  all : try lia.
  Focus 13. clear. unfold prop2_claim2.  time ProveCca2After.
  Focus 19. clear. unfold prop2_claim1'. time ProveCca2After.
  Focus 25. clear. unfold prop2_claim1.  time ProveCca2After.
  Focus 6. clear. unfold prop2_claim2'.  time ProveCca2After.
  all : try ProveCca2Before.
  all : try constructor.
Admitted.




Proposition prop3_small: cca_2 ->
  [ bd I0 ; bd I1 ; B1 I0 hbd0 ; B1 I1 hbd1 ; Phi2 I0 K1 hbd0 hbd1; B2 I0 K1 bd0 bd1]
 ~
  [ bd I0 ; bd I1 ; B1 I0 hbd1 ; B1 I1 hbd0 ; Phi2 I1 K1 hbd1 hbd0; B2 I0 K1 bd1 bd0].
Proof.
  intros cca2.
  unfold cca_2 in cca2.
  apply CCA2toCCA2L in cca2.

(* Claim1:   t2[x1 ,h(b1)] is (npkS, r30, h(b0), h(b1)) −- CCA2 compliant. npkS := nonce 12, r30 := nonce 200 *)
  rewrite prop3_lemma3 with (k := K1). 
  pose (cca2 [nonce 12] [nonce 200]
             (fun x1 => [ bd I0; bd I1; B1 I0 hbd0; B1 I1 hbd1; Phi2 I0 K1 hbd0 hbd1;
                     If C21 I0 K1 bd0 bd1 Then ＜ x1, ｛ x1 ｝_ ssk0 ˆ rs3 I0, Pseudonym [pk0; vk0; np K1] ＞
                     Else If Ver [π1 adv5 (t2_vec I0 bd0 bd1); π2 t1 K1 bd0; vkRA] Then If hbd0 ≟ (π2 Dec_iif I0 (π1 t1 K1 bd0) (π1 adv5 (t2_vec I0 bd0 bd1)))
                     Then ＜ x1, ｛ x1 ｝_ ssk I0 ˆ rs3 I0, π1 Dec_iif I0 (π1 t1 K1 bd0) (π1 adv5 (t2_vec I0 bd0 bd1)) ＞
                     Else Error Else Error])
       (bd0) (bd1)) as claim1.
  rewrite claim1.
  clear claim1.
  Focus 9. clear. unfold C21. unfold t2_vec. simpl. time ProveCca2After. (* Without unfold t2_vec, the time will be 8.767. Or will be 45.419*)

(* Claim1':  t2[x1 ,h(b1)] is (npk0, rRA11, h(b0), h(b1)) −- CCA2 compliant. npk0 := nonce 0, rRA11 := nonce 101 *)
  unfold C21. unfold t2_vec. simpl.
  pose (cca2 [nonce 0] [nonce 101]
             (fun x1 => (prop2_claim1 x1) ++ [ If (π1 adv5 (prop2_claim1 x1)) ≟ (π1 t1 K1 bd0)
  Then ＜ ❴ bd1 ❵_ pkS ＾ r30, ｛ ❴ bd1 ❵_ pkS ＾ r30 ｝_ ssk0 ˆ rs30, Pseudonym [pk0; vk0; np1] ＞
  Else If Ver [π1 adv5 [bd0; bd1; B1 I0 hbd0; B1 I1 hbd1; Phi2 I0 K1 hbd0 hbd1]; π2 t1 K1 bd0; vkRA]
       Then If hbd0
               ≟ (π2 If (π1 adv5 [bd0; bd1; B1 I0 hbd0; B1 I1 hbd1; Phi2 I0 K1 hbd0 hbd1]) ≟ (π1 t1 K1 bd0) Then Error
                     Else Dec [π1 adv5 [bd0; bd1; B1 I0 hbd0; B1 I1 hbd1; Phi2 I0 K1 hbd0 hbd1]; sk0])
            Then ＜ ❴ bd1 ❵_ pkS ＾ r30, ｛ ❴ bd1 ❵_ pkS ＾ r30 ｝_ ssk0 ˆ rs30,
                 π1 If (π1 adv5 [bd0; bd1; B1 I0 hbd0; B1 I1 hbd1; Phi2 I0 K1 hbd0 hbd1]) ≟ (π1 t1 K1 bd0) Then Error
                    Else Dec [π1 adv5 [bd0; bd1; B1 I0 hbd0; B1 I1 hbd1; Phi2 I0 K1 hbd0 hbd1]; sk0] ＞ Else Error Else Error])
       (＜ Pseudonym [pk0; vk0; np11], hbd0 ＞) (＜ Pseudonym [pk0; vk0; np11], hbd1 ＞)) as claim1.
  rewrite claim1.
  clear claim1.
  
  (* Claim1'': t2[x1 ,h(b1)] is (npkRA, r0, h(b0), h(b1)) −- CCA2 compliant. npkRA := nonce 8, r0 := nonce 2 *)
  unfold B2 at 1. unfold C21. unfold t2_vec.
  (* ... *)

  
  rewrite prop3_lemma3_weak with (k := K1).
  (* Claim2:   t2[h(b1) ,x2] is (npkRA, r1, h(b0), h(b1)) −- CCA2 compliant.  npkRA := nonce 8, r0 := nonce 6*)

  (* Claim2':  t2[h(b1), x2] is (npk1, rRA12, h(b0), h(b1)) −- CCA2 compliant. npk1 := nonce 4, rRA12 := nonce 191*)

  (* Claim2'': t2[h(b1) ,x2] is (npkS, r11, h(b0), h(b1)) −- CCA2 compliant. npkS := nonce 12, r11 := nonce 600 *)

If_eval*)

Abort.

*)
