(************************************************************************)
(* Copyright (c) 2021, Gergei Bana, Qianli Zhang                        *)
(*                                                                      *)
(* This work is licensed under the MIT license. The license is          *)
(* described in the file "LICENSE" available at the root of the source  *)
(* or at https://opensource.org/licenses/MIT                            *)
(************************************************************************)
 
Require Export Coq.Lists.List.
Import ListNotations.
Require Import Coq.micromega.Lia.
Require Export prop1.


Axiom Tau1Tri :
      forall {x1 x2 x3: ppt} ,
        (Tau1 [Triple [x1; x2; x3]]) = x1.

Axiom Tau2Tri :
      forall {x1 x2 x3: ppt} ,
        (Tau2 [Triple [x1; x2; x3]]) = x2.

Axiom Tau3Tri :
      forall {x1 x2 x3: ppt} ,
        (Tau3 [Triple [x1; x2; x3]]) = x3.



(**********************************
 ** Fundamental Function Symbols **
 **********************************)

Parameter Pseudonyms : Symbols Deterministic (narg 3).
Notation Pseudonym := (FuncInt Deterministic (narg 3) Pseudonyms).


(**************************
 **   Nonce management   **
 **************************)

(*np and rRA should be indexed by K. *)
Notation np1  := (nonce 16).
Notation np2  := (nonce 17).
Notation np3  := (nonce 18).

Notation rRA1   := (Rand [nonce 10]).
Notation rRA2   := (Rand [nonce 19]).
Notation rRA3   := (Rand [nonce 20]).



(*the return value of adv2 should be a triple vector, then we can use τ?*)
Notation adv2 c1 c2:= (adv (2) [c1; c2]).
Notation Dec_RA c := (Dec [c; skRA]).
Notation Dec_RAif x d := (If (EQ [d ;x]) Then Error Else (Dec [d; skRA])).


(*why we have to add τ after adv2? when we use if-then-else, we need to compare "τ1 (adv2 (B1_0 x1) (B1_1 x2))" with "B1_0 x1"*)
Definition C1 k x1 x2 := tau k [adv2 (B1 I0 x1) (B1 I1 x2)] ≟ B1 I0 x1 .
Definition C2 k x1 x2 := tau k [adv2 (B1 I0 x1) (B1 I1 x2)] ≟ B1 I1 x2 .
Definition C i k x1 x2 := match i with
                          | I0 => C1 k x1 x2
                          | I1 => C2 k x1 x2
                          end.





Definition d k x1 x2 := Dec_RA (tau k [adv2 (B1 I0 x1) (B1 I1 x2)]).
Definition dif i k x1 x2 :=  If (C i k x1 x2) Then Error Else (d k x1 x2) .



Definition np k  := match k with K1 => np1 | K2 => np2 | K3 => np3 end.



(*we use the public key, public verification key and a randomness to generate the pseudonyms*)
Definition P k x1 x2 := (Pseudonym [π1 (τ1 (d k x1 x2)); π2 (τ1 (d k x1 x2)); (np k)]).
Definition Pif i k x1 x2 := (Pseudonym [π1 (τ1 (dif i k x1 x2)); π2 (τ1 (dif i k x1 x2)); (np k)]).



Definition rRA k  := match k with K1 => rRA1 | K2 => rRA2 | K3 => rRA3 end.




(*Here randomness should be different, while this doesn't affect the final result*)
(*And here the randomness rRA and rsRA should be different in different encryption and signature*)
Definition R1 k x1 x2 := (If Ver [τ2 (d k x1 x2); τ3 (d k x1 x2);  π2 (τ1 (d k x1 x2))]
                             Then ｛❴＜P k x1 x2, τ2 (d k x1 x2)＞❵_(π1 (τ1 (d k x1 x2))) ＾ (rRA k)｝_sskRA ˆ rsRA
                             Else Error).

Definition R1if i k x1 x2 := (If Ver [τ2 (dif i k x1 x2); τ3 (dif i k x1 x2);  π2 (τ1 (dif i k x1 x2))]
                             Then ｛❴＜Pif i k x1 x2, τ2 (dif i k x1 x2)＞❵_(π1 (τ1 (dif i k x1 x2))) ＾ (rRA k)｝_sskRA ˆ rsRA
                             Else Error).



(* This is essentially Lemma 2 from Ali's thesis: *)

Lemma prop2_lemma2:
  forall  k x1 x2,
    R1 k x1 x2 = If C1 k x1 x2
                    Then｛❴＜Pseudonym [pk0; vk0  ; np k], x1 ＞❵_pk0 ＾ (rRA k)｝_sskRA ˆ rsRA
                    Else (If C2 k x1 x2
                             Then｛❴＜Pseudonym [pk1; vk1  ; np k], x2 ＞❵_pk1 ＾ (rRA k) ｝_sskRA ˆ rsRA
                             Else (If C1 k x1 x2
                                      Then Error
                                      Else (If C2 k x1 x2 Then Error Else (R1 k x1 x2) )
                                  )
                         ).

Proof.
  intros.
  rewrite @Example7_1 with (b:=  C2 k x1 x2) (x1 := default) (y1 := default)
                           (t1 := (fun x :ppt =>  ｛ ❴ ＜ Pseudonym [pk1; vk1; np k], x2 ＞ ❵_ pk1 ＾ rRA k ｝_ sskRA ˆ rsRA))
                           (t2 := (fun x :ppt =>   If (C1 k x1 x2) Then Error Else x ) ).
  rewrite @Example7_1 with (b:=  C1 k x1 x2) (x1 := default) (y1 := default)
                           (t1 := (fun x :ppt =>  ｛ ❴ ＜ Pseudonym [pk0; vk0; np k], x1 ＞ ❵_ pk0 ＾ rRA k ｝_ sskRA ˆ rsRA))
                           (t2 := (fun x :ppt =>   If (C2 k x1 x2)
                                                      Then ｛ ❴ ＜ Pseudonym [pk1; vk1; np k], x2 ＞ ❵_ pk1 ＾ rRA k ｝_ sskRA ˆ rsRA
                                                      Else x )).
  rewrite <- (@If_same (C1 k x1 x2 ) ( R1 k x1 x2)) at 1.
  rewrite <- (@If_same (C2 k x1 x2 ) ( R1 k x1 x2)) at 2.
  unfold R1 at 1.  unfold C1 at 1. unfold P. unfold d.
  rewrite (Eq_branch (tau k [adv2 (B1 I0 x1) (B1 I1 x2)])
                     (B1 I0 x1)
                     (fun x : ppt =>  If Ver [τ2 (Dec_RA ( x )); τ3 (Dec_RA ( x ) ); π2 τ1 (Dec_RA ( x ))]
                                         Then ｛ ❴ ＜ Pseudonym [π1 τ1 (Dec_RA ( x )); π2 τ1 (Dec_RA ( x )); np k ],
                                                   τ2 (Dec_RA ( x  )) ＞
                                                 ❵_ π1 τ1 (Dec_RA ( x  )) ＾ ( rRA k )
                                              ｝_ sskRA ˆ rsRA
                                         Else Error ) ).
  unfold B1 at 4 5 6 7 8 9 10. unfold r.
  rewrite decenc.
  rewrite Tau1Tri. rewrite Tau2Tri. rewrite Tau3Tri.
  unfold ssk. unfold rs. unfold aid. unfold pk. unfold vk.
  rewrite proj1pair. rewrite proj2pair.
  rewrite correctness.
  repeat rewrite ceqeq.
  repeat rewrite If_true.
  fold  (C1 k x1 x2).
  unfold R1 at 1. unfold C2 at 1.  unfold P. unfold d.
  rewrite (Eq_branch (tau k [adv2 (B1 I0 x1) (B1 I1 x2)])
                     (B1 I1 x2)
                     (fun x : ppt =>  If Ver [τ2 (Dec_RA ( x )); τ3 (Dec_RA ( x ) ); π2 τ1 (Dec_RA ( x ))]
                                         Then ｛ ❴ ＜ Pseudonym [π1 τ1 (Dec_RA ( x )); π2 τ1 (Dec_RA ( x )); np k ],
                                                   τ2 (Dec_RA ( x  )) ＞
                                                 ❵_ π1 τ1 (Dec_RA ( x  )) ＾ ( rRA k )
                                              ｝_ sskRA ˆ rsRA
                                         Else Error ) ).
  unfold B1 at 4 5 6 7 8 9 10. unfold r.
  rewrite decenc.
  rewrite Tau1Tri. rewrite Tau2Tri. rewrite Tau3Tri.
  unfold ssk. unfold rs. unfold aid. unfold pk. unfold vk.
  rewrite proj1pair. rewrite proj2pair.
  rewrite correctness.
  repeat rewrite ceqeq.
  repeat rewrite If_true.
  fold  (C2 k x1 x2).
  reflexivity.
  all: destruct k ; unfold tau; ProveContext; unfold tau; ProvePPT.
Qed.




Notation np11  := (nonce 161).
Notation np12  := (nonce 162).


Notation rRA11   := (Rand [nonce 101]).
Notation rRA12   := (Rand [nonce 191]).



(* The following Proposition contains all branches for R1 K1.:  *)

Proposition prop2small_allbranches :
  cca_2 ->
  [ bd I0 ; bd I1 ; B1 I0 hbd0 ; B1 I1 hbd1 ;
    C1 K1 hbd0 hbd1 ; C2 K1 hbd0 hbd1 ;
  ｛❴＜Pseudonym [pk0; vk0  ; np11], hbd0 ＞❵_pk0 ＾ rRA11｝_sskRA ˆ rsRA ;
  ｛❴＜Pseudonym [pk1; vk1  ; np12], hbd1 ＞❵_pk1 ＾ rRA12 (*should be rRA12 instead of rRA1*) ｝_sskRA ˆ rsRA ;
     If C1 K1 hbd0 hbd1 Then Error Else (If (C2 K1 hbd0 hbd1) Then Error Else (R1 K1 hbd0 hbd1))]
~
  [ bd I0 ; bd I1 ; B1 I0 hbd1 ; B1 I1 hbd0 ;
    C1 K1 hbd1 hbd0 ; C2 K1 hbd1 hbd0 ;
  ｛❴＜Pseudonym [pk0; vk0  ; np11], hbd1 ＞❵_pk0 ＾ rRA11 ｝_sskRA ˆ rsRA ;
  ｛❴＜Pseudonym [pk1; vk1  ; np12], hbd0 ＞❵_pk1 ＾ rRA12 ｝_sskRA ˆ rsRA ;
    If C1 K1 hbd1 hbd0 Then Error Else (If (C2 K1 hbd1 hbd0) Then Error Else (R1 K1 hbd1 hbd0))].
(* Prove this by using CCA2 for all encryptions. Use CCA2toCCA2L and CCA2L to avoid if and else terms with length. *)

Proof.
  intros cca2. unfold cca_2 in cca2.
  apply CCA2toCCA2L in cca2.


  unfold R1.
  unfold P.
  rewrite <- (@If_false Error (d K1 hbd0 hbd1)).
  rewrite <- @If_eval with (b :=  C1 K1 hbd0 hbd1)
                          (tc1 := (fun x => Error))
                          (tc2 := (fun x =>  If C2 K1 hbd0 hbd1 Then Error Else
                                           (If Ver [τ2 (If x Then Error Else d K1 hbd0 hbd1);
                                                    τ3 (If x Then Error Else d K1 hbd0 hbd1);
                                                    π2 τ1 (If x Then Error Else d K1 hbd0 hbd1)]
                                               Then ｛ ❴ ＜ Pseudonym [π1 τ1 (If x  Then Error Else d K1 hbd0 hbd1);
                                                                       π2 τ1 (If x Then Error Else d K1 hbd0 hbd1); np K1],
                                                         τ2 (If x Then Error Else d K1 hbd0 hbd1) ＞
                                                       ❵_ π1 τ1 (If x Then Error Else d K1 hbd0 hbd1)＾ rRA K1 ｝_ sskRA ˆ rsRA Else Error ))).

(* Claim1: t2[x1 ,h(b1)] is (npkRA, r0, h(b0), h(b1)) −- CCA2 compliant. npkRA := nonce 8, r0 := nonce 2 *)
  pose (cca2 [nonce 8] [nonce 2]
             (fun x1 => [bd0; bd1; x1; B1 I1 hbd1;
                     τ1 (adv2 x1 (B1 I1 hbd1)) ≟ x1;
                     τ1 (adv2 x1 (B1 I1 hbd1)) ≟ B1 I1 hbd1;
                     ｛ ❴ ＜ Pseudonym [pk0; vk0; np11], hbd0 ＞ ❵_ pk0 ＾ rRA11 ｝_ sskRA ˆ rsRA;
                     ｛ ❴ ＜ Pseudonym [pk1; vk1; np12], hbd1 ＞ ❵_ pk1 ＾ rRA12  ｝_ sskRA ˆ rsRA;
                     If τ1 (adv2 x1 (B1 I1 hbd1)) ≟ x1 Then Error Else If τ1 (adv2 x1 (B1 I1 hbd1)) ≟ B1 I1 hbd1 Then Error
                        Else (If Ver [τ2 (Dec_RAif x1 (τ1 (adv2 x1 (B1 I1 hbd1))));
                                      τ3 (Dec_RAif x1 (τ1 (adv2 x1 (B1 I1 hbd1))));
                                      π2 τ1 (Dec_RAif x1 (τ1 (adv2 x1 (B1 I1 hbd1))))]
                                 Then ｛ ❴ ＜ Pseudonym [π1 τ1 (Dec_RAif x1 (τ1 (adv2 x1 (B1 I1 hbd1))));
                                                         π2 τ1 (Dec_RAif x1 (τ1 (adv2 x1 (B1 I1 hbd1)))); np1], τ2 (Dec_RAif x1 (τ1 (adv2 x1 (B1 I1 hbd1)))) ＞
                                         ❵_ π1 τ1 (Dec_RAif x1 (τ1 (adv2 x1 (B1 I1 hbd1)))) ＾ rRA1 ｝_ sskRA ˆ rsRA Else Error)])
       (＜ aid I0, hbd0, ｛ hbd0 ｝_ ssk0 ˆ rs0 ＞) (＜ aid I0, hbd1, ｛ hbd1 ｝_ ssk0 ˆ rs0 ＞)) as claim1.
  rewrite claim1.
  clear claim1.


  rewrite @If_eval with (b := τ1 (adv2 (❴ ＜ aid I0, hbd1, ｛ hbd1 ｝_ ssk0 ˆ rs0 ＞ ❵_ pkRA ＾ r0) (B1 I1 hbd1))
                                 ≟ ❴ ＜ aid I0, hbd1, ｛ hbd1 ｝_ ssk0 ˆ rs0 ＞ ❵_ pkRA ＾ r0)
                        (tc1 := (fun x => Error))
                        (tc2 := (fun x => If τ1 (adv2 (❴ ＜ aid I0, hbd1, ｛ hbd1 ｝_ ssk0 ˆ rs0 ＞ ❵_ pkRA ＾ r0) (B1 I1 hbd1)) ≟ B1 I1 hbd1
                                          Then Error
                                          Else If Ver [τ2 (If x Then Error
                                                              Else Dec_RA (τ1 (adv2 (❴ ＜ aid I0, hbd1, ｛ hbd1 ｝_ ssk0 ˆ rs0 ＞ ❵_ pkRA ＾ r0) (B1 I1 hbd1))));
                                                       τ3 (If x Then Error
                                                              Else Dec_RA (τ1 (adv2 (❴ ＜ aid I0, hbd1, ｛ hbd1 ｝_ ssk0 ˆ rs0 ＞ ❵_ pkRA ＾ r0) (B1 I1 hbd1))));
                                                       π2 τ1 (If x Then Error
                                                                 Else Dec_RA (τ1 (adv2 (❴ ＜ aid I0, hbd1, ｛ hbd1 ｝_ ssk0 ˆ rs0 ＞ ❵_ pkRA ＾ r0) (B1 I1 hbd1))))]
                                          Then ｛ ❴ ＜ Pseudonym [π1 τ1 (If x Then Error Else Dec_RA (τ1 (adv2 (❴ ＜ aid I0, hbd1, ｛ hbd1 ｝_ ssk0 ˆ rs0 ＞ ❵_ pkRA ＾ r0) (B1 I1 hbd1))));
                                                                  π2 τ1 (If x Then Error Else Dec_RA (τ1 (adv2 (❴ ＜ aid I0, hbd1, ｛ hbd1 ｝_ ssk0 ˆ rs0 ＞ ❵_ pkRA ＾ r0) (B1 I1 hbd1)))); np1],
                                                    τ2 (If x Then Error Else Dec_RA (τ1 (adv2 (❴ ＜ aid I0, hbd1, ｛ hbd1 ｝_ ssk0 ˆ rs0 ＞ ❵_ pkRA ＾ r0) (B1 I1 hbd1)))) ＞
                                                  ❵_ π1 τ1 (If x Then Error Else Dec_RA (τ1 (adv2 (❴ ＜ aid I0, hbd1, ｛ hbd1 ｝_ ssk0 ˆ rs0 ＞ ❵_ pkRA ＾ r0) (B1 I1 hbd1))))
                                                   ＾ rRA1 ｝_ sskRA ˆ rsRA Else Error)).

  rewrite  (@If_false Error ( Dec_RA (τ1 (adv2 (❴ ＜ aid I0, hbd1, ｛ hbd1 ｝_ ssk0 ˆ rs0 ＞ ❵_ pkRA ＾ r0) (B1 I1 hbd1))))).



(* Claim1': t2[x1 ,h(b1)] is (npk0, rRA11, h(b0), h(b1)) −- CCA2 compliant. npk0 := nonce 0, rRA11 := nonce 101 *)
  pose (cca2 [nonce 0] [nonce 101]
             (fun x => [bd I0; bd I1; B1 I0 hbd1; B1 I1 hbd1;
                     C1 K1 hbd1 hbd1; C2 K1 hbd1 hbd1;
                     ｛ x ｝_ sskRA ˆ rsRA;
                     ｛ ❴ ＜ Pseudonym [pk1; vk1; np12], hbd1 ＞ ❵_ pk1 ＾ rRA12 ｝_ sskRA ˆ rsRA;
                     If C1 K1 hbd1 hbd1 Then Error Else If C2 K1 hbd1 hbd1 Then Error Else R1 K1 hbd1 hbd1])
       (＜ Pseudonym [pk0; vk0; np11], hbd0 ＞) (＜ Pseudonym [pk0; vk0; np11], hbd1 ＞)) as claim1'.
  rewrite claim1'.
  clear claim1'.


  rewrite <- (@If_false Error (d K1 hbd1 hbd0)).

  rewrite <- @If_eval with (b :=  C2 K1 hbd1 hbd0)
                          (tc1 := (fun x => Error))
                          (tc2 := (fun x => If Ver [τ2 (If x Then Error Else d K1 hbd1 hbd0);
                                                 τ3 (If x Then Error Else d K1 hbd1 hbd0);
                                                 π2 τ1 (If x Then Error Else d K1 hbd1 hbd0)]
                                            Then ｛ ❴ ＜ Pseudonym [π1 τ1 (If x  Then Error Else d K1 hbd1 hbd0);
                                                                    π2 τ1 (If x Then Error Else d K1 hbd1 hbd0); np K1],
                                                      τ2 (If x Then Error Else d K1 hbd1 hbd0) ＞
                                                    ❵_ π1 τ1 (If x Then Error Else d K1 hbd1 hbd0) ＾ rRA K1 ｝_ sskRA ˆ rsRA Else Error )).


(* Claim2: t2[h(b1) ,x2] is (npkRA, r1, h(b0), h(b1)) −- CCA2 compliant.  npkRA := nonce 8, r0 := nonce 6*)
   pose (cca2 [nonce 8] [nonce 6]
              (fun x2 => [bd0; bd1; B1 I0 hbd1; x2;
                       tau K1 [adv2 (B1 I0 hbd1) x2] ≟ B1 I0 hbd1;
                       tau K1 [adv2 (B1 I0 hbd1) x2] ≟ x2;
                       ｛ ❴ ＜ Pseudonym [pk0; vk0; np11], hbd1 ＞ ❵_ pk0 ＾ rRA11 ｝_ sskRA ˆ rsRA;
                       ｛ ❴ ＜ Pseudonym [pk1; vk1; np12], hbd0 ＞ ❵_ pk1 ＾ rRA12  ｝_ sskRA ˆ rsRA;
                       If (tau K1 [adv2 (B1 I0 hbd1) x2] ≟ B1 I0 hbd1) Then Error Else If (tau K1 [adv2 (B1 I0 hbd1) x2] ≟ x2) Then Error
                          Else (If Ver [τ2 (Dec_RAif x2 (τ1 (adv2 (B1 I0 hbd1) x2)));
                                        τ3 (Dec_RAif x2 (τ1 (adv2 (B1 I0 hbd1) x2)));
                                        π2 τ1 (Dec_RAif x2 (τ1 (adv2 (B1 I0 hbd1) x2)))]
                                   Then ｛ ❴ ＜ Pseudonym [π1 τ1 (Dec_RAif x2 (τ1 (adv2 (B1 I0 hbd1) x2)));
                                                           π2 τ1 (Dec_RAif x2 (τ1 (adv2 (B1 I0 hbd1) x2))); np1],
                                             τ2 (Dec_RAif x2 (τ1 (adv2 (B1 I0 hbd1) x2))) ＞
                                           ❵_ π1 τ1 (Dec_RAif x2 (τ1 (adv2 (B1 I0 hbd1) x2))) ＾ rRA1 ｝_ sskRA ˆ rsRA Else Error)])
       (＜ aid I1, hbd0, ｛ hbd0 ｝_ ssk1 ˆ rs1 ＞) (＜ aid I1, hbd1, ｛ hbd1 ｝_ ssk1 ˆ rs1 ＞)) as claim2.
  rewrite claim2.
  clear claim2.


  unfold tau.

  rewrite  @If_eval with (b :=  Tau1 [adv2 (B1 I0 hbd1) (❴ ＜ aid I1, hbd1, ｛ hbd1 ｝_ ssk1 ˆ rs1 ＞ ❵_ pkRA ＾ r1)] ≟ ❴ ＜ aid I1, hbd1, ｛ hbd1 ｝_ ssk1 ˆ rs1 ＞ ❵_ pkRA ＾ r1)
                         (tc1 := (fun x => Error))
                         (tc2 := (fun x =>  If Ver [τ2 (If x Then Error Else Dec_RA (τ1 (adv2 (B1 I0 hbd1) (❴ ＜ aid I1, hbd1, ｛ hbd1 ｝_ ssk1 ˆ rs1 ＞ ❵_ pkRA ＾ r1))));
                                                 τ3 (If x Then Error Else Dec_RA (τ1 (adv2 (B1 I0 hbd1) (❴ ＜ aid I1, hbd1, ｛ hbd1 ｝_ ssk1 ˆ rs1 ＞ ❵_ pkRA ＾ r1))));
                                                 π2 τ1 (If x Then Error Else Dec_RA (τ1 (adv2 (B1 I0 hbd1) (❴ ＜ aid I1, hbd1, ｛ hbd1 ｝_ ssk1 ˆ rs1 ＞ ❵_ pkRA ＾ r1))))]
                                            Then ｛ ❴ ＜ Pseudonym
                                                         [π1 τ1 (If x Then Error Else Dec_RA (τ1 (adv2 (B1 I0 hbd1) (❴ ＜ aid I1, hbd1, ｛ hbd1 ｝_ ssk1 ˆ rs1 ＞ ❵_ pkRA ＾ r1))));
                                                          π2 τ1 (If x Then Error Else Dec_RA (τ1 (adv2 (B1 I0 hbd1) (❴ ＜ aid I1, hbd1, ｛ hbd1 ｝_ ssk1 ˆ rs1 ＞ ❵_ pkRA ＾ r1)))); np1],
                                                      τ2 (If x Then Error Else Dec_RA (τ1 (adv2 (B1 I0 hbd1) (❴ ＜ aid I1, hbd1, ｛ hbd1 ｝_ ssk1 ˆ rs1 ＞ ❵_ pkRA ＾ r1)))) ＞
                                                    ❵_ π1 τ1 (If x Then Error Else Dec_RA (τ1 (adv2 (B1 I0 hbd1) (❴ ＜ aid I1, hbd1, ｛ hbd1 ｝_ ssk1 ˆ rs1 ＞ ❵_ pkRA ＾ r1))))
                                                     ＾ rRA1 ｝_ sskRA ˆ rsRA Else Error)).

  rewrite  (@If_false Error ( Dec_RA (τ1 (adv2 (B1 I0 hbd1) (❴ ＜ aid I1, hbd1, ｛ hbd1 ｝_ ssk1 ˆ rs1 ＞ ❵_ pkRA ＾ r1))))).


(* Claim2': t2[h(b1), x2] is (npk1, rRA12, h(b0), h(b1)) −- CCA2 compliant. npk1 := nonce 4, rRA12 := nonce 191*)
  pose (cca2 [nonce 4] [nonce 191]
             (fun x2 =>  [bd I0; bd I1; B1 I0 hbd1; B1 I1 hbd1;
                       C1 K1 hbd1 hbd1; C2 K1 hbd1 hbd1;
                       ｛ ❴ ＜ Pseudonym [pk0; vk0; np11], hbd1 ＞ ❵_ pk0 ＾ rRA11 ｝_ sskRA ˆ rsRA;
                       ｛ x2  ｝_ sskRA ˆ rsRA;
                       If C1 K1 hbd1 hbd1 Then Error Else If C2 K1 hbd1 hbd1 Then Error Else R1 K1 hbd1 hbd1])
       (＜ Pseudonym [pk1; vk1; np12], hbd0 ＞) (＜ Pseudonym [pk1; vk1; np12], hbd1 ＞)) as claim2'.
  rewrite claim2'.
  clear claim2'.

  reflexivity.


(*  all : (simpl; auto).
  all: time (try ProveListFresh; try lia). (* 59.311 *)  all : (try ProveNonceList).
  all : time try ProveCca2Before. (*19.216*)
  unfold R1;  unfold P; unfold d; unfold C1; unfold C2;  unfold  B1; unfold aid; unfold pk; unfold vk; unfold tau; unfold np.
  time  ProveCca2After. (*18*)
  Focus 4.
   unfold R1;  unfold P; unfold d; unfold C1; unfold C2;  unfold  B1; unfold aid; unfold pk; unfold vk; unfold tau; unfold np.
   time ProveCca2After. (*28*)
   Focus 7.
      unfold R1;  unfold P; unfold d; unfold C1; unfold C2;  unfold  B1; unfold aid; unfold pk; unfold vk; unfold tau; unfold np.
      time ProveCca2After. (*16*)
       Focus 10.
      unfold R1;  unfold P; unfold d; unfold C1; unfold C2;  unfold  B1; unfold aid; unfold pk; unfold vk; unfold tau; unfold np.
   time ProveCca2After. (*27*)
all: unfold tau; try ProveContext ; unfold tau; ProvePPT.*)

  (* Finish remaining goals *)
Admitted.





(* Now we can generate all branches: *)

Proposition prop2small_branch1 :
  cca_2
  -> [ bd I0 ; bd I1 ; B1 I0 hbd0 ; B1 I1 hbd1 ;
     C1 K1 hbd0 hbd1 ;｛❴＜Pseudonym [pk0; vk0  ; np1], hbd0 ＞❵_pk0 ＾ rRA1｝_sskRA ˆ rsRA ]
       ~
     [ bd I0 ; bd I1 ; B1 I0 hbd1 ; B1 I1 hbd0 ;
     C1 K1 hbd1 hbd0 ;｛❴＜Pseudonym [pk0; vk0  ; np1], hbd1 ＞❵_pk0 ＾ rRA1｝_sskRA ˆ rsRA ].

(*  Prove this by first projecting (function application)  from prop2small_allbranches  to the above formula
    Now replace the nonces of np11 and rRA11 to the right nonces using FreshInd and function application *)

Proof.
  intros cca2.
  apply prop2small_allbranches in cca2.
  apply (@cind_funcapp (fun lc => firstn 5 lc ++ [Nth 6 lc])) in cca2; unfold Nth in cca2; simpl in cca2.

  assert ((fun x => x ~ x) ([pk0; vk0; hbd0; sskRA; rsRA; bd0; bd1; B1 I0 hbd0; B1 I1 hbd1; C1 K1 hbd0 hbd1])). simpl; reflexivity.
      simpl in H.
      apply (FreshInd (nonce 161) (nonce 16)) in H. (*np11  and np1 *)
      apply (FreshInd (nonce 101) (nonce 10)) in H. (*rRA11 and rRA1*)
      apply (@cind_funcapp (fun lc => (skipn 7 lc) ++ [｛ ❴ ＜ Pseudonym [(Nth 2 lc); (Nth 3 lc); (Nth 1 lc)], (Nth 4 lc) ＞ ❵_ (Nth 2 lc) ＾ (Rand [Nth 0 lc]) ｝_ (Nth 5 lc) ˆ (Nth 6 lc)])) in H.
      unfold Nth in H; simpl in H; auto.
      rewrite H in cca2.
      clear H.

  assert ((fun x => x ~ x) ([pk0; vk0; hbd1; sskRA; rsRA; bd0; bd1; B1 I0 hbd1; B1 I1 hbd0; C1 K1 hbd1 hbd0])). simpl; reflexivity.
      simpl in H.
      apply (FreshInd (nonce 161) (nonce 16)) in H. (*np11  and np1 *)
      apply (FreshInd (nonce 101) (nonce 10)) in H. (*rRA11 and rRA1*)
      apply (@cind_funcapp (fun lc => (skipn 7 lc) ++ [｛ ❴ ＜ Pseudonym [(Nth 2 lc); (Nth 3 lc); (Nth 1 lc)], (Nth 4 lc) ＞ ❵_ (Nth 2 lc) ＾ (Rand [Nth 0 lc]) ｝_ (Nth 5 lc) ˆ (Nth 6 lc)])) in H.
      unfold Nth in H; simpl in H; auto.
      rewrite H in cca2.
      clear H.

  auto.
  all : ProveContext.
  unfold C1 in *. unfold B1 in *.
  all : time ProveFresh. (*2.319 secs*)
  all : try lia.
  all : unfold tau; ProveContext. Qed.




Proposition prop2small_branch2 :
  cca_2
  -> [ bd I0 ; bd I1 ; B1 I0 hbd0 ; B1 I1 hbd1 ;
     C1 K1 hbd0 hbd1 ; C2 K1 hbd0 hbd1 ;｛❴＜Pseudonym [pk1; vk1  ; np1], hbd1 ＞❵_pk1 ＾ rRA1 ｝_sskRA ˆ rsRA]
       ~
     [ bd I0 ; bd I1 ; B1 I0 hbd1 ; B1 I1 hbd0 ;
     C1 K1 hbd1 hbd0 ; C2 K1 hbd1 hbd0 ;｛❴＜Pseudonym [pk1; vk1  ; np1], hbd0 ＞❵_pk1 ＾ rRA1 ｝_sskRA ˆ rsRA].
  (* The same method as branch1*)
Proof.
  intros cca2.
  apply prop2small_allbranches in cca2.
  apply (@cind_funcapp (fun lc => firstn 6 lc ++ [Nth 7 lc])) in cca2; unfold Nth in cca2; simpl in cca2.

  assert ((fun x => x ~ x) ([pk1; vk1; hbd1; sskRA; rsRA; bd0; bd1; B1 I0 hbd0; B1 I1 hbd1; C1 K1 hbd0 hbd1; C2 K1 hbd0 hbd1])) as H1. simpl; reflexivity.
  assert ((fun x => x ~ x) ([pk1; vk1; hbd0; sskRA; rsRA; bd0; bd1; B1 I0 hbd1; B1 I1 hbd0; C1 K1 hbd1 hbd0; C2 K1 hbd1 hbd0])) as H2. simpl; reflexivity.
      simpl in H1, H2. Print rRA12.
      apply (FreshInd (nonce 162) (nonce 16)) in H1.
      apply (FreshInd (nonce 162) (nonce 16)) in H2. (*np12  and np1 *)
      apply (FreshInd (nonce 191) (nonce 10)) in H1.
      apply (FreshInd (nonce 191) (nonce 10)) in H2. (*rRA12 and rRA1*)
      apply (@cind_funcapp (fun lc => (skipn 7 lc) ++ [｛ ❴ ＜ Pseudonym [(Nth 2 lc); (Nth 3 lc); (Nth 1 lc)], (Nth 4 lc) ＞ ❵_ (Nth 2 lc) ＾ (Rand [Nth 0 lc]) ｝_ (Nth 5 lc) ˆ (Nth 6 lc)])) in H1.
      unfold Nth in H1; simpl in H1; auto.
      apply (@cind_funcapp (fun lc => (skipn 7 lc) ++ [｛ ❴ ＜ Pseudonym [(Nth 2 lc); (Nth 3 lc); (Nth 1 lc)], (Nth 4 lc) ＞ ❵_ (Nth 2 lc) ＾ (Rand [Nth 0 lc]) ｝_ (Nth 5 lc) ˆ (Nth 6 lc)])) in H2.
      unfold Nth in H2; simpl in H2; auto.
      rewrite H1 in cca2.
      rewrite H2 in cca2.
      clear H1 H2.

  auto.
  all : ProveContext.
  unfold C1 in *. unfold B1 in *.
  all : time ProveFresh. (* 5.328 secs*)
  all : try lia.
  all : unfold tau; ProveContext. Qed.





Proposition prop2small_branch3 :
  cca_2
  -> [ bd I0 ; bd I1 ; B1 I0 hbd0 ; B1 I1 hbd1 ;
     C1 K1 hbd0 hbd1 ; C2 K1 hbd0 hbd1 ; If C1 K1 hbd0 hbd1
                                            Then Error
                                            Else (If (C2 K1 hbd0 hbd1)
                                                     Then Error
                                                     Else (R1 K1 hbd0 hbd1))]
       ~
     [ bd I0 ; bd I1 ; B1 I0 hbd1 ; B1 I1 hbd0 ;
     C1 K1 hbd1 hbd0 ; C2 K1 hbd1 hbd0 ; If C1 K1 hbd1 hbd0
                                            Then Error
                                            Else (If (C2 K1 hbd1 hbd0)
                                                     Then Error
                                                     Else (R1 K1 hbd1 hbd0))].

(* This can be obtained from prop2small_allbranches by function application *)
Proof.
  intros cca2.
  apply prop2small_allbranches in cca2.
  apply (@cind_funcapp (fun lc => firstn 6 lc ++ [Nth 8 lc])) in cca2; unfold Nth in cca2; simpl in cca2. auto.
  ProveContext. Qed.


Proposition prop2small :
  cca_2 ->
  [ bd I0 ; bd I1 ; B1 I0 hbd0 ; B1 I1 hbd1 ; R1 K1 hbd0 hbd1 ]
  ~
  [ bd I0 ; bd I1 ; B1 I0 hbd1 ; B1 I1 hbd0 ; R1 K1 hbd1 hbd0 ].

(* This you can get by putting together the 3 branches. First combine branch 2 and 3 and then 1 *)

Proof.
  intros cca2.
  assert (H := cca2).
  assert (H0 := cca2).
  assert (H1 := cca2).
  pose (@IF_branch [bd I0 ; bd I1 ; B1 I0 hbd0 ; B1 I1 hbd1 ;  C1 K1 hbd0 hbd1]
                   [ bd I0 ; bd I1 ; B1 I0 hbd1 ; B1 I1 hbd0 ; C1 K1 hbd1 hbd0]
                   (｛❴＜Pseudonym [pk1; vk1  ; np1], hbd1 ＞❵_pk1 ＾ rRA1 ｝_sskRA ˆ rsRA)
                   (｛❴＜Pseudonym [pk1; vk1  ; np1], hbd0 ＞❵_pk1 ＾ rRA1 ｝_sskRA ˆ rsRA)
                   (If C1 K1 hbd0 hbd1
                       Then Error
                       Else (If (C2 K1 hbd0 hbd1)
                                Then Error
                                Else (R1 K1 hbd0 hbd1)))
                   (If C1 K1 hbd1 hbd0
                       Then Error
                       Else (If (C2 K1 hbd1 hbd0)
                                Then Error
                                Else (R1 K1 hbd1 hbd0)))
                   (C2 K1 hbd0 hbd1)
                   (C2 K1 hbd1 hbd0)) as branch2. simpl in branch2.
  apply prop2small_branch2 in H.
  apply prop2small_branch3 in H0.
  apply branch2 in H. clear branch2.
  apply (@cind_funcapp (fun lc => firstn 5 lc ++ (skipn 6 lc))) in H; simpl in H.

  pose (@IF_branch [bd I0 ; bd I1 ; B1 I0 hbd0 ; B1 I1 hbd1 ]
                   [bd I0 ; bd I1 ; B1 I0 hbd1 ; B1 I1 hbd0 ]
                   (｛❴＜Pseudonym [pk0; vk0  ; np1], hbd0 ＞❵_pk0 ＾ rRA1｝_sskRA ˆ rsRA)
                   (｛❴＜Pseudonym [pk0; vk0  ; np1], hbd1 ＞❵_pk0 ＾ rRA1｝_sskRA ˆ rsRA)
                   (If C2 K1 hbd0 hbd1
                       Then ｛ ❴ ＜ Pseudonym [pk1; vk1; np1], hbd1 ＞ ❵_ pk1 ＾ rRA1 ｝_ sskRA ˆ rsRA
                       Else (If C1 K1 hbd0 hbd1
                                Then Error
                                Else (If C2 K1 hbd0 hbd1
                                         Then Error
                                         Else R1 K1 hbd0 hbd1)))
                   (If C2 K1 hbd1 hbd0
                       Then ｛ ❴ ＜ Pseudonym [pk1; vk1; np1], hbd0 ＞ ❵_ pk1 ＾ rRA1 ｝_ sskRA ˆ rsRA
                       Else (If C1 K1 hbd1 hbd0
                                Then Error
                                Else (If C2 K1 hbd1 hbd0
                                         Then Error
                                         Else R1 K1 hbd1 hbd0)))
                   (C1 K1 hbd0 hbd1)
                   (C1 K1 hbd1 hbd0)) as branch1. simpl in branch1.
  apply prop2small_branch1 in H1.
  apply branch1 in H1. clear branch1.
  apply (@cind_funcapp (fun lc => firstn 4 lc ++ (skipn 5 lc))) in H1; simpl in H1.

  rewrite (prop2_lemma2 K1 hbd0 hbd1).
  rewrite (prop2_lemma2 K1 hbd1 hbd0).
  all : auto.

  all : ProveContext. Qed.





Proposition prop2 :
  cca_2
  -> [ bd I0 ; bd I1 ; B1 I0 hbd0 ; B1 I1 hbd1 ; R1 K1 hbd0 hbd1 ; R1 K2 hbd0 hbd1 ; R1 K3 hbd0 hbd1 ]
       ~
     [ bd I0 ; bd I1 ; B1 I0 hbd1 ; B1 I1 hbd0 ; R1 K1 hbd1 hbd0 ; R1 K2 hbd1 hbd0 ; R1 K3 hbd1 hbd0 ].

Proof. Admitted.




