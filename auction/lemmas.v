(************************************************************************)
(* Copyright (c) 2021, Gergei Bana, Qianli Zhang                        *)
(*                                                                      *)
(* This work is licensed under the MIT license. The license is          *)
(* described in the file "LICENSE" available at the root of the source  *)
(* or at https://opensource.org/licenses/MIT                            *)
(************************************************************************)

(*   Unicode used in this file:
 *   1.'｛' :     U+FF5B
 *   2.'｝' :     U+FF5D
 *   3.'ˆ'  :     U+02C6
 *   4.'Ｈ' :     U+03C0
 *   5. 'τ' :     U+03C4

 *   5. '❴' :     U+2774
 *   5. '❵' :     U+
 *   5. '＾':     U+ FF3E

 *   5. '⎨' :     U+23A8
 *   5. '⎬' :     U+23AC
 *   5. 'ꞈ' :     U+ A788 no better CIRCUMFLEX in unicode

*)

Require Import Coq.Lists.List.
Require Import Coq.micromega.Lia.
Import ListNotations.
Require Export AuxiliaryTheoremEquality.


(********************************************
 ** Fundamental Function Symbols && Axioms **
 ********************************************)

Parameter Signs : Symbols Deterministic (narg 3).
Notation "'Sign'" := (FuncInt Deterministic (narg 3) Signs).
Notation "'｛' m '｝_' sk 'ˆ' r " := (Sign [m; sk; r]) (at level 101, left associativity).

Parameter Vers : Symbols Deterministic (narg 3).
Notation "'Ver'" := (FuncInt Deterministic (narg 3) Vers).

(* Axioms for signature *)
Axiom correctness :  forall {m n r}, Ver [m; ｛m｝_ (Skey [nonce n]) ˆ (Rand [nonce r]);  Pkey [nonce n]] = TRue.

Parameter Hashs : Symbols Deterministic (narg 1).
Notation "'Hash'" := (FuncInt Deterministic (narg 1) Hashs).
Notation "'Ｈ' m " := (Hash [m]) (at level 101, left associativity).

Parameter Triples : Symbols Deterministic (narg 3).
Notation "'Triple'" := (FuncInt Deterministic (narg 3) Triples).

Parameter Taus1 : Symbols Deterministic (narg 1).
Notation "'Tau1'" := (FuncInt Deterministic (narg 1) Taus1).
Notation τ1 x := (Tau1 [x]).
Parameter Taus2 : Symbols Deterministic (narg 1).
Notation "'Tau2'" := (FuncInt Deterministic (narg 1) Taus2).
Notation τ2 x := (Tau2 [x]).
Parameter Taus3 : Symbols Deterministic (narg 1).
Notation "'Tau3'" := (FuncInt Deterministic (narg 1) Taus3).
Notation τ3 x := (Tau3 [x]).
Notation "'＜' c1 ',' c2 '＞'" := (Pair [c1; c2]) (at level 100, right associativity). (* U+FF1C and U+FF1E)*)
Notation "'＜' c1 ',' c2 ',' c3 '＞'" := (Triple [c1; c2; c3]) (at level 100, right associativity). (*overload ＜,＞ notation*)

(* Axioms for tau_i *)
Axiom Tau1Tri : forall {x1 x2 x3: ppt}, (τ1 (＜x1, x2, x3 ＞)) = x1.
Axiom Tau2Tri : forall {x1 x2 x3: ppt}, (τ2 (＜x1, x2, x3 ＞)) = x2.
Axiom Tau3Tri : forall {x1 x2 x3: ppt}, (τ3 (＜x1, x2, x3 ＞)) = x3.

Parameter Pseudonyms : Symbols Deterministic (narg 4). (*Pseudonyms [pk, vk, hbd, np]*)
Notation Pseudonym := (FuncInt Deterministic (narg 4) Pseudonyms).

Parameter getVKs : Symbols Deterministic (narg 1).
Notation getVK := (FuncInt Deterministic (narg 1) getVKs).
Axiom P2Vk : forall {x1 x2 x3 x4: ppt}, (getVK [Pseudonym [x1; x2; x3; x4 ]]) = x2.

Parameter getHashedBids : Symbols Deterministic (narg 1).
Notation getHashedBid := (FuncInt Deterministic (narg 1) getHashedBids).
Axiom P2Hbd : forall {x1 x2 x3 x4: ppt}, (getHashedBid [Pseudonym [x1; x2; x3; x4 ]]) = x3.

(* symmetric enc *)

(*Key generation*)
Parameter keys : Symbols Deterministic (narg 1) .
Notation "'key'" := (FuncInt Deterministic (narg 1) Pkeys).

(*Encryption*)
Parameter Sencs : Symbols Deterministic (narg 3).
Notation "'Senc'" := (FuncInt Deterministic (narg 3) Sencs).
Notation "'⎨' c1 '⎬_' c2 'ꞈ' c3 " := (Senc [c1; c2; c3]) (at level 100, left associativity). 




(**************************
 **   Nonce management   **
 **************************)

Notation pk0  := (Pkey [nonce 0]).
Notation sk0  := (Skey [nonce 0]).
Notation vk0  := (Pkey [nonce 1]).
Notation ssk0 := (Skey [nonce 1]).

Notation pk1  := (Pkey [nonce 2]).
Notation sk1  := (Skey [nonce 2]).
Notation vk1  := (Pkey [nonce 3]).
Notation ssk1 := (Skey [nonce 3]).

Notation pkRA  := (Pkey [nonce 4]).
Notation skRA  := (Skey [nonce 4]).
Notation vkRA  := (Pkey [nonce 5]).
Notation sskRA := (Skey [nonce 5]).

Notation pkS  := (Pkey [nonce 6]).
Notation skS  := (Skey [nonce 6]).
Notation vkS  := (Pkey [nonce 7]).
Notation sskS := (Skey [nonce 7]).

Notation symk := (key [nonce 8]).



(** Randomness for Enc, Sign and Pseudonym **)
Notation r7   := (Rand [nonce 30]).
Notation r8   := (Rand [nonce 31]).
Notation rs9  := (Rand [nonce 32]).
Notation rs10 := (Rand [nonce 33]).
Notation ps11  := (nonce 34).
Notation ps12  := (nonce 35).
Notation ps13  := (nonce 36).
Notation rRA14   := (Rand [nonce 37]).
Notation rRA15   := (Rand [nonce 38]).
Notation rRA16   := (Rand [nonce 39]).
Notation rsRA17  := (Rand [nonce 40]).
Notation rsRA18  := (Rand [nonce 41]).
Notation rsRA19  := (Rand [nonce 42]).
Notation r20   := (Rand [nonce 43]).
Notation r21   := (Rand [nonce 44]).
Notation rs22  := (Rand [nonce 45]).
Notation rs23  := (Rand [nonce 46]).
Notation rRA24  := (Rand [nonce 47]).
Notation rRA25  := (Rand [nonce 48]).
Notation rRA26  := (Rand [nonce 49]).
Notation rsRA27  := (Rand [nonce 50]).
Notation rsRA28  := (Rand [nonce 51]).
Notation rsRA29  := (Rand [nonce 52]).
Notation rRA30  := (Rand [nonce 53]). (**)




(************************
 **   Bidding prices   **
 ************************)

Parameter Bidding0s : Symbols Deterministic (narg 0).
Notation bd0 := (ConstInt Deterministic Bidding0s).

Parameter Bidding1s : Symbols Deterministic (narg 0).
Notation bd1 := (ConstInt Deterministic Bidding1s).

Notation hbd0 := (Ｈ bd0).
Notation hbd1 := (Ｈ bd1).


(* number of biddings *)
Parameter Ns : Symbols Deterministic (narg 0).
Notation n := (ConstInt Deterministic Ns).


(**************************
 **   Index management   **
 **************************)

(* i index for bidder *)
Inductive iIndex : Set :=
| I0
| I1.
Definition negi (i : iIndex) : iIndex := match i with I0 => I1 | I1 => I0 end.
Definition pk (i : iIndex) : ppt := match i with I0 => pk0 | I1 => pk1 end.
Definition sk (i : iIndex) : ppt := match i with I0 => sk0 | I1 => sk1 end.
Definition vk (i : iIndex) : ppt := match i with I0 => vk0 | I1 => vk1 end.
Definition ssk (i : iIndex) : ppt := match i with I0 => ssk0 | I1 => ssk1 end.


(** x index for messages **)
Inductive xIndex : Set :=
| X0
| X1.
Definition negx (x : xIndex) : xIndex := match x with X0 => X1 | X1 => X0 end.
Definition bd  (x : xIndex) : ppt := match x with X0 => bd0  | X1 => bd1  end.
Definition hbd (x : xIndex) : ppt := match x with X0 => hbd0 | X1 => hbd1 end.


(* k index for all the phase 1 reponses from RA *)
Inductive kIndex : Set :=
| K1
| K2
| K3.
Definition tau (k : kIndex) : list ppt -> ppt := match k with K1 => Tau1 | K2 => Tau2 | K3 => Tau3 end.


(* kk index for all the phase 2 responses from RA *)
Inductive kkIndex : Set :=
| KK1
| KK2
| KK3.


(* randomness used in phase1 *)
Definition r1 (i : iIndex) : ppt := match i with I0 => r7 | I1 => r8 end.
Definition rs1 (i : iIndex) : ppt := match i with I0 => rs9 | I1 => rs10 end.
Definition ps (k : kIndex) : ppt := match k with K1 => ps11 | K2 => ps12 | K3 => ps13 end.
Definition P (i : iIndex) (x: xIndex) (k : kIndex) : ppt := Pseudonym [pk i; vk i; hbd x; ps k].



(* randomness used in phase2 *)
Definition rRA (k : kIndex) : ppt := match k with K1 => rRA14 | K2 => rRA15  | K3 => rRA16 end.  (* Randomness used in the public key encryption by RA*)
Definition rsRA (k : kIndex) : ppt := match k with K1 => rsRA17 | K2 => rsRA18  | K3 => rsRA19 end. (* Randomness used in the signature of RA *)

(* randomness used in phase3 *)
Definition r3  (i : iIndex) : ppt := match i with I0 => r20  | I1 => r21 end.
Definition rs3 (i : iIndex) : ppt := match i with I0 => rs22 | I1 => rs23 end.



(* randomness used in phase4 & phase5 *)
Definition r4  (kk : kkIndex) : ppt := match kk with KK1 => rRA24  | KK2 => rRA25 | KK3 => rRA26 end. (* randomness for sym enc *)
Definition rs4 (kk : kkIndex) : ppt := match kk with KK1 => rsRA27  | KK2 => rsRA28 | KK3 => rsRA29 end.


(****************
 **   Lemma1   **
 ****************)

Definition aid i  := (＜ pk i , vk i ＞).
Definition B1 i x := ❴＜ aid i,  (hbd x), ｛hbd x｝_(ssk i) ˆ (rs1 i) ＞❵_pkRA ＾ (r1 i).

(* we prove prop1 in a different way, with logical relation between i and x. or called trace indistinguishability?  *)
Theorem prop1 : forall x i,
  [bd x; B1 i x]
 ~
  [bd x; B1 (negi i) (negx x)].
Admitted.

  

(****************
 **   Lemma2   **
 ****************)

Definition adv2 c1 c2:= (adv (2) [c1; c2]).
Notation Dec_RA c := (Dec [c; skRA]).

(* For the Minimalist principle, we will not use this "If b Then Error Else dec blabla" format.*)

Definition C1 x0 x1 k := (tau k [adv2 (B1 I0 x0) (B1 I1 x1)]) ≟ (B1 I0 x0).
Definition C2 x0 x1 k := (tau k [adv2 (B1 I0 x0) (B1 I1 x1)]) ≟ (B1 I1 x1).
Definition t i x k := (❴＜ P i x k, (hbd x) ＞❵_(pk i) ＾ (rRA k)).
Definition t1 x k := (❴＜ P I0 x k, (hbd x) ＞❵_pk0 ＾ (rRA k)).
Definition t2 x k := (❴＜ P I1 x k, (hbd x) ＞❵_pk1 ＾ (rRA k)).


Definition d x0 x1 k := Dec_RA (tau k [adv2 (B1 I0 x0) (B1 I1 x1)]).


Definition R1 x0 x1 k := (If Ver [τ2 (d x0 x1 k); τ3 (d x0 x1 k);  π2 (τ1 (d x0 x1 k))] (* should be no i in R1 *)
 Then ＜(❴＜(Pseudonym [(π1 (τ1 (d x0 x1 k))); (π2 (τ1 (d x0 x1 k))); (τ2 (d x0 x1 k)); ps k]), τ2 (d x0 x1 k)＞❵_(π1 (τ1 (d x0 x1 k))) ＾ (rRA k)),
    (  ｛❴＜(Pseudonym [(π1 (τ1 (d x0 x1 k))); (π2 (τ1 (d x0 x1 k))); (τ2 (d x0 x1 k)); ps k]), τ2 (d x0 x1 k)＞❵_(π1 (τ1 (d x0 x1 k))) ＾ (rRA k)｝_sskRA ˆ (rsRA k))＞ Else Error).

(* in R1 there should not be a i. i should be specialized.*)

(* This supposes to be Lemma 2 from Ali's thesis*)
Lemma prop2_lemma2: forall k,
    R1 X0 X1 k = If C1 X0 X1 k Then ＜ (t1 X0 k), (｛t1 X0 k｝_sskRA ˆ (rsRA k))＞
           Else (If C2 X0 X1 k Then ＜ (t2 X1 k), (｛t2 X1 k｝_sskRA ˆ (rsRA k))＞
           Else (R1 X0 X1 k)).
Proof.
  intros.
  rewrite <- (@If_same (C1 X0 X1 k) (R1 X0 X1 k)) at 1.
  rewrite <- (@If_same (C2 X0 X1 k) (R1 X0 X1 k)) at 2. 
  rewrite (Eq_branch  (tau k [adv2 (B1 I0 X0) (B1 I1 X1)]) (B1 I0 X0)
                      (fun x => If Ver [τ2 (Dec_RA x); τ3 (Dec_RA x); π2 τ1 (Dec_RA x)]
                             Then ＜❴＜ (Pseudonym [(π1 (τ1 (Dec_RA x))); (π2 (τ1 (Dec_RA x))); (τ2 (Dec_RA x)); ps k]), τ2 (Dec_RA x) ＞ ❵_ π1 τ1 (Dec_RA x) ＾ rRA k,
                                 ｛ ❴＜ (Pseudonym [(π1 (τ1 (Dec_RA x))); (π2 (τ1 (Dec_RA x))); (τ2 (Dec_RA x)); ps k]), τ2 (Dec_RA x) ＞ ❵_ π1 τ1 (Dec_RA x) ＾ rRA k ｝_ sskRA ˆ rsRA k ＞ Else Error) _ ).
  rewrite (Eq_branch  (tau k [adv2 (B1 I0 X0) (B1 I1 X1)]) (B1 I1 X1)
                      (fun x => If Ver [τ2 (Dec_RA x); τ3 (Dec_RA x); π2 τ1 (Dec_RA x)]
                             Then ＜❴＜ (Pseudonym [(π1 (τ1 (Dec_RA x))); (π2 (τ1 (Dec_RA x))); (τ2 (Dec_RA x)); ps k]), τ2 (Dec_RA x) ＞ ❵_ π1 τ1 (Dec_RA x) ＾ rRA k,
                                 ｛ ❴＜ (Pseudonym [(π1 (τ1 (Dec_RA x))); (π2 (τ1 (Dec_RA x))); (τ2 (Dec_RA x)); ps k]), τ2 (Dec_RA x) ＞ ❵_ π1 τ1 (Dec_RA x) ＾ rRA k ｝_ sskRA ˆ rsRA k ＞ Else Error) _ ).
  unfold B1 at 4 5 6 7 8 9 10 11 12 13 14 15 16; simpl.
  repeat (rewrite decenc; rewrite Tau2Tri; rewrite Tau3Tri; rewrite Tau1Tri; unfold aid; rewrite proj2pair; simpl; rewrite correctness;
          rewrite If_true; rewrite proj1pair).
  unfold B1 at 7 8 9 10 11 12 13 14 15 16 17 18 19; simpl.
  repeat (rewrite decenc; rewrite Tau2Tri; rewrite Tau3Tri; rewrite Tau1Tri; unfold aid; rewrite proj2pair; simpl; rewrite correctness;
          rewrite If_true; rewrite proj1pair).
  reflexivity.
  all : ProveContext. Qed.


Theorem prop2 : forall x i k,
  [bd x; B1 i x;               R1 X0 X1 k]
 ~
  [bd x; B1 (negi i) (negx x); R1 X0 X1 k].
Admitted.



(****************
 **   Lemma3   **
 ****************)

Notation adv5 t2_vec:= (adv (5) t2_vec).
Definition t2_vec x0 x1 := [ bd x0 ; bd x1 ; B1 I0 x0 ; B1 I1 x1 ; R1 x0 x1 K1 ].

Definition C21 x0 x1 k:= ((π1 (adv5 (t2_vec x0 x1))) ≟ (t1 x0 k)).
Definition C22 x0 x1 k:= ((π1 (adv5 (t2_vec x0 x1))) ≟ (t2 x1 k)).
Definition C2i i x0 x1 k := match i with I0 => C21 x0 x1 k | I1 => C22 x0 x1 k end.

Notation Dec_i i c := (Dec [c; (sk i)]).
Definition ti i x0 x1 k := match i with I0 => t1 x0 k | I1 => t2 x1 k end.
Definition hbdi i x0 x1 := match i with I0 => hbd x0 | I1 => hbd x1 end.
Definition bdi i x0 x1 := match i with I0 => bd x0 | I1 => bd x1 end.

(* Definition B1 i x := ❴＜ aid i,  (hbd x), ｛hbd x｝_(ssk i) ˆ (rs1 i) ＞❵_pkRA ＾ (r1 i). *)

(* i can be I0 and I1 *)
Definition B2 i x0 x1 k := (If Ver [ π1 (adv5 (t2_vec x0 x1)); ｛(ti i x0 x1 k)｝_sskRA ˆ (rsRA k) ; vkRA]
                             Then If (hbdi i x0 x1) ≟ π2 (Dec_i i (π1 (adv5 (t2_vec x0 x1))))
                                     Then ＜ (  ❴ bdi i x0 x1 ❵_pkS ＾ (r3 i)),
                                             (｛❴ bdi i x0 x1 ❵_pkS ＾ (r3 i)｝_(ssk i) ˆ (rs3 i)),
                                             ( π1 (Dec_i i (π1 (adv5 (t2_vec x0 x1))))) ＞
                                     Else Error
                                Else Error).

Definition t3i i x :=  ❴ bd x ❵_ pkS ＾ (r3 i).
Definition realBd i x k := (＜ t3i i x, ｛t3i i x｝_ (ssk i) ˆ (rs3 i), ( P i x k) ＞).

Axiom Correctness : forall {m n r}, Ver [m; ｛ m ｝_ Skey [nonce (n)] ˆ r; Pkey [nonce (n)]] = TRue.
Axiom Decenc : forall {m n n'}, Dec [❴ m ❵_ Pkey [n] ＾ n'; Skey [n]] = m.
Axiom Ceq_ref: forall m,  ((m ≟ m) = TRue).

Definition i2x (i: iIndex) (x0 x1 :xIndex):= match i with I0 => x0 | I1 => x1 end.

Lemma prop3_lemma3: forall i k x0 x1,
    B2 i x0 x1 k = If C2i i x0 x1 k Then (realBd i (i2x i x0 x1) k) Else (B2 i x0 x1 k).
Proof.
  intros.
  rewrite <- (@If_same (C2i i x0 x1 k) (B2 i x0 x1 k)) at 1. unfold B2 at 1.
  destruct i; simpl. unfold C21 at 1.

  - rewrite (Eq_branch (π1 adv5 (t2_vec x0 x1)) (t1 x0 k)
                       (fun x => ( If Ver [x; _; _] Then If _ ≟ (π2 Dec [x; sk0]) Then ＜ _ , _ , π1 Dec [x; sk0] ＞ Else _ Else _ ))).
    rewrite Correctness; rewrite If_true; unfold t1; rewrite Decenc; unfold P; rewrite proj2pair; rewrite proj1pair; rewrite Ceq_ref; rewrite If_true; simpl.
    reflexivity.
    ProveContext.

  - rewrite (Eq_branch (π1 adv5 (t2_vec x0 x1)) (t2 x1 k)
                       (fun x => ( If Ver [x; _; _] Then If _ ≟ (π2 Dec [x; sk1]) Then ＜ _ , _ , π1 Dec [x; sk1] ＞ Else _ Else _ ))).
    rewrite Correctness; rewrite If_true; unfold t2; rewrite Decenc; unfold P; rewrite proj2pair; rewrite proj1pair; rewrite Ceq_ref; rewrite If_true; simpl.
    reflexivity.
    ProveContext.
Qed.  


Theorem prop3 : forall x i k,
  [hbd x; bd x; B1 i x;               R1 X0 X1 k; B2 i x (negx x) k]
 ~
  [hbd x; bd x; B1 (negi i) (negx x); R1 X1 X0 k; B2 (negi i) (negx x) x k ].
Admitted.





(****************
 **   Lemma4   **
 ****************)


Notation adv7 t3_vec:= (adv (5) t3_vec).
Definition t3_vec x0 x1 := [ bd x0 ; bd x1 ; B1 I0 x0 ; B1 I1 x1 ; R1 x0 x1 K1; B2 I1 x0 x1 K1 ].

Definition C31 i x0 x1 k:= ((adv7 (t3_vec x0 x1)) ≟ (realBd i x0 k)).
Definition C32 i x0 x1 k:= ((adv7 (t3_vec x0 x1)) ≟ (realBd i x1 k)).

Definition R2 x0 x1 kk := (If Ver [τ1 (adv7 (t3_vec x0 x1)); τ2 (adv7 (t3_vec x0 x1));  getVK [τ3 (adv7 (t3_vec x0 x1))]]
 Then (｛(τ1 (adv7 (t3_vec x0 x1)))｝_(sskRA) ˆ (rs4 kk)) Else Error).



(* This supposes to be corresponding to the expansion {on Page 21} from Ali's thesis *)
Lemma prop4_lemma: forall k kk,
     R2 X0 X1 kk  = If C31 I0 X0 X1 k Then (｛❴ bd X0 ❵_ pkS ＾ (r3 I0)｝_(sskRA) ˆ (rs4 kk))
              Else (If C32 I1 X0 X1 k Then (｛❴ bd X1 ❵_ pkS ＾ (r3 I1)｝_(sskRA) ˆ (rs4 kk))
                       Else (R2 X0 X1 kk)).
Proof.
  intros.
  rewrite <- (@If_same (C31 I0 X0 X1 k) (R2 X0 X1 kk)) at 1.
  rewrite <- (@If_same (C32 I1 X0 X1 k) (R2 X0 X1 kk)) at 2. unfold R2 at 1 2.
  rewrite (Eq_branch  (adv7 (t3_vec X0 X1)) (realBd I0 X0 k)
                      (fun x =>  If Ver [τ1 x; τ2 x; getVK [τ3 x]] Then (｛ τ1 x ｝_ sskRA ˆ rs4 kk ) Else _ )).
  rewrite (Eq_branch  (adv7 (t3_vec X0 X1)) (realBd I1 X1 k)
                      (fun x => If Ver [τ1 x; τ2 x; getVK [τ3 x]] Then ｛ τ1 x ｝_ sskRA ˆ rs4 kk Else Error)).
  unfold realBd at 2 3 4 5 7 8 9 10; simpl.
  repeat (rewrite Tau1Tri; rewrite Tau2Tri; rewrite Tau3Tri).
  repeat (unfold P; rewrite P2Vk; simpl; rewrite correctness; rewrite If_true).
  reflexivity.
  all: ProveContext.
Qed. 


Theorem prop4 : forall x i k kk,
  [hbd x; bd x; B1 i x;               R1 X0 X1 k; B2 i x (negx x) k;        R2 X0 X1 kk]
 ~
  [hbd x; bd x; B1 (negi i) (negx x); R1 X1 X0 k; B2 (negi i) (negx x) x k; R2 X1 X0 kk].
Admitted.






(****************
 **   Lemma5   **
 ****************)

Definition t4 i x k kk := ⎨ ＜ (P i x k) , (t3i i x), (hbd x) ＞ ⎬_ symk ꞈ r4 kk.
                                                              

Definition R3 x0 x1 kk := (If Ver [τ1 (adv7 (t3_vec x0 x1)); τ2 (adv7 (t3_vec x0 x1));  getVK [τ3 (adv7 (t3_vec x0 x1))]]
 Then (⎨＜ (τ3 (adv7 (t3_vec x0 x1))) , (τ1 (adv7 (t3_vec x0 x1))), getHashedBid [τ3 (adv7 (t3_vec x0 x1))] ＞⎬_ symk ꞈ (r4 kk))  Else Error).

(* This supposes to be corresponding to the expansion {on Page 24} from Ali's thesis *) 
Lemma prop5_lemma: forall k kk,
     R3 X0 X1 kk  = If C31 I0 X0 X1 k Then (t4 I0 X0 k kk)
              Else (If C32 I1 X0 X1 k Then (t4 I1 X1 k kk)
                       Else (R3 X0 X1 kk)).
Proof.
  intros.
  rewrite <- (@If_same (C31 I0 X0 X1 k) (R3 X0 X1 kk)) at 1.
  rewrite <- (@If_same (C32 I1 X0 X1 k) (R3 X0 X1 kk)) at 2. unfold R3 at 1 2.
  rewrite (Eq_branch  (adv7 (t3_vec X0 X1)) (realBd I0 X0 k)
                      (fun x =>  If Ver [τ1 x; τ2 x; getVK [τ3 x]] Then ⎨＜ τ3 x, τ1 x, getHashedBid [τ3 x] ＞⎬_ _ ꞈ _ Else _ )).
  rewrite (Eq_branch  (adv7 (t3_vec X0 X1)) (realBd I1 X1 k)
                      (fun x => If Ver [τ1 x; τ2 x; getVK [τ3 x]] Then ⎨＜ τ3 x, τ1 x, getHashedBid [τ3 x] ＞⎬_ _ ꞈ _ Else _ )).
  unfold realBd at 2 3 4 5 6 7 9 10 11 12 13 14; simpl.
  repeat (rewrite Tau1Tri; rewrite Tau2Tri; rewrite Tau3Tri).
  repeat (unfold P; rewrite P2Vk; simpl; rewrite correctness; rewrite If_true; rewrite P2Hbd).
  reflexivity.
  all : ProveContext.  
Qed.


Theorem prop5 : forall x i k kk,
  [hbd x; bd x; B1 i x;               R1 X0 X1 k; B2 i x (negx x) k;        R2 X0 X1 kk;  R3 X0 X1 kk ]
 ~
  [hbd x; bd x; B1 (negi i) (negx x); R1 X1 X0 k; B2 (negi i) (negx x) x k; R2 X1 X0 kk;  R3 X1 X0 kk].
Admitted.




(****************
 **   Lemma6   **
 ****************)


Definition RS3 := ❴ ＜ symk , n ＞ ❵_ pkS ＾ rRA30.

(* This supposes to be corresponding to the expansion {on Page 27} from Ali's thesis *)

Theorem prop6 : forall x i k kk,
  [hbd x; bd x; B1 i x;               R1 X0 X1 k; B2 i x (negx x) k;        R2 X0 X1 kk;  R3 X0 X1 kk; RS3]
 ~
  [hbd x; bd x; B1 (negi i) (negx x); R1 X1 X0 k; B2 (negi i) (negx x) x k; R2 X1 X0 kk;  R3 X1 X0 kk; RS3].
Admitted.



