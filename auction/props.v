(************************************************************************)
(* Copyright (c) 2021, Gergei Bana, Qianli Zhang                        *)
(*                                                                      *)
(* This work is licensed under the MIT license. The license is          *)
(* described in the file "LICENSE" available at the root of the source  *)
(* or at https://opensource.org/licenses/MIT                            *)
(************************************************************************)

(*   Unicode used in this file:
 *   1.'｛' :     U+FF5B
 *   2.'｝' :     U+FF5D
 *   3.'ˆ'  :     U+02C6
 *   4.'Ｈ' :     U+03C0
 *   5. 'τ' :     U+03C4*)

Require Import Coq.Lists.List.
Require Import Coq.micromega.Lia.
Import ListNotations.
Require Export AuxiliaryTheoremEquality.


(****************
 **   Lemma1   **
 ****************)





(****************
 **   Lemma2   **
 ****************)

(* This supposes to be Lemma 2 from Ali's thesis essentially*)



(****************
 **   Lemma3   **
 ****************)



(****************
 **   Lemma4   **
 ****************)

(* This supposes to be corresponding to the expansion {on Page 21} from Ali's thesis *)




(****************
 **   Lemma5   **
 ****************)

(* This supposes to be corresponding to the expansion {on Page 24} from Ali's thesis *)



(****************
 **   Lemma6   **
 ****************)


(* This supposes to be corresponding to the expansion {on Page 29} from Ali's thesis *)
