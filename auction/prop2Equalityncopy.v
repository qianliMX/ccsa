(************************************************************************)
(* Copyright (c) 2021, Gergei Bana, Qianli Zhang                        *)   
(*                                                                      *)
(* This work is licensed under the MIT license. The license is          *)
(* described in the file "LICENSE" available at the root of the source  *)
(* or at https://opensource.org/licenses/MIT                            *)
(************************************************************************)

Require Export Coq.Lists.List.
Import ListNotations.
Require Import Coq.micromega.Lia. 
Require Export prop1Equalityncopy.


Axiom Tau1Tri :
      forall {x1 x2 x3: ppt} ,
        (Tau1 [Triple [x1; x2; x3]]) = x1.

Axiom Tau2Tri :
      forall {x1 x2 x3: ppt} ,
        (Tau2 [Triple [x1; x2; x3]]) = x2.

Axiom Tau3Tri :
      forall {x1 x2 x3: ppt} ,
        (Tau3 [Triple [x1; x2; x3]]) = x3.





(*how the RA connects the "B1_0 x1" with pseudonym?*)
Parameter Pseudonyms : Symbols Deterministic (narg 3).
Notation Pseudonym := (FuncInt Deterministic (narg 3) Pseudonyms).

(**)
Notation np1  := (nonce 16).
Notation np2  := (nonce 17).
Notation np3  := (nonce 18).

Notation rRA1   := (Rand [nonce 10]).
Notation rRA2   := (Rand [nonce 19]).
Notation rRA3   := (Rand [nonce 20]).



(*the return value of adv2 should be a triple vector, then we can use τ?*)
Notation adv2 c1 c2:= (adv (2) [c1; c2]).
Notation Dec_RA c := (Dec [c; skRA]).
Notation Dec_RAif x d := (If (EQ [d ;x]) Then Error Else (Dec [d; skRA])).


(*why we have to add τ after adv2? when we use if-then-else, we need to compare "τ1 (adv2 (B1_0 x1) (B1_1 x2))" with "B1_0 x1"*)
Definition C1 k x1 x2 := tau k [adv2 (B1 I0 x1) (B1 I1 x2)] ≟ B1 I0 x1 .
Definition C2 k x1 x2 := tau k [adv2 (B1 I0 x1) (B1 I1 x2)] ≟ B1 I1 x2 .
Definition C i k x1 x2 := match i with
                          | I0 => C1 k x1 x2
                          | I1 => C2 k x1 x2
                          end.





Definition d k x1 x2 := Dec_RA (tau k [adv2 (B1 I0 x1) (B1 I1 x2)]).
Definition dif i k x1 x2 :=  If (C i k x1 x2) Then Error Else (d k x1 x2) .



Definition np k  := match k with K1 => np1 | K2 => np2 | K3 => np3 end.



(*we use the public key, public verification key and a randomness to generate the pseudonyms*)
Definition P k x1 x2 := (Pseudonym [π1 (τ1 (d k x1 x2)); π2 (τ1 (d k x1 x2)); (np k)]).
Definition Pif i k x1 x2 := (Pseudonym [π1 (τ1 (dif i k x1 x2)); π2 (τ1 (dif i k x1 x2)); (np k)]).


      
Definition rRA k  := match k with K1 => rRA1 | K2 => rRA2 | K3 => rRA3 end.




(*Here randomness should be different, while this doesn't affect the final result*)
(*And here the randomness rRA and rsRA should be different in different encryption and signature*)
Definition R1 k x1 x2 := (If Ver [τ2 (d k x1 x2); τ3 (d k x1 x2);  π2 (τ1 (d k x1 x2))]
                             Then ｛❴＜P k x1 x2, τ2 (d k x1 x2)＞❵_(π1 (τ1 (d k x1 x2))) ＾ (rRA k)｝_sskRA ˆ rsRA
                             Else Error).

Definition R1if i k x1 x2 := (If Ver [τ2 (dif i k x1 x2); τ3 (dif i k x1 x2);  π2 (τ1 (dif i k x1 x2))]
                             Then ｛❴＜Pif i k x1 x2, τ2 (dif i k x1 x2)＞❵_(π1 (τ1 (dif i k x1 x2))) ＾ (rRA k)｝_sskRA ˆ rsRA
                             Else Error).



(* This is essentially Lemma 2 from Ali's thesis: *)

Lemma prop2_lemma2:
  forall  k x1 x2,
    R1 k x1 x2 = If C1 k x1 x2
                    Then｛❴＜Pseudonym [pk0; vk0  ; np k], x1 ＞❵_pk0 ＾ (rRA k)｝_sskRA ˆ rsRA
                    Else (If C2 k x1 x2
                             Then｛❴＜Pseudonym [pk1; vk1  ; np k], x2 ＞❵_pk1 ＾ (rRA k) ｝_sskRA ˆ rsRA
                             Else (If C1 k x1 x2
                                      Then Error
                                      Else (If C2 k x1 x2 Then Error Else (R1 k x1 x2) )
                                  )
                         ).

Proof. 
  intros.
  rewrite @Example7_1 with (b:=  C2 k x1 x2) (x1 := default) (y1 := default)
                           (t1 := (fun x :ppt =>  ｛ ❴ ＜ Pseudonym [pk1; vk1; np k], x2 ＞ ❵_ pk1 ＾ rRA k ｝_ sskRA ˆ rsRA))
                           (t2 := (fun x :ppt =>   If (C1 k x1 x2) Then Error Else x ) ).
  rewrite @Example7_1 with (b:=  C1 k x1 x2) (x1 := default) (y1 := default)
                           (t1 := (fun x :ppt =>  ｛ ❴ ＜ Pseudonym [pk0; vk0; np k], x1 ＞ ❵_ pk0 ＾ rRA k ｝_ sskRA ˆ rsRA))
                           (t2 := (fun x :ppt =>   If (C2 k x1 x2)
                                                      Then ｛ ❴ ＜ Pseudonym [pk1; vk1; np k], x2 ＞ ❵_ pk1 ＾ rRA k ｝_ sskRA ˆ rsRA
                                                      Else x )).
  rewrite <- (@If_same (C1 k x1 x2 ) ( R1 k x1 x2)) at 1.
  rewrite <- (@If_same (C2 k x1 x2 ) ( R1 k x1 x2)) at 2. 
  unfold R1 at 1.  unfold C1 at 1. unfold P. unfold d.
  rewrite (Eq_branch (tau k [adv2 (B1 I0 x1) (B1 I1 x2)])
                     (B1 I0 x1)
                     (fun x : ppt =>  If Ver [τ2 (Dec_RA ( x )); τ3 (Dec_RA ( x ) ); π2 τ1 (Dec_RA ( x ))]
                                         Then ｛ ❴ ＜ Pseudonym [π1 τ1 (Dec_RA ( x )); π2 τ1 (Dec_RA ( x )); np k ],
                                                   τ2 (Dec_RA ( x  )) ＞
                                                 ❵_ π1 τ1 (Dec_RA ( x  )) ＾ ( rRA k ) 
                                              ｝_ sskRA ˆ rsRA
                                         Else Error ) ).
  unfold B1 at 4 5 6 7 8 9 10. unfold r.
  rewrite decenc. 
  rewrite Tau1Tri. rewrite Tau2Tri. rewrite Tau3Tri.
  unfold ssk. unfold rs. unfold aid. unfold pk. unfold vk.
  rewrite proj1pair. rewrite proj2pair. 
  rewrite correctness.
  repeat rewrite ceqeq.
  repeat rewrite If_true.
  fold  (C1 k x1 x2).
  unfold R1 at 1. unfold C2 at 1.  unfold P. unfold d. 
  rewrite (Eq_branch (tau k [adv2 (B1 I0 x1) (B1 I1 x2)])
                     (B1 I1 x2)
                     (fun x : ppt =>  If Ver [τ2 (Dec_RA ( x )); τ3 (Dec_RA ( x ) ); π2 τ1 (Dec_RA ( x ))]
                                         Then ｛ ❴ ＜ Pseudonym [π1 τ1 (Dec_RA ( x )); π2 τ1 (Dec_RA ( x )); np k ],
                                                   τ2 (Dec_RA ( x  )) ＞
                                                 ❵_ π1 τ1 (Dec_RA ( x  )) ＾ ( rRA k ) 
                                              ｝_ sskRA ˆ rsRA
                                         Else Error ) ).
  unfold B1 at 4 5 6 7 8 9 10. unfold r.
  rewrite decenc.
  rewrite Tau1Tri. rewrite Tau2Tri. rewrite Tau3Tri.
  unfold ssk. unfold rs. unfold aid. unfold pk. unfold vk.
  rewrite proj1pair. rewrite proj2pair. 
  rewrite correctness.
  repeat rewrite ceqeq.
  repeat rewrite If_true.
  fold  (C2 k x1 x2).
  reflexivity.
  all: destruct k ; unfold tau; ProveContext; unfold tau; ProvePPT.
Qed.



(* Here we introduce if then else in front of Decryptions to match the CCA2 definition *)     

Proposition CCA2_form_for_pkRA :
  forall i k x1 x2,
    If C1 k x1 x2
       Then Error
       Else (If (C2 k x1 x2) Then Error Else (R1 k x1 x2)) =
    If C1 k x1 x2
       Then Error
       Else (If (C2 k x1 x2) Then Error Else (R1if i k x1 x2)).
Proof.
  intros.
  unfold R1; unfold R1if. unfold Pif.  unfold dif. 
  destruct i.
  unfold C.
  rewrite @If_eval with (b :=  C1 k x1 x2)  (tc1 := (fun x => Error))  (tc2 := (fun x =>  If C2 k x1 x2
                                                                                         Then Error
                                                                                         Else (If Ver [τ2 (If x Then Error Else d k x1 x2);
                                                                                                      τ3 (If x Then Error Else d k x1 x2);
                                                                                                      π2 τ1 (If x Then Error Else d k x1 x2)]
                                                                                                  Then ｛ ❴ ＜ Pseudonym [π1 τ1 (If x  Then Error Else d k x1 x2); π2 τ1 (If x Then Error Else d k x1 x2); np k],
                                                                                                            τ2 (If x Then Error Else d k x1 x2) ＞
                                                                                                          ❵_ π1 τ1 (If x Then Error Else d k x1 x2)
                                                                                                           ＾ rRA k ｝_ sskRA ˆ rsRA Else Error ))).
  rewrite If_false.
  unfold P.
  reflexivity.
  ProveContext.  ProveContext ; destruct k ; unfold tau ; ProvePPT. 
  unfold C. 
  rewrite @If_eval with (b :=  C2 k x1 x2)  (tc1 := (fun x => Error))  (tc2 := (fun x =>  If Ver [τ2 (If x Then Error Else d k x1 x2);
                                                                                                  τ3 (If x Then Error Else d k x1 x2);
                                                                                                  π2 τ1 (If x Then Error Else d k x1 x2)]
                                                                                             Then ｛ ❴ ＜ Pseudonym [π1 τ1 (If x  Then Error Else d k x1 x2); π2 τ1 (If x Then Error Else d k x1 x2); np k],
                                                                                                       τ2 (If x Then Error Else d k x1 x2) ＞
                                                                                                     ❵_ π1 τ1 (If x Then Error Else d k x1 x2)
                                                                                                      ＾ rRA k ｝_ sskRA ˆ rsRA Else Error )).
  rewrite If_false.
  unfold P. 
  reflexivity.
  ProveContext.  ProveContext ; destruct k ; unfold tau ; ProvePPT. 
Qed. 


Notation np11  := (nonce 161).
Notation np12  := (nonce 162).


Notation rRA11   := (Rand [nonce 101]).
Notation rRA12   := (Rand [nonce 191]).




(* The following Proposition contains all branches for R1 K1.:  *)

Proposition prop2small_allbranches :
  cca_2
  -> [ bd I0 ; bd I1 ; B1 I0 hbd0 ; B1 I1 hbd1 ;
     C1 K1 hbd0 hbd1 ; C2 K1 hbd0 hbd1 ;
     ｛❴＜Pseudonym [pk0; vk0  ; np11], hbd0 ＞❵_pk0 ＾ rRA11｝_sskRA ˆ rsRA ; ｛❴＜Pseudonym [pk1; vk1  ; np12], hbd1 ＞❵_pk1 ＾ rRA1 ｝_sskRA ˆ rsRA ;
     If C1 K1 hbd0 hbd1
        Then Error
        Else (If (C2 K1 hbd0 hbd1) Then Error Else (R1 K1 hbd0 hbd1))]
       ~
     [ bd I0 ; bd I1 ; B1 I0 hbd1 ; B1 I1 hbd0 ;
     C1 K1 hbd1 hbd0 ; C2 K1 hbd1 hbd0 ;
     ｛❴＜Pseudonym [pk0; vk0  ; np11], hbd1 ＞❵_pk0 ＾ rRA11｝_sskRA ˆ rsRA ; ｛❴＜Pseudonym [pk1; vk1  ; np12], hbd0 ＞❵_pk1 ＾ rRA1 ｝_sskRA ˆ rsRA ;
     If C1 K1 hbd1 hbd0
        Then Error
        Else (If (C2 K1 hbd1 hbd0) Then Error Else (R1 K1 hbd1 hbd0))].

  (* Prove this by using CCA2 for all encryptions. Use CCA2toCCA2L and CCA2L to avoid if and else terms with length. *)

Admitted.



(* Now we can generate all branches: *)



Proposition prop2small_branch1 :
  cca_2
  -> [ bd I0 ; bd I1 ; B1 I0 hbd0 ; B1 I1 hbd1 ;
     C1 K1 hbd0 hbd1 ;｛❴＜Pseudonym [pk0; vk0  ; np1], hbd0 ＞❵_pk0 ＾ rRA1｝_sskRA ˆ rsRA ]
       ~
     [ bd I0 ; bd I1 ; B1 I0 hbd1 ; B1 I1 hbd0 ;
     C1 K1 hbd1 hbd0 ;｛❴＜Pseudonym [pk0; vk0  ; np1], hbd1 ＞❵_pk0 ＾ rRA1｝_sskRA ˆ rsRA ].
Proof.
  assert (
[ bd I0 ; bd I1 ; B1 I0 hbd0 ; B1 I1 hbd1 ;
     C1 K1 hbd0 hbd1 ;｛❴＜Pseudonym [pk0; vk0  ; np11], hbd0 ＞❵_pk0 ＾ rRA11｝_sskRA ˆ rsRA ]
       ~
     [ bd I0 ; bd I1 ; B1 I0 hbd1 ; B1 I1 hbd0 ;
     C1 K1 hbd1 hbd0 ;｛❴＜Pseudonym [pk0; vk0  ; np11], hbd1 ＞❵_pk0 ＾ rRA11｝_sskRA ˆ rsRA ]).
  (* Prove this by first projecting (function application)  from prop2small_allbranches  to the above formula *)
(* Now replace the nonces of np11 and rRA11 to the right nonces using FreshInd and function application *)

   
Admitted.

Proposition prop2small_branch2 :
  cca_2
  -> [ bd I0 ; bd I1 ; B1 I0 hbd0 ; B1 I1 hbd1 ;
     C1 K1 hbd0 hbd1 ; C2 K1 hbd0 hbd1 ;｛❴＜Pseudonym [pk1; vk1  ; np1], hbd1 ＞❵_pk1 ＾ rRA1 ｝_sskRA ˆ rsRA]
       ~
     [ bd I0 ; bd I1 ; B1 I0 hbd1 ; B1 I1 hbd0 ;
     C1 K1 hbd1 hbd0 ; C2 K1 hbd1 hbd0 ;｛❴＜Pseudonym [pk1; vk1  ; np1], hbd0 ＞❵_pk1 ＾ rRA1 ｝_sskRA ˆ rsRA].
  (* The same method as branch1*)  
Admitted.


  

Proposition prop2small_branch3 :
  cca_2
  -> [ bd I0 ; bd I1 ; B1 I0 hbd0 ; B1 I1 hbd1 ;
     C1 K1 hbd0 hbd1 ; C2 K1 hbd0 hbd1 ; If C1 K1 hbd0 hbd1
                                            Then Error
                                            Else (If (C2 K1 hbd0 hbd1) Then Error Else (R1 K1 hbd0 hbd1))]
       ~
     [ bd I0 ; bd I1 ; B1 I0 hbd1 ; B1 I1 hbd0 ;
     C1 K1 hbd1 hbd0 ; C2 K1 hbd1 hbd0 ; If C1 K1 hbd1 hbd0
                                            Then Error
                                            Else (If (C2 K1 hbd1 hbd0) Then Error Else (R1 K1 hbd1 hbd0))].
  
(* This can be obtained from prop2small_allbranches by function application *)
  
Admitted.
  

Proposition prop2small :
  cca_2
  -> [ bd I0 ; bd I1 ; B1 I0 hbd0 ; B1 I1 hbd1 ; R1 K1 hbd0 hbd1 ]
       ~
       [ bd I0 ; bd I1 ; B1 I0 hbd1 ; B1 I1 hbd0 ; R1 K1 hbd1 hbd0 ].

(* This you can get by putting together the 3 branches. First combine branch 2 and 3 and then 1 *)  


  
Admitted.




















Proposition prop2 :
  cca_2
  -> [ bd I0 ; bd I1 ; B1 I0 hbd0 ; B1 I1 hbd1 ; R1 K1 hbd0 hbd1 ; R1 K2 hbd0 hbd1 ; R1 K3 hbd0 hbd1 ]
       ~
     [ bd I0 ; bd I1 ; B1 I0 hbd1 ; B1 I1 hbd0 ; R1 K1 hbd1 hbd0 ; R1 K2 hbd1 hbd0 ; R1 K3 hbd1 hbd0 ].
  
Proof.
  intros.
Admitted. 












(* Only keep those that are needed: *)

Lemma If_morph2 :(*"<<<"*)  forall  {f}  , (*">>>"*)
              forall  {b x1 y1 x2 y2} {u} ,
      (*"<<<"*)ContextTerm General List f -> (*">>>"*)
    (f ((If b Then x1 Else y1) :: (If b Then x2 Else y2) :: u))
  = (If b Then (f (x1 :: x2 :: u) ) Else (f (y1 :: y2 :: u))). Admitted.


Lemma If_morph3 :(*"<<<"*)  forall  {f}  , (*">>>"*)
              forall  {b x1 y1 x2 y2 x3 y3} {u} ,
      (*"<<<"*)ContextTerm General List f -> (*">>>"*)
    (f ((If b Then x1 Else y1) :: (If b Then x2 Else y2) :: (If b Then x3 Else y3) :: u))
  = (If b Then (f (x1 :: x2 :: x3 :: u) ) Else (f (y1 :: y2 :: y3 :: u))). Admitted.


(**)
Lemma If_simpl : forall {b1 b2 b bla1 bla2 bla},
  (If (If b1 Then TRue Else (If b2 Then TRue Else b)) Then (If b1 Then bla1 Else (If b2 Then bla2 Else bla)) Else Error)
 =
  (If b1 Then bla1 Else If b2 Then bla2 Else If b Then bla Else Error).
  intros.
  repeat rewrite (@If_morph (fun x => If x Then _ Else Error)).
  repeat rewrite If_true.
  rewrite (@If_eval (fun b1  => If b1 Then bla1 Else If b2 Then bla2 Else bla )
                    (fun b1  => If b2 Then If b1 Then bla1 Else If b2 Then bla2 Else bla Else If b Then If b1 Then bla1 Else If b2 Then bla2 Else bla Else Error )).
  repeat rewrite If_true.
  repeat rewrite If_false. 
  rewrite (@If_eval (fun b2  => If b2 Then bla2 Else bla  )
                    (fun b2 =>  If b Then If b2 Then bla2 Else bla Else Error)).
  repeat rewrite If_true.
  repeat rewrite If_false.
  reflexivity.
  all : ProveContext.
Qed.
  
