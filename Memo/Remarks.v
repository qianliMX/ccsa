Require Export Coq.Lists.List.
Require Export Coq.Init.Logic.
Require Export Coq.Init.Peano.
Require Export Coq.Arith.EqNat.
Require Export Setoid.
Import ListNotations.
Require Export Ensembles. 





(****************************************************************************)
(****************************************************************************)
(****************************************************************************)
(****************************************************************************)


(* Difference of Variable and Parameter *)

Section TestA.
Variable n : nat. 
Definition addn (m : nat) : nat := m + n.
Print addn.
Compute addn 2.
End TestA. 
Print addn. 
Compute addn 2.
Compute addn 2 3.

Section TestB.
Parameter n : nat. 
Definition addnn (m : nat) : nat := m + n.
Print addnn.
Compute addn 2.
End TestB. 
Print addnn.
Compute addnn 2.
(* "Compute addnn 2 3." Does not work *)


(****************************************************************************)
(****************************************************************************)
(****************************************************************************)
(****************************************************************************)


(* How free objects are not equal *)

Inductive day : Type :=
  | monday
  | tuesday
  | wednesday
  | thursday
  | friday
  | saturday
  | sunday.

Lemma noteq3 : monday <> tuesday.
Proof. discriminate. Qed. 

Inductive bla : Type :=
  | blaf : day -> bla.

Lemma noteq4 : (blaf monday) <> (blaf tuesday).
Proof.  discriminate. Qed.



Inductive GroundTerms : Type :=
| defaultT : GroundTerms
| nonceT : nat -> GroundTerms
| EQT : GroundTerms -> GroundTerms -> GroundTerms
| If_Then_Else_T : GroundTerms -> GroundTerms -> GroundTerms -> GroundTerms.


(* Term context *)

Inductive SubT : GroundTerms -> Ensemble GroundTerms :=
| tint : forall t, (In GroundTerms (SubT t) t) .
(*| tinEQ : forall t t1, t2 , (InT t (EQ t1 t2)) -> *)

Lemma noteq : forall (t t': GroundTerms) , (In GroundTerms (SubT t) t') -> t = t'.
Proof. intros. destruct H. reflexivity. Qed.

Lemma noteq0 : forall (n n': nat) , (In GroundTerms (SubT ( nonceT n )) (nonceT n')) -> nonceT n = nonceT n'.
Proof. intros. destruct H. reflexivity. Qed.

Lemma noteq2 :  nonceT 0 <> nonceT 1.
Proof. 
 discriminate. Qed.

Inductive nats2 : Type :=
| zero : nats2
| SS : nats2 -> nats2.

Lemma noteq5: zero <> SS zero.
Proof. discriminate. Qed. 

Lemma noteq6: SS (SS (SS zero)) <> SS (SS (SS (SS zero))).
Proof. discriminate. Qed. 

Fixpoint SSinv (n : nats2) : nats2 :=
match n with 
| zero => zero
| SS n' => n'
end.

Lemma SSinvIsLeftInverse : forall n, SSinv (SS n) = n.
Proof. intros. simpl. reflexivity. Qed.


Lemma noteq7 : forall n m , SS n = SS m -> n = m.
Proof. intros. assert (SSinv (SS n0) = n0). apply SSinvIsLeftInverse.
destruct H0. assert (SSinv (SS m) = m). apply SSinvIsLeftInverse.
destruct H0. destruct H. simpl. reflexivity. Qed.
Print noteq7.
(* I don't know how to do this simpler *)

Lemma noteq8 : forall n m , SS n = SS m -> n = m.
intros. inversion H. reflexivity. Qed.


