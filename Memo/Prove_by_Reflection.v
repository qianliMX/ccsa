Require Export Coq.Init.Logic.
Require Export Coq.Init.Peano.
Require Export Coq.Arith.PeanoNat.
Require Import Coq.micromega.Lia.
Require Import Coq.Bool.Bool.
Require Import Coq.Arith.Compare_dec.
Require Import Coq.Lists.List.
Import ListNotations.
Require Import FunInd.

Reserved Notation "n <=' m" (at level 70, no associativity).
Inductive le' : nat -> nat -> Prop :=
| le_n : forall n : nat, 0 <=' n 
| le_S : forall n m : nat, n <='m -> (S n) <='(S m)
where "n <=' m" := (le' n m).

(*Either 3<= 5 => 3<= 4 => 3<= 3 
  or     3<='5 => 2<='4 => 1<='3 => 0<='2 *)


Lemma le'_nn : forall n, n <=' n.
induction n; constructor; try auto.
Defined.

Lemma le'_nm : forall n m, n <=' m -> n <=' (S m).
intros.
induction H; constructor; auto.
Defined.

Lemma le_0n : forall n, 0 <= n. 
  intros.
  induction n; constructor; auto.
Defined.

Lemma le_snsn : forall n m, n <= m -> S n <= S m.
  intros.
  induction H; constructor; auto.
Defined.
  

Theorem le_le' : forall n m, n <= m <-> n <=' m.
split; intros; induction H.
apply le'_nn.
apply le'_nm; auto.
apply le_0n.
apply le_snsn; auto.
Defined.


(*Nat.leb = 
fix leb (n m : nat) {struct n} : bool :=
  match n with
  | 0 => true
  | S n' => match m with
            | 0 => false
            | S m' => leb n' m'
            end
  end*)
(*3 <=? 5 => 2 <=? 4 => 1 <=? 3 => 0<=? _ => true*)


Theorem leb_le: forall n m : nat, n <= m <-> (n <=? m) = true.
split; intros.
+
  apply le_le' in H.
  induction H; auto.
+
  apply le_le'.
  generalize dependent m.
  induction n; intros.
  -
    constructor.
  -
    induction m; intros.
    simpl in H; inversion H. (* absurd in H*)
    constructor.
    simpl in H.
    apply IHn; auto.
Defined.

(*It seems that functional induction could be really powerful
  and it serves the rule as le', is that true? *)
Functional Scheme leb_ind := Induction for Nat.leb Sort Prop.

Theorem leb_le':  forall n m : nat, (n <=? m) = true -> n <= m.
  intros.
  functional induction  (n <=? m) using leb_ind.
  + apply le_0n.
  + inversion H.
  + apply IHb in H.
    apply le_snsn. auto.
Defined. (*excellent!!*)



Print reflect.
(*Inductive reflect (P : Prop) : bool -> Set :=
    ReflectT : P -> reflect P true | ReflectF : ~ P -> reflect P false*)
Check iff_reflect.
(*forall (P : Prop) (b : bool), P <-> b = true -> reflect P b*)
Goal forall (P : Prop) (b : bool), P <-> b = true -> reflect P b.
  intros.
  destruct H.
  destruct b. (*we can destruct b because b is defined constructively*)
  + constructor.
    apply (H0 eq_refl).
  + constructor.
    unfold not.
    intros.
    apply H in H1.
    inversion H1. (*H1 is absurd*)
Defined.
    
Lemma leb_reflect : forall x y, reflect (x <= y) (x <=? y).
  intros x y. 
  apply iff_reflect.
  apply (leb_le x y).
Defined.

Goal 3 <= 7.
  (*pose (leb_reflect 3 7) as H.*)
  (*assert (leb_reflect 3 7).*)
  assert (H := leb_reflect 3 7).
  remember ( 3<=? 7) as b.
  destruct H.
  (* Case 1: H = ReflectT (3<7) P *)
  auto.
  (* Case 2: H = ReflectF (3<7) Q *)
  simpl in Heqb.
  inversion Heqb.
Qed.  

Goal 3<=7.
  assert (H := leb_reflect 3 7).
  inversion H. (*inversion will remove absurd path*)
  auto.
Qed.




Inductive sumbool (A B : Prop) : Set :=
 | left : A -> sumbool A B
 | right : B -> sumbool A B.
(*when the function eval to false, we use true=false to remove absurt path*)

Definition t1 := sumbool (3<=7) (2<=3).
Lemma less37: 3<=7. Proof. lia. Qed.
Lemma greater23: 2<=3. Proof. lia. Qed.

Definition v1a: t1 := left (3<=7) (2<=3) less37.
Definition v1b: t1 := right (3<=7) (2<=3) greater23.
Definition t2 := sumbool (3<=7) (3<=2).
Definition v2a: t2 := left (3<=7) (3<=2) less37.
Notation "{ A } + { B }" := (sumbool A B) : type_scope.

Definition t4 := forall a b, {a<=b}+{~(a<=b)}.
Definition v3: {3<=7}+{~(3<=7)} := left _ _ less37.
Definition is_3_less_7: bool :=
 match v3 with
 | left _ _ _ => true
 | right _ _ _ => false
 end.
Eval compute in is_3_less_7. (* = true : bool *)
Print t4. (* = forall a b : nat, {a < b} + {~ a < b} *)

Check leb_reflect.
Definition le_dec (a: nat) (b: nat) : {a<=b}+{~(a<=b)} :=
match leb_reflect a b with
| ReflectT _ P => left (a <= b) (not (a <= b)) P
| ReflectF _ Q => right (a <= b) (not (a <= b)) Q
end.
Definition le_dec' (a: nat) (b: nat) : {a<=b}+{~(a<=b)}.
  destruct (leb_reflect a b) as [P|Q]. left. apply P. right. apply Q.
Defined.
Print le_dec.
Print le_dec'.
Theorem lt_dec_equivalent: forall a b, le_dec a b = le_dec' a b.
Proof.
intros.
unfold le_dec, le_dec'.
reflexivity.
Qed.


Inductive sorted: list nat -> Prop :=
| sorted_nil:
    sorted nil
| sorted_1: forall x,
    sorted (x::nil)
| sorted_cons: forall x y l,
   x <= y -> sorted (y::l) -> sorted (x::y::l). 


Hint Resolve leb_reflect : bdestruct.
Ltac bdestruct X :=
  let H := fresh in let e := fresh "e" in
   evar (e: Prop);
   assert (H: reflect e X); subst e;
    [eauto with bdestruct
    | destruct H as [H|H];
       [ | try first [apply not_le in H | apply not_le in H]]].

Example reflect_example2: forall a,
    (if a <=? 5 then a else 2) <= 6.
  intros.
  bdestruct (a <=? 5). (* instead of: destruct (leb_reflect a 5). *)
  lia.
  lia.
Qed.




Module Sort1.
Fixpoint insert (i : nat) (l : list nat) :=
  match l with
  | [] => [i]
  | h :: t => if i <=? h then i :: h :: t else h :: insert i t
  end.
Fixpoint sort (l : list nat) : list nat :=
  match l with
  | [] => []
  | h :: t => insert h (sort t)
  end.
Eval simpl in sort [3;1;4;1;5;9;2;6;5;3;5].

Lemma insert_sorted:
  forall a l, sorted l -> sorted (insert a l).
Proof.
  intros.
  induction H; simpl.
  +
    constructor.
  +
    bdestruct (a <=? x).
    - constructor. auto.
      constructor.
    - assert (x <= a). lia.
      constructor. auto.
      constructor.
  + bdestruct (a <=? x).
    - constructor. auto.
      constructor. auto.
      auto.
    - bdestruct (a <=? y).
      constructor. auto.
      lia.
      constructor. auto.
      auto.
      constructor.
      lia.
      simpl in IHsorted.
      assert (a <=? y = false).
      assert (not (a <= y)).
      lia.
      assert (H4 := leb_reflect a y).
      destruct H4.
      lia.
      reflexivity.
      rewrite H3 in *.
      auto.
Qed.
End Sort1.


Module Sort2.
Fixpoint insert (x:nat) (l: list nat) :=
  match l with
  | nil => x::nil
  | h::t => if le_dec x h then x::h::t else h :: insert x t
 end.

Fixpoint sort (l: list nat) : list nat :=
  match l with
  | nil => nil
  | h::t => insert h (sort t)
  end.
  
Lemma insert_sorted:
  forall a l, sorted l -> sorted (insert a l).
Proof.
  intros.
  induction H.
  + constructor.
  + simpl.
    destruct (le_dec a x).
    constructor. auto. constructor.
    apply Nat.nle_gt in n. apply Nat.lt_le_incl in n.
    constructor. auto.
    constructor.
  + simpl.
    destruct (le_dec a x).
    constructor. auto.
    constructor. auto.
    auto.
    destruct (le_dec a y).
    constructor. lia.
    constructor. auto.
    auto.
    constructor.
    auto.
    simpl in IHsorted.
    destruct (le_dec a y).
    apply n0 in l0. contradiction.
    auto.
Qed.
End Sort2.


Axiom lt_dec_axiom_1: forall i j: nat, i<j \/ ~(i<j).

Axiom le_dec_axiom_2: forall i j: nat, {i<=j} + {~(i<=j)}.
Definition max_with_axiom (i j: nat) : nat :=
   if le_dec_axiom_2 i j then j else i.
Eval compute in max_with_axiom 3 7.


Lemma prove_with_max_axiom: max_with_axiom 3 7 = 7.
Proof.
unfold max_with_axiom.
try reflexivity. (* does not do anything, reflexivity fails *)
(* uncomment this line and try it: 
   unfold lt_dec_axiom_2.
*)
destruct (le_dec_axiom_2 3 7).
reflexivity.
contradiction n. lia.
Qed.


Lemma compute_with_lt_dec: (if le_dec 3 7 then 7 else 3) = 7.
Proof.
  compute.
  reflexivity.
Qed.











