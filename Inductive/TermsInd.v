

(************************************************************************)
(* Copyright (c) 2021, Gergei Bana and Qianli Zhang                                    *)
(*                                                                      *)
(* This work is licensed under the MIT license. The license is          *)
(* described in the file "LICENSE" available at the root of the source  *)
(* or at https://opensource.org/licenses/MIT                            *)
(* Special thanks to Qianli and Ajay K. Eeralla                         *)
(************************************************************************)

Require Export Coq.Lists.List. 
Require Export Coq.Init.Logic.
Require Export Coq.Init.Peano.
Require Export Coq.Arith.EqNat.
Require Export Setoid.
Import ListNotations.
Require Export FunctionalExtensionality.
Require Import Coq.Bool.Bool.
Require Import Coq.Init.Nat.
Require Export ComputationTypeInd.
Require Export CpdtTactics.
Require Import Coq.micromega.Lia.

 


(* INduction *)


Section ppt_ind'.
  Variable P : ppt -> Prop.
  
  Hypothesis Holes_case :
    forall n,
      P  (Holes n).
    
  Hypothesis Functions_case :
    forall hag arg f (ls : list ppt),
      listforall ppt P ls -> P  (Functions hag arg f ls).



  (* Why is this needed? *)
Definition list_ppt_ind := O.


Fixpoint ppt_ind' (tr : ppt) : P tr :=
  match tr with
  | Holes n => Holes_case n
  | Functions hag arg f ls => Functions_case hag arg f ls
                                             ((fix list_ppt_ind (ls : list ppt) : listforall ppt P ls :=
                                                 match ls with
                                                 | nil => I
                                                 | cons tr' rest => conj (ppt_ind' tr') (list_ppt_ind rest)
                                                 end) ls)
  end.
  

  (** We include an anonymous [fix] version of [list_ppt_ind] that is literally _nested_ inside the definition 
of the recursive function corresponding to the inductive definition that had the nested use of [list].   *)

End ppt_ind'.

Print ppt_ind'.



(*******************************************************************************)
(*******************************************************************************)
(***************************** Ctypeof ***********************************)
(*******************************************************************************)
(*******************************************************************************)


Fixpoint Ctypeoft (t : ppt) : ComputationType :=
  match t with
  | Holes n => Deterministic
  | Functions hag arg f tl => haglubpair hag ( haglub (maplist ppt ComputationType Ctypeoft tl)) 
  end.

Compute (Ctypeoft (EQ [default ; nonce 3 ; (adv 2) [nonce 1] ])).  
Compute (Ctypeoft (EQ [ nonce 3 ; (adv 2) [nonce 1] ])).  
Compute (Ctypeoft (EQ [ (adv 2) [nonce 1] ])).  


Definition  Ctypeofl (tl : list ppt) : ComputationType :=
  haglub (maplist ppt ComputationType Ctypeoft tl).

Proposition Ctypeoflis :
  forall tl, (haglub
                (maplist ppt ComputationType
                         (fix Ctypeoft (t : ppt) : ComputationType :=
                            match t with
                            | Holes _ => Deterministic
                            | Functions hag0 _ _ tl0 =>
                              haglubpair hag0
                                         (haglub
                                            (maplist ppt ComputationType Ctypeoft
                                                     tl0))
                            end) tl)) = Ctypeofl tl. 
Proof.
  auto.
Qed. 


Lemma unfold_Ctypeoft :
  forall (tl :  list ppt) (hag : ComputationType) (arg : arity) (f : Symbols hag arg),
    Ctypeoft (Functions hag arg f tl) =  haglubpair hag (Ctypeofl tl).
Proof.
  intros.
  unfold Ctypeoft.
  rewrite Ctypeoflis.
  auto.
Qed. 


Lemma unfold_Ctypeofl :
  forall (t : ppt) (tl :  list ppt)  ,
    Ctypeofl (t :: tl) =  haglubpair (Ctypeoft t) (Ctypeofl tl).
Proof.
  intros. unfold Ctypeofl at 1. simpl. rewrite Ctypeoflis.
  auto.
Qed.


Definition PPTbt (hag : ComputationType) (t : ppt) : bool :=
  if (hagleb  (Ctypeoft t) hag ) then true else false.  


Definition PPTbl (hag : ComputationType) (tl : list ppt) : bool :=
  if (hagleb  (Ctypeofl tl) hag ) then true else false.  


Lemma unfold_PPTbl :
  forall (t : ppt)  (tl : list ppt)  (hag : ComputationType),
    PPTbl hag (t :: tl) = PPTbt hag t && PPTbl hag tl.
Proof.
  intros.
  unfold PPTbl at 1. 
  rewrite unfold_Ctypeofl.
  unfold andb.
  unfold PPTbt. 
  unfold PPTbl.
  unfold haglubpair; unfold hagleb; destruct (Ctypeoft t); destruct (Ctypeofl tl); destruct hag; auto.
Qed.



Inductive  PPTt: ComputationType -> ppt -> Prop :=
| pHole' :
    forall hag n,
      PPTt hag (Holes n)
| pFun':
    forall hag hag' arg (f : Symbols hag' arg) tl  ,
      hag' <<< hag
      -> PPTl hag tl
      -> PPTt hag (Functions hag' arg f tl)
| PPTtup :
    forall hag hag' {t: ppt},
      hag <<< hag'
      ->(PPTt hag) t
      -> (PPTt hag') t
with  PPTl: ComputationType -> list ppt -> Prop :=
| pnil :
    forall hag,
      PPTl hag nil
| pCons :
    forall hag {t : ppt} {tl : list ppt} ,  PPTt hag t -> PPTl hag tl -> PPTl hag  (t :: tl).


Scheme PPTt_mut := Minimality for PPTt Sort Prop
  with PPTl_mut := Minimality for PPTl Sort Prop.



Lemma listforallPPT :
  forall hag tl , PPTl hag tl <-> listforall ppt (fun t => PPTt hag t) tl. 
Proof.
  intros.
  split.
  - intros. 
    induction tl. 
    + unfold listforall; auto.
    + rewrite unfold_listforall.
      split.
      inversion H; auto.
      apply IHtl.
      inversion H; auto.
  - intros. 
    induction tl.
    constructor. 
    apply pCons. 
    destruct H; auto.
    destruct H; auto. 
Qed. 






                                                                                                    
Lemma PPTbtPPTt :
  forall hag t,
    PPTbt hag t  = true <-> PPTt hag t.
Proof. 
  split.
  - intros.
    induction t using ppt_ind'.
    + constructor.    
    + apply pFun'. 
      * unfold PPTbt in *.
        rewrite unfold_Ctypeoft in *.
        apply hagleble.
        destruct hag; destruct hag0; destruct (Ctypeofl ls);
          unfold haglubpair in *; unfold listforall in H0; unfold hagleb in *; simpl in *; auto. 
      * unfold PPTbt in H. 
        rewrite unfold_Ctypeoft in H.
        induction ls. 
        ** constructor. 
        ** apply pCons. 
           *** inversion H0. 
               apply H1. 
               rewrite unfold_Ctypeofl in H.
               unfold PPTbt.
               destruct (Ctypeoft a); destruct (Ctypeofl ls); destruct hag; destruct hag0;
                 unfold haglubpair in *; unfold hagleb in *;  try discriminate H;  auto. 
           *** apply IHls. 
               **** inversion H0. 
                    auto.
               **** rewrite unfold_Ctypeofl in H.
                    destruct (Ctypeoft a); destruct (Ctypeofl ls); destruct hag; destruct hag0;
                      unfold haglubpair in *; unfold hagleb in *;  try discriminate H;  auto.
  - intros.
    Print PPTt_mut. 
    apply (PPTt_mut (fun hag' => fun t' => PPTbt hag' t' = true ) (fun hag' => fun tl' => PPTbl hag' tl' = true )).
    + intros.
      unfold PPTbt.
      unfold Ctypeoft.  
      destruct hag; unfold hagleb; auto . 
    + intros.
      unfold PPTbt.
      rewrite unfold_Ctypeoft.
      unfold PPTbl in H2.  apply hagleble in H0.
      destruct (Ctypeofl tl); destruct hag0; destruct hag'; unfold hagleb in *; simpl; auto. 
    + intros.
      unfold PPTbt.
      unfold PPTbt in H2.   apply hagleble in H0.
      destruct (Ctypeoft t0); destruct hag0; destruct hag'; unfold hagleb in *; simpl; auto.
    + intros.
      unfold PPTbl; simpl; auto.
    + intros. 
      unfold PPTbt in *.
      unfold PPTbl in *.
      rewrite unfold_Ctypeofl.
      destruct (Ctypeoft t0); destruct (Ctypeofl tl); destruct hag; destruct hag0; unfold haglubpair in *;
      unfold hagleb in *; try discriminate H1;  auto.
    + auto.
Qed.       
      
 
        
          
          
          
Lemma CtypeofPPT :
  forall  {t : ppt} ,
    (PPTt (Ctypeoft t)) t.
Proof.
  intros.
  apply PPTbtPPTt.
  unfold PPTbt.
  destruct (Ctypeoft t); unfold hagleb; auto.
Qed.  

Ltac ProvePPTt :=
  repeat ( intros;
           match goal with
           |H : ?prop |- ?prop => apply H
           | |- ?hag1 <<< ?hag2  => ProveHag
           |   |- PPTl ?hag [] => apply pnil
           |   |- PPTt ?hag (Holes ?n) => apply pHole'
           |   |- PPTt ?hag (Functions ?hag' ?arg ?f ?tl) => apply pFun'
           |   |- PPTl ?hag ?tl => apply pCons
           |H: PPTt ?hag1 ?t |- PPTt ?hag2 ?t =>  apply (@PPTtup hag1 hag2)
           end).



Lemma DeterministicHAGt :
  forall (hag : ComputationType) {t :  ppt} ,
    (PPTt Deterministic) t
    -> (PPTt hag) t.
Proof.
  intros.
  ProvePPTt. 
Qed.



Proposition HAGGeneral :
  forall (hag : ComputationType) , forall {t : ppt} ,
      (PPTt hag) t
      -> (PPTt General) t .
Proof.
  intros. destruct hag;   ProvePPTt. 
Qed.

                                                                                                    
Lemma PPTblPPTl :
  forall hag tl,
    PPTbl hag tl  = true <-> PPTl hag tl.
Proof.
  intros.
  split. 
  - intros. 
    induction tl. 
     + constructor.
     + apply pCons.
       * rewrite unfold_PPTbl in H.
         assert ( PPTbt hag a = true).
         { destruct (PPTbt hag a); destruct (PPTbl hag tl); try discriminate H; auto. }
         apply PPTbtPPTt.
         auto.
       * apply IHtl. 
         rewrite unfold_PPTbl in H.
         assert ( PPTbl hag tl = true).
         { destruct (PPTbt hag a); destruct (PPTbl hag tl); try discriminate H; auto. }
         auto. 
  - intros.
    induction tl. 
    + constructor.
    + inversion H.
      rewrite unfold_PPTbl.
      apply IHtl in H4. 
      apply PPTbtPPTt in H3.
      rewrite H3; rewrite H4; auto.
Qed.








Inductive TermOrList : Set :=
| Term
| List.


Definition  toleqb (tol1 tol2 : TermOrList) : bool :=
  match tol1 with
  | Term => match tol2 with Term => true | List => false end
  | List => match tol2 with Term => false | List => false end
  end.



Definition ToL (tol : TermOrList) : Set :=
  match tol with
  | Term => ppt
  | List => list ppt
  end.




(* Note that the map below when tol is List fills holes with default if lx  it is too short. 
To avoid that, you need to complement lx  with Holes. *)
 

Fixpoint Cintt (t : ppt) (tol : TermOrList) : ToL tol -> ppt :=
  match t with
  | Functions hag arg f tl => (fun x => Functions hag arg f ((maplist ppt ppt (fun t' =>  Cintt t' tol x)) tl))
  | Holes  n  => match tol with Term => (fun x => x) | List => (fun lx => nth n lx default) end
  end.




Definition Cintl : list ppt -> forall tol : TermOrList ,  ToL tol -> list ppt :=
  (fun tl => (fun tol => (fun x => (maplist ppt ppt (fun t => Cintt t tol x) tl ) ))).


Lemma unfold_Cintt :
  forall hag arg f tl tol x,
    Cintt  (Functions hag arg f tl) tol x  = Functions hag arg f (Cintl  tl tol x).
Proof.
  auto.
Qed.   


Lemma Cintlis:
  forall  tl tol x ,
    maplist ppt ppt (fun t => Cintt t tol x) tl = Cintl tl tol x. 
Proof.
  auto. 
Qed. 

(*
Lemma Cintlis2 : forall  tol t2 l ,
    (fix maplist (ls : list ppt) : list ppt :=
       match ls with
       | [] => []
       | h :: t =>
         (fix Cintt (t0 : ppt) (tol : TermOrList) {struct t0} : ToL tol -> ppt :=
            match t0 with
            | Holes n => match tol as tol0 return (ToL tol0 -> ppt) with
                         | Term => fun x : ToL Term => x
                         | List => fun x : ToL List => nth n x default
                         end
            | Functions hag0 arg0 f tl => fun x : ToL tol => Functions hag0 arg0 f
                                                                  ((fix maplist0 (ls0 : list ppt) : list ppt :=
                                                                      match ls0 with
                                                                      | [] => []
                                                                      | h0 :: t1 => Cintt h0 tol x :: maplist0 t1
                                                                      end
                                                                   ) tl)
            end
         ) h tol t2 :: maplist t
       end
    ) l = Cintl  l  tol  t2.
Proof.
  intros.
  auto.
Qed.
 *)

Lemma unfold_Cintl :
  forall  t tl tol x,
    Cintl  (t :: tl) tol x  = (Cintt t tol x) ::  (Cintl  tl tol x).
Proof.
  auto.
Qed. 

Lemma Cintllength :
  forall {tol lt x} ,
    length (Cintl lt tol x) = length lt.
Proof.
  intros. 
  induction lt.
  - simpl; auto.
  - simpl; auto. 
Qed.





(* This might be useful but takes enormous time to process: 

Lemma bla : forall t1 t2 ,
    Ctypeoft (Cintt t1 Term t2) <<< haglubpair (Ctypeoft t1) (Ctypeoft t2).
Proof. 
  intros.
  apply hagleble. 
  induction t1 using ppt_ind'. (* have to get rid of (f ls) before induction on ls *)
  - simpl. 
    induction ls.
    + simpl. 
      destruct hag; destruct (Ctypeoft t2); unfold haglubpair; unfold hagleb; auto. 
    + inversion H.
      rewrite Ctypeoflis in *.
      rewrite Ctypeoflis in *.
      rewrite Cintlis in *. simpl. 
      unfold Ctypeofl. unfold maplist. simpl.  rewrite Ctypeoflis in *. rewrite Ctypeoflis in *.
      unfold haglubpair in *; unfold hagleb in *.
      destruct hag; destruct  (Ctypeoft a); destruct  (Ctypeoft t2); destruct (Ctypeoft (Cintt a Term t2)); destruct (Ctypeofl (Cintl ls Term t2)); destruct (Ctypeofl ls); 
          try discriminate H1; auto.
  - unfold Cintt.
    unfold Ctypeoft at 2.
    destruct (Ctypeoft t2); unfold haglubpair; unfold hagleb; auto. 
Qed. 
  *)



 

Fixpoint Holesmaxt (t : ppt) : nat :=
  match t with
  | Holes n => S n    (* The reason for S n and not n is to have 0 for no Holes at all  *)
  | Functions hag arg f lt => listmax (maplist ppt nat Holesmaxt lt)
  end.


Definition  Holesmaxl (lt :list ppt) : nat :=
  listmax (maplist ppt nat Holesmaxt lt).


Lemma unfold_Holesmaxt :
  forall hag arg f lt,
    Holesmaxt (Functions hag arg f lt) = Holesmaxl lt.
Proof.
  auto.
Qed.



Lemma unfold_Holesmaxl :
  forall t lt,
    Holesmaxl (t :: lt) = max (Holesmaxt t) (Holesmaxl lt).
Proof.
  intros.
  unfold Holesmaxl at 1. 
  unfold listmax.   
  simpl. 
  auto.
Qed. 

  
Definition Holesl (ln : list nat) : list ppt :=
  maplist nat ppt  Holes ln.

Fixpoint FirstnHoles (n :  nat) : list ppt :=
  match n with
  | 0 => [ ]
  | S n => FirstnHoles n ++ [Holes n]
  end.

Lemma unfold_FirstnHoles :
  forall n,
    FirstnHoles (S n) = FirstnHoles n ++ [Holes n].
Proof.
  auto.
Qed.


Lemma FirstnHoles_length :
  forall n ,
    length (FirstnHoles n) =  n.
Proof.
  intros.
  induction n.
  - unfold FirstnHoles. 
    unfold length.
    reflexivity.
  - rewrite unfold_FirstnHoles.
    rewrite app_length.
    rewrite IHn.
    unfold length.
    lia.
Qed. 



Lemma CinttonHoles : forall  (n : nat) (t : ppt)  (tl : list ppt),
    n >= Holesmaxt t
    -> Cintt t List  (FirstnHoles n  ++ tl) =  t.
Proof.
  intros.
  assert (forall tl ,   Cintt t List  (FirstnHoles n  ++ tl) =  t).
  { induction t using ppt_ind'.
    - unfold Holesmaxt in H.
      induction n.
      + lia. 
      + induction n.
        * intros. 
          assert (n0 = 0).
          { lia. }
          rewrite H0.
          unfold FirstnHoles.
          unfold Cintt.  
          unfold nth. 
          simpl.
          reflexivity.
        * intros.
          rewrite unfold_FirstnHoles. 
          unfold Cintt.
          rewrite <- app_assoc.
          inversion H. 
          ** assert (n >= length (FirstnHoles n)).
             { rewrite FirstnHoles_length. 
               lia. } 
             rewrite app_nth2.
             *** rewrite FirstnHoles_length.
                 rewrite n_minus_n. 
                 simpl. 
                 reflexivity.  
             *** rewrite FirstnHoles_length.
                 lia.
          ** apply IHn.
             lia.
    - intros.
      simpl.
      { assert (maplist ppt ppt (fun t' : ppt => Cintt t' List (FirstnHoles n ++ tl0)) ls = ls).  
        induction ls.
        + simpl.
          reflexivity. 
        + unfold maplist.
          destruct H0.
          rewrite unfold_Holesmaxt in H.
          rewrite unfold_Holesmaxl in H.
          assert (n >= Holesmaxt a).
          { lia. }
          rewrite (H0 H2).
          assert ((fix maplist (ls0 : list ppt) : list ppt := match ls0 with
                                                   | [] => []
                                                   | h :: t => Cintt h List (FirstnHoles n ++ tl0) :: maplist t
                                                   end) ls = maplist ppt ppt (fun t' : ppt => Cintt t' List (FirstnHoles n ++ tl0)) ls).
          { auto. }
          rewrite H3.
          rewrite IHls.
           * reflexivity.
           * assumption.
           * rewrite unfold_Holesmaxt.
             lia.  
        + rewrite H1.          
          reflexivity.
      }
  }
  apply H0.
Qed.



Lemma CinttonHolesmax :
  forall   (t : ppt)  (tl : list ppt),
    Cintt t List  (FirstnHoles (Holesmaxt t)  ++ tl) =  t.
Proof.
  intros.
  apply CinttonHoles.
  lia.
Qed.




Definition SkipnHolest (n : nat) (t : ppt) : list ppt :=
   FirstnHoles n ++ [t]. 


Lemma CinttonSkipnHolest :
  forall   (t : ppt)  (x : ppt),
    Cintt t List  (SkipnHolest (Holesmaxt t) x) =  t.
Proof.
  intros.
  unfold SkipnHolest.
  apply CinttonHoles.
  lia.
Qed.


Lemma nthonFirstnHoles : 
  forall  m n tl, 
    nth (m + n) (FirstnHoles m  ++ tl) default = nth n tl default.
Proof.
  intros.
  rewrite app_nth2.
  - rewrite FirstnHoles_length. 
    assert (m + n - m = n).
    { lia. }
    rewrite H.
    reflexivity.
  - rewrite FirstnHoles_length.
    lia.
Qed.


Lemma nthonSkipnHolest : 
  forall  m  t, 
    nth m  (SkipnHolest m t) default = t.
Proof.
  intros.
  unfold SkipnHolest.
  rewrite app_nth2.
  - rewrite FirstnHoles_length. 
    rewrite n_minus_n.
    simpl.
    reflexivity.
  - rewrite FirstnHoles_length.
    lia.
Qed.
