(************************************************************************)
(* Copyright (c) 2021, Ajay Eeralla                                     *)
(*                                                                      *)
(* This work is licensed under the MIT license. The license is          *)
(* described in the file "LICENSE" available at the root of the source  *)
(* or at https://opensource.org/licenses/MIT                            *)
(************************************************************************)

(** Formalizing the language defined in the paper "An interactive prover for protocol verification in the computational model by David Baelde et al." *)


Require Import Coq.Lists.List.

Set Implicit Arguments.

Module Vector.
  
 Inductive vec (A : Type) : nat -> Type :=
 | Nil : vec A 0
 | Cons: forall n, A -> vec A n  -> vec A (S n).

End Vector.

Section Term.

(** Signature for set of fixed symbols with fixed Arity *)

Record Signature : Type := mkSignature {
  symbol :> Type;
  arity : symbol -> nat;
  beq_symb : symbol -> symbol -> bool;
  beq_symb_ok : forall x y, beq_symb x y = true <-> x = y
  }.

(** Algebraic term *)
Check (arity).
Check symbol.
Parameter Sig : Signature.
Check (symbol Sig).
Variable f : Sig.
Check (arity Sig f).

Inductive Term : Type :=
| Var : nat -> Term
| FApp : forall f : (symbol Sig), Vector.vec Term (arity _ f) -> Term.


End Term.