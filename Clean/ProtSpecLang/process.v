(************************************************************************)
(* Copyright (c) 2021, Ajay Eeralla                                     *)
(*                                                                      *)
(* This work is licensed under the MIT license. The license is          *)
(* described in the file "LICENSE" available at the root of the source  *)
(* or at https://opensource.org/licenses/MIT                            *)
(************************************************************************)

(** Formalizing the language defined in the paper "An interactive prover for protocol verification in the computational model by David Baelde et al." *)


Require Import Coq.Lists.List.

Inductive process : Type :=
 | null : process
 | parallel : process -> process -> process
 | replicate : process -> process
 