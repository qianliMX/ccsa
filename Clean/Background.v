Require Export Coq.Lists.List.
Require Import Coq.micromega.Lia.
Require Export Coq.Init.Nat.




(* About the built-in "if then else" function *)

Lemma iteinv :
  forall (A : Type) (a : bool) (b c: A)  ,
    not (b = c)
    -> (if a then b else c) = b
    -> a = true.
Proof.
  intros.
  assert (not  (a = false)).
  { unfold not.
    intros.
    assert   ((if a then b else c) = c).
    { destruct a. discriminate H1. auto. }
    rewrite H0 in H2.  auto. }
  destruct a.
  - auto.
  - contradiction.
Qed.

Lemma TnF :
  not (True = False). 
Proof.
  unfold not.
  intros.
  rewrite <- H.
  auto.
Qed.  
  
Lemma TFiteinv :
  forall (a : bool) ,
    (if a then True else False)
    -> a = true.
Proof.
  intros.
  assert (H1 := TnF).
  apply (iteinv Prop a True False).
  - auto.
  - destruct a.
    + auto.
    + contradiction.
Qed. 






(* About lists *)


Section listforall.
  
  Variable T : Set.
  Variable P : T -> Prop.

  Fixpoint listforall (ls : list T) : Prop :=
    match ls with
    | nil => True
    | cons h t => P h /\ listforall t
    end.
   
End listforall.




Lemma unfold_listforall :
  forall (T : Set) (P : T -> Prop) (t : T) (tl : list T),
    listforall T P (cons t  tl) = (P t /\ listforall T P tl).  
Proof.
  auto. 
Qed.



  
Section maplist.
  
  Variables T T' : Set.
  Variable F : T -> T'.

  Fixpoint maplist (ls : list T) : list T' :=
    match ls with
    | nil => nil
    | cons h t => cons (F h) (maplist t)
    end.
  
End maplist.

Lemma unfold_maplist:
  forall T T' F h t,
    maplist T T' F (cons h t) = cons (F h) (maplist T T' F t).
Proof.
  auto.
Qed.



(* About Naturals *)

Lemma n_minus_n :
  forall (n : nat),
    n - n = 0.
Proof.
  lia.
Qed.


Fixpoint inb (n: nat) (ln: list nat) : bool :=
    match ln with
      | nil => false
      | n' :: ln' => eqb n n' || inb n ln'
    end.

Lemma unfold_inb:
  forall n n' ln',
    inb n (n' :: ln') = orb (eqb n n') (inb n ln').
Proof.
  auto.
Qed.



Fixpoint listmax  (ln: list nat) : nat :=
    match ln with
      | nil => 0
      | n' :: ln' => max n' (listmax ln')
    end.

Lemma unfold_listmax:
  forall  n' ln',
    listmax (n' :: ln') = max n' (listmax ln').
Proof.
  auto.
Qed.






Lemma firstn_app_exact :
  forall {A : Type} (l1 l2 : list A),
    @firstn A  (length l1)   (l1 ++ l2)   = l1.
Proof.
  intros.
  rewrite firstn_app.
  rewrite n_minus_n. 
  rewrite firstn_O.
  rewrite firstn_all.
  rewrite app_nil_r.
  reflexivity.
Qed. 


Lemma skipn_app_exact : forall {A : Type}  (l1 l2 : list A),
    @skipn A (length l1) (l1 ++ l2) = l2.
Proof.
  intros.
  rewrite skipn_app.
  rewrite n_minus_n.
  rewrite skipn_O.
  rewrite skipn_all.
  rewrite app_nil_l.
  reflexivity.
Qed. 
  
