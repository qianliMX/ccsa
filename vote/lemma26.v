(************************************************************************)
(* Copyright (c) 2021 ~ 2022, Gergei Bana, Qianli Zhang                 *)
(*                                                                      *)
(* This work is licensed under the MIT license. The license is          *)
(* described in the file "LICENSE" available at the root of the source  *)
(* or at https://opensource.org/licenses/MIT                            *)
(*                                                                      *)
(* Special thanks to Ajay K. Eeralla                                    *)
(************************************************************************)

(*   Unicode used in this file:

 *   5. 'τ' :     U+03C4

 *   5. '❴' :     U+2774
 *   5. '❵' :     U+
 *   5. '＾':     U+ FF3E

*)

Require Import Coq.Lists.List.
Require Import Coq.micromega.Lia.
Import ListNotations.
Require Export AuxiliaryTheoremEquality.


Parameter Triples : Symbols Deterministic (narg 3).
Notation "'Triple'" := (FuncInt Deterministic (narg 3) Triples).

Parameter Taus1 : Symbols Deterministic (narg 1).
Notation τ1 x := ((FuncInt Deterministic (narg 1) Taus1) [x]).

Parameter Taus2 : Symbols Deterministic (narg 1).
Notation τ2 x := ((FuncInt Deterministic (narg 1) Taus2) [x]).

Parameter Taus3 : Symbols Deterministic (narg 1).
Notation τ3 x := ((FuncInt Deterministic (narg 1) Taus3) [x]).

Notation "'＜' c1 ',' c2 '＞'" := (Pair [c1; c2]) (at level 100, right associativity). (* U+FF1C and U+FF1E)*)
Notation "'＜' c1 ',' c2 ',' c3 '＞'" := (Triple [c1; c2; c3]) (at level 100, right associativity). (*overload ＜,＞ notation*)

(*  *)
Axiom Tau1Tri : forall {x1 x2 x3: ppt}, (τ1 (＜x1, x2, x3 ＞)) = x1.
Axiom Tau2Tri : forall {x1 x2 x3: ppt}, (τ2 (＜x1, x2, x3 ＞)) = x2.
Axiom Tau3Tri : forall {x1 x2 x3: ppt}, (τ3 (＜x1, x2, x3 ＞)) = x3.


Inductive iIndex : Set :=
| I0
| I1.

Inductive jIndex : Set :=
| J0
| J1.

Definition xor j i := match i with I0 => j | I1 => match j with J0 => J1 | J1 => J0 end end.

Notation "j ⨁ i" := (xor j i) (at level 100, right associativity). (* U+2A01 *)


(* Notation "b0 & b1" := (If b0 Then (If b1 Then TRue Else FAlse) Else FAlse) (at level 20, right associativity). *)

Notation "b0 & b1"    := (If b0 Then b1 Else FAlse) (at level 20, right associativity). (* *)
Notation "b0 'or' b1" := (If b0 Then TRue Else b1)  (at level 20, right associativity). (* *)
Notation "! b ":= (If b Then FAlse Else TRue) (at level 20, right associativity). (* notb *)


(* *)
Theorem AndComp: forall A B,
    A & B = If A Then (If B Then TRue Else FAlse) Else FAlse.
  intros.
  rewrite <- If_tf.
  reflexivity. 
Qed.  


(* *)
Theorem AndIdem : forall A,
    A & A = A.
  intros.
  rewrite (@If_eval (fun x => x) _ A).
  rewrite <- If_tf; auto.
  all : ProveContext. Qed.

(* *)
Theorem AndComm : forall A B,
    A & B = B & A.
  intros.
  rewrite (@If_tf B) at 1.
  rewrite (@If_morph (fun x => If A Then x Else FAlse) B).
  rewrite If_same.
  rewrite <- (If_tf).
  reflexivity.
  ProveContext.
Qed.

(* *)
Theorem AndAsso : forall A B C,
    (A & B) & C = A & B & C.
  intros.
  rewrite (@If_morph (fun x => If x Then C Else FAlse) A).
  rewrite If_false.
  reflexivity.
  ProveContext.
Qed.


(* *)
Theorem OrIdem : forall A,
    A or A = A.
Proof.
  intros.
  rewrite (@If_eval _ (fun x => x) A). Search (If _ Then TRue Else FAlse).
  rewrite <- If_tf. auto.
  all: ProveContext. Qed.
  


(* *)
Lemma IfFirst  : forall B C x y, (If B & C Then x Else (If B & (!C) Then x Else (If (! B) & C Then y Else y))) = (If B Then x Else y).
  intros.
  rewrite (@If_morph (fun x => If x Then _ Else _ ) B _ _).
  rewrite If_same, If_false.
  rewrite (@If_eval (fun b => (If C Then x Else (If b & ! C Then x Else y))) (fun b => (If b & ! C Then x Else y))).
  rewrite If_true, If_false, If_false. 
  rewrite (@If_eval (fun _ => _ ) (fun c => (If ! c Then x Else y))).
  rewrite If_false, If_true, If_same.
  reflexivity.
  all : ProveContext. Qed.

(* *)
Lemma IfFirst'  : forall A B C x y, (If A & B & C Then x Else (If A & B & (!C) Then x Else (If A & (! B) & C Then y Else y))) = (If A & B Then x Else y).
  intros.
  repeat rewrite <- AndAsso.
  rewrite (@If_morph (fun x => If x Then _ Else _ ) (A & B) _ _).
  rewrite If_false, If_same.
  rewrite (@If_eval (fun ab => (If C Then x Else (If ab & ! C Then x Else y))) (fun ab => (If ab & ! C Then x Else y))).
  rewrite If_true, If_false, If_false.
  rewrite (@If_eval (fun _ => _ ) (fun c => (If ! c Then x Else y))).
  rewrite If_false, If_true, If_same.
  reflexivity.
  all : ProveContext. Qed.

                                                                                                                                          
(* *)
Lemma IfSecond : forall B C x y, (If B & C Then x Else (If B & (!C) Then y Else (If (! B) & C Then x Else y))) = (If C Then x Else y).
  intros.
  rewrite (@If_morph (fun a => If a Then x Else _ ) B _ _).
  rewrite If_false.
  rewrite (@If_eval (fun b => (If C Then x Else (If b & ! C Then y Else (If (If ! b Then C Else FAlse) Then x Else y))))
                    (fun b => (If b & ! C Then y Else If If ! b Then C Else FAlse Then x Else y))).
  repeat rewrite If_true. repeat rewrite If_false. rewrite If_true.
  rewrite If_same. rewrite If_same.
  reflexivity.
  all : ProveContext. Qed.


(* *)
Lemma IfSecond'  : forall A B C x y, (If A & B & C Then x Else (If A & B & (!C) Then y Else (If A & (! B) & C Then x Else y))) = (If A & C Then x Else y).
  intros.
  rewrite (@If_morph (fun x => If (If A Then x Else FAlse) Then _ Else _) B).
  repeat rewrite If_same. rewrite If_false.
  rewrite (@If_eval (fun b => If If A Then C Else FAlse Then x
                              Else If If A Then If b Then ! C Else FAlse Else FAlse Then y Else If If A Then If ! b Then C Else FAlse Else FAlse Then x Else y)
                    (fun b => If If A Then If b Then ! C Else FAlse Else FAlse Then y Else If If A Then If ! b Then C Else FAlse Else FAlse Then x Else y)).
  rewrite If_true. rewrite If_false. rewrite If_true. rewrite If_false. rewrite If_same.
  repeat rewrite If_false. rewrite If_same. rewrite If_true.
  rewrite If_same.
  reflexivity.
  all : ProveContext.
Qed.



(** Variants of If_branch **)

Lemma MT: forall p q: Prop,
    (p -> q) -> not q -> not p.
Proof.
  auto.
Qed.

Theorem cind_len_rev : forall {lt1 lt2 : list ppt},
    (lt1 ~ lt2) -> length lt1 = length lt2.
Proof.
  intros.
  pose (@cind_len lt1 lt2).
  apply (MT (length lt1 <> length lt2) (~ lt1 ~ lt2)) in n.
  intuition.
  auto.
Qed.




(* *)

Theorem IF_branch_gen : forall (lz lz' z z' : list ppt) (x x' y y' b b' : ppt),
    lz ++ z ++ [b; x] ~ lz' ++ z' ++ [b'; x']
  -> lz ++ z ++ [b; y] ~ lz' ++ z' ++ [b'; y']
  -> lz ++ z ++ [If b Then x Else y] ~ lz' ++ z' ++ [If b' Then x' Else y'].
Proof.
  intros.
  repeat rewrite app_assoc in H, H0.
  pose (@IF_branch (lz ++ z) (lz'++z') x x' y y' b b' H H0).

(* |lz ++ z| = |lz' ++ z'|*)
  assert (H1:= H); auto.
  apply cind_len_rev in H1.
  rewrite app_length in H1. rewrite (app_length (lz' ++ z') [b'; x']) in H1. simpl in H1.
  rewrite Nat.add_comm in H1. rewrite (Nat.add_comm (length (lz' ++ z')) 2) in H1.
  apply Plus.plus_reg_l in H1.

(* *)
  repeat rewrite app_assoc. 
  apply (@cind_funcapp (fun x => (firstn (length (lz ++ z)) x) ++ (skipn (1 + (length (lz ++ z))) x))) in c.
  rewrite H1 in c at 3 4.
  repeat rewrite firstn_app_exact in c.
  assert (forall (lz:list ppt) z b, lz ++ [z; b] = (lz ++ [z]) ++ [b]).
  { intros. induction lz0; auto. intros; simpl. rewrite IHlz0. auto. }
  repeat rewrite H2 in c. clear H2.
  rewrite skipn_app in c.
  rewrite last_length in c.
  assert (1 + length (lz ++ z) - S (length (lz ++ z)) = 0). lia.
  rewrite H2 in c.
  rewrite skipn_O in c. clear H2. clear H1.

  assert (forall lz z (b:ppt), (1 + length (lz ++ z)) = length ((lz ++ z) ++ [b])).
  { intros. rewrite (app_length (lz0 ++ z0) [b0]). simpl. lia. }
  rewrite (H1 lz z b) in c. rewrite (H1 lz' z' b') in c. clear H1.
  rewrite skipn_all in c.
  rewrite skpn in c.
  auto.

  ProveContext.
Qed.
  
  
Theorem IF_branch':  forall (lz lz' : list ppt) (x x' y y' b b' : ppt),
    lz ++ [b; x] ~ lz' ++ [b'; x']
  -> lz ++ [b; y] ~ lz' ++ [b'; y']
  -> lz ++ [If b Then x Else y] ~ lz' ++ [If b' Then x' Else y'].
Proof.
  intros.
  pose (@IF_branch_gen lz lz' [] [] x x' y y' b b' H H0).
  simpl in c.
  auto.
Qed.


Theorem IF_branch'' : forall (lz lz': list ppt) (z z' x x' y y' b b' : ppt),
    lz ++ [z; b; x] ~ lz' ++ [z'; b'; x']
  -> lz ++ [z; b; y] ~ lz' ++ [z'; b'; y']
  -> lz ++ [z; If b Then x Else y] ~ lz' ++ [z'; If b' Then x' Else y'].
Proof.
  intros.
  pose (@IF_branch_gen lz lz' [z] [z'] x x' y y' b b' H H0).
  auto.
Qed.
    
    
Theorem IF_branch''' : forall (lz lz' : list ppt) (z z' z1 z1' x x' y y' b b' : ppt),
    lz ++ [z; z1; b; x] ~ lz' ++ [z'; z1'; b'; x']
  -> lz ++ [z; z1; b; y] ~ lz' ++ [z'; z1'; b'; y']
  -> lz ++ [z; z1; If b Then x Else y] ~ lz' ++ [z'; z1'; If b' Then x' Else y'].
Proof.
  intros.
  pose (@IF_branch_gen lz lz' [z; z1] [z'; z1'] x x' y y' b b' H H0).
  auto.
Qed.


Theorem IF_branch'''' : forall (lz lz' : list ppt) (z z' z1 z1' z2 z2' x x' y y' b b' : ppt),
    lz ++ [z; z1; z2; b; x] ~ lz' ++ [z'; z1'; z2'; b'; x']
  -> lz ++ [z; z1; z2; b; y] ~ lz' ++ [z'; z1'; z2'; b'; y']
  -> lz ++ [z; z1; z2; If b Then x Else y] ~ lz' ++ [z'; z1'; z2'; If b' Then x' Else y'].
Proof.
  intros.
  pose (@IF_branch_gen lz lz' [z; z1; z2] [z'; z1'; z2'] x x' y y' b b' H H0).
  auto.
Qed.

  
(*  *)



Lemma lemma26: forall z z' A0 A1 x0 x1 y0 y1 z0 z1 B0 B1 t00 t10 C0 C1 t01 t11 u0 u1, 
    z  ++ [If A0 Then ＜ ＜ x0, y0, z0 ＞, ＜ If B0 Then t00 Else Error , If C0 Then t01 Else Error , u0 ＞＞ Else Error]
    ~
    z' ++ [If A1 Then ＜ ＜ x1, y1, z1 ＞, ＜ If B1 Then t10 Else Error , If C1 Then t11 Else Error , u1 ＞＞ Else Error].
Proof.
  intros.
   
  assert (forall A {x y z B} {t: ppt} {C t0 t1 u},
  (If A Then ＜ ＜ x, y, z ＞, ＜ If B Then t0 Else Error , If C Then t1 Else Error , u ＞＞ Else Error
   =
   If A Then ＜ ＜ x, y, z ＞, ＜ If (A & B) Then t0 Else Error , If (A & C) Then t1 Else Error, u ＞＞ Else Error)) as H.
  {
    intros.
    rewrite (@If_eval (fun a => ＜ _ , ＜ If a & _ Then _ Else Error, If a & _ Then _ Else Error, _  ＞＞ ) (fun _ => _)).
    rewrite (AndComm TRue C).
    repeat rewrite If_true.
    repeat rewrite <-If_tf.
    reflexivity.
    all : try ProveContext.
  }
  rewrite (H A0).
  rewrite (H A1).

  
  rewrite <- (IfFirst' A0 B0 C0 t00 Error).
  
  rewrite (@If_morph (fun x => If A0 Then ＜ _ , ＜ x, _ , _ ＞ ＞ Else Error) (A0 & B0 & C0) _ _ ).
  rewrite (@If_morph (fun x => If A0 Then ＜ _ , ＜ x, _ , _ ＞ ＞ Else Error) (A0 & B0 & ! C0) _ _ ).
  rewrite (@If_morph (fun x => If A0 Then ＜ _ , ＜ x, _ , u0 ＞ ＞ Else Error) (A0 & (! B0) & C0) _ _ ).

  rewrite <- (IfSecond' A0 B0 C0 t01 Error) at 1 2 3 4.
  rewrite (@If_eval (fun abc => If _ Then ＜ _ , ＜ t00, If abc Then t01 Else _ , u0 ＞ ＞ Else Error )
                    (fun abc => If _ Then If _ Then ＜ _ , ＜ t00, If abc Then t01 Else _ , u0 ＞ ＞ Else Error Else 
                            (If _ Then If _ Then ＜ _ , ＜ Error, If abc Then t01 Else _ , u0 ＞ ＞ Else Error Else
                            (          If _ Then ＜ _ , ＜ Error, If abc Then t01 Else _ , u0 ＞ ＞ Else Error) ) ) (A0 & B0 & C0)).
  rewrite If_true, If_false.

  rewrite (@If_eval (fun abnc => If _ Then ＜ _ , ＜ t00, If abnc Then _ Else _ , u0 ＞ ＞ Else Error )
                    (fun abnc => (If _ Then If _ Then ＜ _, ＜ Error, If abnc Then Error Else _ , u0 ＞ ＞ Else Error Else
                                         If A0 Then ＜ _, ＜ Error, If abnc Then Error Else _ , u0 ＞ ＞ Else Error) ) (A0 & B0 & ! C0)).
  rewrite If_true, If_false.

  rewrite (@If_eval (fun anbc => If A0 Then ＜ _ , ＜ Error, If anbc Then t01 Else Error, u0 ＞ ＞ Else Error)
                    (fun anbc => If A0 Then ＜ _ , ＜ Error, If anbc Then t01 Else Error, u0 ＞ ＞ Else Error ) (A0 & (! B0) & C0)).
  rewrite If_true, If_false.


  rewrite <- (IfFirst' A1 B1 C1 t10 Error).
  rewrite (@If_morph (fun x => If A1 Then ＜ _ , ＜ x,  _ , u1 ＞ ＞ Else Error) (A1 & B1 & C1) _ _ ).
  rewrite (@If_morph (fun x => If A1 Then ＜ _ , ＜ x,  _ , u1 ＞ ＞ Else Error) (A1 & B1 & ! C1) _ _ ).
  rewrite (@If_morph (fun x => If A1 Then ＜ _ , ＜ x,  _ , u1 ＞ ＞ Else Error) (A1 & (! B1) & C1) _ _ ).

  rewrite <- (IfSecond' A1 B1 C1 t11 Error) at 1 2 3 4.
  rewrite (@If_eval (fun abc => If _ Then ＜ _ , ＜ t10 , If abc Then t11 Else _ , u1 ＞ ＞ Else Error )
                    (fun abc => (If _ Then If _ Then ＜_ , ＜ _ , If abc Then t11 Else _ , u1 ＞ ＞ Else Error
                         Else If _ Then If A1 Then ＜ _ , ＜ Error, If abc Then t11 Else _ , u1 ＞ ＞ Else Error
                         Else If _ Then ＜ _ , ＜ Error, If abc Then t11 Else _ , u1 ＞ ＞ Else Error) ) (A1 & B1 & C1)).
  rewrite If_true, If_false.

  rewrite (@If_eval (fun abnc => If _ Then ＜ _ , ＜ t10, If abnc Then _ Else _ , u1 ＞ ＞ Else Error )
                    (fun abnc => (If _ Then If _ Then ＜ _, ＜ Error, If abnc Then Error Else _ , u1 ＞ ＞ Else Error Else
                                       If A1 Then ＜ _, ＜ Error, If abnc Then Error Else _ , u1 ＞ ＞ Else Error) ) (A1 & B1 & ! C1)).
  rewrite If_true, If_false.

  rewrite (@If_eval (fun anbc => If A1 Then ＜ _ , ＜ Error, If anbc Then t11 Else Error, u1 ＞ ＞ Else Error)
                    (fun anbc => If A1 Then ＜ _ , ＜ Error, If anbc Then t11 Else Error, u1 ＞ ＞ Else Error ) (A1 & (! B1) & C1)).
  rewrite If_true, If_false. 
   
  (* 13 *)  
  apply IF_branch'. admit.
  (* 14 *)
  apply IF_branch''. admit.
  (* 15 *)
  apply IF_branch'''. admit.
  (* 16 *)
  apply IF_branch''''. admit.
  admit.

  all : ProveContext. 
  
  Admitted.















