(************************************************************************)
(* Copyright (c) 2021, Gergei Bana and Qianli Zhang                     *)
(*                                                                      *)
(* This work is licensed under the MIT license. The license is          *)
(* described in the file "LICENSE" available at the root of the source  *)
(* or at https://opensource.org/licenses/MIT                            *)
(************************************************************************)


Require Export Coq.Lists.List.
Import ListNotations.
Require Import Coq.micromega.Lia.


Require Export RandomizedPublicKeyEncryptionsEquality.
Require Export BasicExampleEquality.
Require Export PairsEquality.



(*Additional function symbols*)
Parameter ERrors : Symbols Deterministic (narg 0).
Notation "'Error'" := (ConstInt Deterministic ERrors).

Parameter Lens : Symbols Deterministic (narg 1).
Notation "'Len'" := (FuncInt Deterministic (narg 1) Lens).



(* Some basic notation *)
  Notation "'|' c1 '|'" := (Len [c1]) (at level 100, left associativity).




(*We just assume all the message has the same length now.*)
Axiom EqLen : forall u u', EQ [Len [u]; Len [u']] = TRue.


Theorem If_else: forall {b u u' u''}, If b Then u Else (If b Then u' Else u'') = If b Then u Else u''.
  intros. rewrite (@If_eval (fun _ => u) (fun x => If x Then u' Else u'') b). rewrite (@If_false u' u''). reflexivity. all: try ProveContext. Qed.


Lemma If_morph_list :(*"<<<"*)  forall  {f}  , (*">>>"*)
              forall  {b x y} {u} ,
      (*"<<<"*)ContextTerm General List f -> (*">>>"*)
    (f (If b Then x Else y :: u))
                     = (If b Then (f (x :: u) ) Else (f (y :: u))).
Proof.
  intros.
  apply (ceq_transymm  (@If_same b (f (If b Then x Else y :: u)))).
  assert (ContextTerm General Term (fun b' : ppt => f (If b' Then x Else y :: u))). ProveContext.
  rewrite (@If_eval (fun b'=> f (If b' Then x Else y :: u)) (fun b' => f (If b' Then x Else y :: u)) b H0 H0).
  apply (@ceq_subeq (fun x' : ToL Term => If b
    Then (f (x' :: u))
    Else (f  (If FAlse Then x Else y :: u)))
    (fun x': ToL Term => (If b Then f (x :: u) Else f (y :: u))) x (If TRue
               Then x
               Else y)). ProveContext. ProveContext.
 apply ceq_symm. apply If_true.
 apply (@ceq_subeq (fun x': ToL Term => If b
    Then (f (x :: u))
    Else (f (x' :: u)))  (fun x': ToL Term  => (If b
      Then f (x :: u)
      Else f (y :: u))) y (If FAlse
               Then x
               Else y)). ProveContext. ProveContext.
 apply ceq_symm. apply If_false.
 apply ceq_ref. Qed.
